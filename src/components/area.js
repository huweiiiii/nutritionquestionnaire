export default {
  area: [
    {
      sid: '110101',
      name: '东城区',
      citySid: '1101',
      provinceSid: '11'
    },
    {
      sid: '110102',
      name: '西城区',
      citySid: '1101',
      provinceSid: '11'
    },
    {
      sid: '110105',
      name: '朝阳区',
      citySid: '1101',
      provinceSid: '11'
    },
    {
      sid: '110106',
      name: '丰台区',
      citySid: '1101',
      provinceSid: '11'
    },
    {
      sid: '110107',
      name: '石景山区',
      citySid: '1101',
      provinceSid: '11'
    },
    {
      sid: '110108',
      name: '海淀区',
      citySid: '1101',
      provinceSid: '11'
    },
    {
      sid: '110109',
      name: '门头沟区',
      citySid: '1101',
      provinceSid: '11'
    },
    {
      sid: '110111',
      name: '房山区',
      citySid: '1101',
      provinceSid: '11'
    },
    {
      sid: '110112',
      name: '通州区',
      citySid: '1101',
      provinceSid: '11'
    },
    {
      sid: '110113',
      name: '顺义区',
      citySid: '1101',
      provinceSid: '11'
    },
    {
      sid: '110114',
      name: '昌平区',
      citySid: '1101',
      provinceSid: '11'
    },
    {
      sid: '110115',
      name: '大兴区',
      citySid: '1101',
      provinceSid: '11'
    },
    {
      sid: '110116',
      name: '怀柔区',
      citySid: '1101',
      provinceSid: '11'
    },
    {
      sid: '110117',
      name: '平谷区',
      citySid: '1101',
      provinceSid: '11'
    },
    {
      sid: '110118',
      name: '密云区',
      citySid: '1101',
      provinceSid: '11'
    },
    {
      sid: '110119',
      name: '延庆区',
      citySid: '1101',
      provinceSid: '11'
    },
    {
      sid: '120101',
      name: '和平区',
      citySid: '1201',
      provinceSid: '12'
    },
    {
      sid: '120102',
      name: '河东区',
      citySid: '1201',
      provinceSid: '12'
    },
    {
      sid: '120103',
      name: '河西区',
      citySid: '1201',
      provinceSid: '12'
    },
    {
      sid: '120104',
      name: '南开区',
      citySid: '1201',
      provinceSid: '12'
    },
    {
      sid: '120105',
      name: '河北区',
      citySid: '1201',
      provinceSid: '12'
    },
    {
      sid: '120106',
      name: '红桥区',
      citySid: '1201',
      provinceSid: '12'
    },
    {
      sid: '120110',
      name: '东丽区',
      citySid: '1201',
      provinceSid: '12'
    },
    {
      sid: '120111',
      name: '西青区',
      citySid: '1201',
      provinceSid: '12'
    },
    {
      sid: '120112',
      name: '津南区',
      citySid: '1201',
      provinceSid: '12'
    },
    {
      sid: '120113',
      name: '北辰区',
      citySid: '1201',
      provinceSid: '12'
    },
    {
      sid: '120114',
      name: '武清区',
      citySid: '1201',
      provinceSid: '12'
    },
    {
      sid: '120115',
      name: '宝坻区',
      citySid: '1201',
      provinceSid: '12'
    },
    {
      sid: '120116',
      name: '滨海新区',
      citySid: '1201',
      provinceSid: '12'
    },
    {
      sid: '120117',
      name: '宁河区',
      citySid: '1201',
      provinceSid: '12'
    },
    {
      sid: '120118',
      name: '静海区',
      citySid: '1201',
      provinceSid: '12'
    },
    {
      sid: '120119',
      name: '蓟州区',
      citySid: '1201',
      provinceSid: '12'
    },
    {
      sid: '130102',
      name: '长安区',
      citySid: '1301',
      provinceSid: '13'
    },
    {
      sid: '130104',
      name: '桥西区',
      citySid: '1301',
      provinceSid: '13'
    },
    {
      sid: '130105',
      name: '新华区',
      citySid: '1301',
      provinceSid: '13'
    },
    {
      sid: '130107',
      name: '井陉矿区',
      citySid: '1301',
      provinceSid: '13'
    },
    {
      sid: '130108',
      name: '裕华区',
      citySid: '1301',
      provinceSid: '13'
    },
    {
      sid: '130109',
      name: '藁城区',
      citySid: '1301',
      provinceSid: '13'
    },
    {
      sid: '130110',
      name: '鹿泉区',
      citySid: '1301',
      provinceSid: '13'
    },
    {
      sid: '130111',
      name: '栾城区',
      citySid: '1301',
      provinceSid: '13'
    },
    {
      sid: '130121',
      name: '井陉县',
      citySid: '1301',
      provinceSid: '13'
    },
    {
      sid: '130123',
      name: '正定县',
      citySid: '1301',
      provinceSid: '13'
    },
    {
      sid: '130125',
      name: '行唐县',
      citySid: '1301',
      provinceSid: '13'
    },
    {
      sid: '130126',
      name: '灵寿县',
      citySid: '1301',
      provinceSid: '13'
    },
    {
      sid: '130127',
      name: '高邑县',
      citySid: '1301',
      provinceSid: '13'
    },
    {
      sid: '130128',
      name: '深泽县',
      citySid: '1301',
      provinceSid: '13'
    },
    {
      sid: '130129',
      name: '赞皇县',
      citySid: '1301',
      provinceSid: '13'
    },
    {
      sid: '130130',
      name: '无极县',
      citySid: '1301',
      provinceSid: '13'
    },
    {
      sid: '130131',
      name: '平山县',
      citySid: '1301',
      provinceSid: '13'
    },
    {
      sid: '130132',
      name: '元氏县',
      citySid: '1301',
      provinceSid: '13'
    },
    {
      sid: '130133',
      name: '赵县',
      citySid: '1301',
      provinceSid: '13'
    },
    {
      sid: '130171',
      name: '石家庄高新技术产业开发区',
      citySid: '1301',
      provinceSid: '13'
    },
    {
      sid: '130172',
      name: '石家庄循环化工园区',
      citySid: '1301',
      provinceSid: '13'
    },
    {
      sid: '130181',
      name: '辛集市',
      citySid: '1301',
      provinceSid: '13'
    },
    {
      sid: '130183',
      name: '晋州市',
      citySid: '1301',
      provinceSid: '13'
    },
    {
      sid: '130184',
      name: '新乐市',
      citySid: '1301',
      provinceSid: '13'
    },
    {
      sid: '130202',
      name: '路南区',
      citySid: '1302',
      provinceSid: '13'
    },
    {
      sid: '130203',
      name: '路北区',
      citySid: '1302',
      provinceSid: '13'
    },
    {
      sid: '130204',
      name: '古冶区',
      citySid: '1302',
      provinceSid: '13'
    },
    {
      sid: '130205',
      name: '开平区',
      citySid: '1302',
      provinceSid: '13'
    },
    {
      sid: '130207',
      name: '丰南区',
      citySid: '1302',
      provinceSid: '13'
    },
    {
      sid: '130208',
      name: '丰润区',
      citySid: '1302',
      provinceSid: '13'
    },
    {
      sid: '130209',
      name: '曹妃甸区',
      citySid: '1302',
      provinceSid: '13'
    },
    {
      sid: '130223',
      name: '滦县',
      citySid: '1302',
      provinceSid: '13'
    },
    {
      sid: '130224',
      name: '滦南县',
      citySid: '1302',
      provinceSid: '13'
    },
    {
      sid: '130225',
      name: '乐亭县',
      citySid: '1302',
      provinceSid: '13'
    },
    {
      sid: '130227',
      name: '迁西县',
      citySid: '1302',
      provinceSid: '13'
    },
    {
      sid: '130229',
      name: '玉田县',
      citySid: '1302',
      provinceSid: '13'
    },
    {
      sid: '130271',
      name: '唐山市芦台经济技术开发区',
      citySid: '1302',
      provinceSid: '13'
    },
    {
      sid: '130272',
      name: '唐山市汉沽管理区',
      citySid: '1302',
      provinceSid: '13'
    },
    {
      sid: '130273',
      name: '唐山高新技术产业开发区',
      citySid: '1302',
      provinceSid: '13'
    },
    {
      sid: '130274',
      name: '河北唐山海港经济开发区',
      citySid: '1302',
      provinceSid: '13'
    },
    {
      sid: '130281',
      name: '遵化市',
      citySid: '1302',
      provinceSid: '13'
    },
    {
      sid: '130283',
      name: '迁安市',
      citySid: '1302',
      provinceSid: '13'
    },
    {
      sid: '130302',
      name: '海港区',
      citySid: '1303',
      provinceSid: '13'
    },
    {
      sid: '130303',
      name: '山海关区',
      citySid: '1303',
      provinceSid: '13'
    },
    {
      sid: '130304',
      name: '北戴河区',
      citySid: '1303',
      provinceSid: '13'
    },
    {
      sid: '130306',
      name: '抚宁区',
      citySid: '1303',
      provinceSid: '13'
    },
    {
      sid: '130321',
      name: '青龙满族自治县',
      citySid: '1303',
      provinceSid: '13'
    },
    {
      sid: '130322',
      name: '昌黎县',
      citySid: '1303',
      provinceSid: '13'
    },
    {
      sid: '130324',
      name: '卢龙县',
      citySid: '1303',
      provinceSid: '13'
    },
    {
      sid: '130371',
      name: '秦皇岛市经济技术开发区',
      citySid: '1303',
      provinceSid: '13'
    },
    {
      sid: '130372',
      name: '北戴河新区',
      citySid: '1303',
      provinceSid: '13'
    },
    {
      sid: '130402',
      name: '邯山区',
      citySid: '1304',
      provinceSid: '13'
    },
    {
      sid: '130403',
      name: '丛台区',
      citySid: '1304',
      provinceSid: '13'
    },
    {
      sid: '130404',
      name: '复兴区',
      citySid: '1304',
      provinceSid: '13'
    },
    {
      sid: '130406',
      name: '峰峰矿区',
      citySid: '1304',
      provinceSid: '13'
    },
    {
      sid: '130407',
      name: '肥乡区',
      citySid: '1304',
      provinceSid: '13'
    },
    {
      sid: '130408',
      name: '永年区',
      citySid: '1304',
      provinceSid: '13'
    },
    {
      sid: '130423',
      name: '临漳县',
      citySid: '1304',
      provinceSid: '13'
    },
    {
      sid: '130424',
      name: '成安县',
      citySid: '1304',
      provinceSid: '13'
    },
    {
      sid: '130425',
      name: '大名县',
      citySid: '1304',
      provinceSid: '13'
    },
    {
      sid: '130426',
      name: '涉县',
      citySid: '1304',
      provinceSid: '13'
    },
    {
      sid: '130427',
      name: '磁县',
      citySid: '1304',
      provinceSid: '13'
    },
    {
      sid: '130430',
      name: '邱县',
      citySid: '1304',
      provinceSid: '13'
    },
    {
      sid: '130431',
      name: '鸡泽县',
      citySid: '1304',
      provinceSid: '13'
    },
    {
      sid: '130432',
      name: '广平县',
      citySid: '1304',
      provinceSid: '13'
    },
    {
      sid: '130433',
      name: '馆陶县',
      citySid: '1304',
      provinceSid: '13'
    },
    {
      sid: '130434',
      name: '魏县',
      citySid: '1304',
      provinceSid: '13'
    },
    {
      sid: '130435',
      name: '曲周县',
      citySid: '1304',
      provinceSid: '13'
    },
    {
      sid: '130471',
      name: '邯郸经济技术开发区',
      citySid: '1304',
      provinceSid: '13'
    },
    {
      sid: '130473',
      name: '邯郸冀南新区',
      citySid: '1304',
      provinceSid: '13'
    },
    {
      sid: '130481',
      name: '武安市',
      citySid: '1304',
      provinceSid: '13'
    },
    {
      sid: '130502',
      name: '桥东区',
      citySid: '1305',
      provinceSid: '13'
    },
    {
      sid: '130503',
      name: '桥西区',
      citySid: '1305',
      provinceSid: '13'
    },
    {
      sid: '130521',
      name: '邢台县',
      citySid: '1305',
      provinceSid: '13'
    },
    {
      sid: '130522',
      name: '临城县',
      citySid: '1305',
      provinceSid: '13'
    },
    {
      sid: '130523',
      name: '内丘县',
      citySid: '1305',
      provinceSid: '13'
    },
    {
      sid: '130524',
      name: '柏乡县',
      citySid: '1305',
      provinceSid: '13'
    },
    {
      sid: '130525',
      name: '隆尧县',
      citySid: '1305',
      provinceSid: '13'
    },
    {
      sid: '130526',
      name: '任县',
      citySid: '1305',
      provinceSid: '13'
    },
    {
      sid: '130527',
      name: '南和县',
      citySid: '1305',
      provinceSid: '13'
    },
    {
      sid: '130528',
      name: '宁晋县',
      citySid: '1305',
      provinceSid: '13'
    },
    {
      sid: '130529',
      name: '巨鹿县',
      citySid: '1305',
      provinceSid: '13'
    },
    {
      sid: '130530',
      name: '新河县',
      citySid: '1305',
      provinceSid: '13'
    },
    {
      sid: '130531',
      name: '广宗县',
      citySid: '1305',
      provinceSid: '13'
    },
    {
      sid: '130532',
      name: '平乡县',
      citySid: '1305',
      provinceSid: '13'
    },
    {
      sid: '130533',
      name: '威县',
      citySid: '1305',
      provinceSid: '13'
    },
    {
      sid: '130534',
      name: '清河县',
      citySid: '1305',
      provinceSid: '13'
    },
    {
      sid: '130535',
      name: '临西县',
      citySid: '1305',
      provinceSid: '13'
    },
    {
      sid: '130571',
      name: '河北邢台经济开发区',
      citySid: '1305',
      provinceSid: '13'
    },
    {
      sid: '130581',
      name: '南宫市',
      citySid: '1305',
      provinceSid: '13'
    },
    {
      sid: '130582',
      name: '沙河市',
      citySid: '1305',
      provinceSid: '13'
    },
    {
      sid: '130602',
      name: '竞秀区',
      citySid: '1306',
      provinceSid: '13'
    },
    {
      sid: '130606',
      name: '莲池区',
      citySid: '1306',
      provinceSid: '13'
    },
    {
      sid: '130607',
      name: '满城区',
      citySid: '1306',
      provinceSid: '13'
    },
    {
      sid: '130608',
      name: '清苑区',
      citySid: '1306',
      provinceSid: '13'
    },
    {
      sid: '130609',
      name: '徐水区',
      citySid: '1306',
      provinceSid: '13'
    },
    {
      sid: '130623',
      name: '涞水县',
      citySid: '1306',
      provinceSid: '13'
    },
    {
      sid: '130624',
      name: '阜平县',
      citySid: '1306',
      provinceSid: '13'
    },
    {
      sid: '130626',
      name: '定兴县',
      citySid: '1306',
      provinceSid: '13'
    },
    {
      sid: '130627',
      name: '唐县',
      citySid: '1306',
      provinceSid: '13'
    },
    {
      sid: '130628',
      name: '高阳县',
      citySid: '1306',
      provinceSid: '13'
    },
    {
      sid: '130629',
      name: '容城县',
      citySid: '1306',
      provinceSid: '13'
    },
    {
      sid: '130630',
      name: '涞源县',
      citySid: '1306',
      provinceSid: '13'
    },
    {
      sid: '130631',
      name: '望都县',
      citySid: '1306',
      provinceSid: '13'
    },
    {
      sid: '130632',
      name: '安新县',
      citySid: '1306',
      provinceSid: '13'
    },
    {
      sid: '130633',
      name: '易县',
      citySid: '1306',
      provinceSid: '13'
    },
    {
      sid: '130634',
      name: '曲阳县',
      citySid: '1306',
      provinceSid: '13'
    },
    {
      sid: '130635',
      name: '蠡县',
      citySid: '1306',
      provinceSid: '13'
    },
    {
      sid: '130636',
      name: '顺平县',
      citySid: '1306',
      provinceSid: '13'
    },
    {
      sid: '130637',
      name: '博野县',
      citySid: '1306',
      provinceSid: '13'
    },
    {
      sid: '130638',
      name: '雄县',
      citySid: '1306',
      provinceSid: '13'
    },
    {
      sid: '130671',
      name: '保定高新技术产业开发区',
      citySid: '1306',
      provinceSid: '13'
    },
    {
      sid: '130672',
      name: '保定白沟新城',
      citySid: '1306',
      provinceSid: '13'
    },
    {
      sid: '130681',
      name: '涿州市',
      citySid: '1306',
      provinceSid: '13'
    },
    {
      sid: '130682',
      name: '定州市',
      citySid: '1306',
      provinceSid: '13'
    },
    {
      sid: '130683',
      name: '安国市',
      citySid: '1306',
      provinceSid: '13'
    },
    {
      sid: '130684',
      name: '高碑店市',
      citySid: '1306',
      provinceSid: '13'
    },
    {
      sid: '130702',
      name: '桥东区',
      citySid: '1307',
      provinceSid: '13'
    },
    {
      sid: '130703',
      name: '桥西区',
      citySid: '1307',
      provinceSid: '13'
    },
    {
      sid: '130705',
      name: '宣化区',
      citySid: '1307',
      provinceSid: '13'
    },
    {
      sid: '130706',
      name: '下花园区',
      citySid: '1307',
      provinceSid: '13'
    },
    {
      sid: '130708',
      name: '万全区',
      citySid: '1307',
      provinceSid: '13'
    },
    {
      sid: '130709',
      name: '崇礼区',
      citySid: '1307',
      provinceSid: '13'
    },
    {
      sid: '130722',
      name: '张北县',
      citySid: '1307',
      provinceSid: '13'
    },
    {
      sid: '130723',
      name: '康保县',
      citySid: '1307',
      provinceSid: '13'
    },
    {
      sid: '130724',
      name: '沽源县',
      citySid: '1307',
      provinceSid: '13'
    },
    {
      sid: '130725',
      name: '尚义县',
      citySid: '1307',
      provinceSid: '13'
    },
    {
      sid: '130726',
      name: '蔚县',
      citySid: '1307',
      provinceSid: '13'
    },
    {
      sid: '130727',
      name: '阳原县',
      citySid: '1307',
      provinceSid: '13'
    },
    {
      sid: '130728',
      name: '怀安县',
      citySid: '1307',
      provinceSid: '13'
    },
    {
      sid: '130730',
      name: '怀来县',
      citySid: '1307',
      provinceSid: '13'
    },
    {
      sid: '130731',
      name: '涿鹿县',
      citySid: '1307',
      provinceSid: '13'
    },
    {
      sid: '130732',
      name: '赤城县',
      citySid: '1307',
      provinceSid: '13'
    },
    {
      sid: '130771',
      name: '张家口市高新技术产业开发区',
      citySid: '1307',
      provinceSid: '13'
    },
    {
      sid: '130772',
      name: '张家口市察北管理区',
      citySid: '1307',
      provinceSid: '13'
    },
    {
      sid: '130773',
      name: '张家口市塞北管理区',
      citySid: '1307',
      provinceSid: '13'
    },
    {
      sid: '130802',
      name: '双桥区',
      citySid: '1308',
      provinceSid: '13'
    },
    {
      sid: '130803',
      name: '双滦区',
      citySid: '1308',
      provinceSid: '13'
    },
    {
      sid: '130804',
      name: '鹰手营子矿区',
      citySid: '1308',
      provinceSid: '13'
    },
    {
      sid: '130821',
      name: '承德县',
      citySid: '1308',
      provinceSid: '13'
    },
    {
      sid: '130822',
      name: '兴隆县',
      citySid: '1308',
      provinceSid: '13'
    },
    {
      sid: '130824',
      name: '滦平县',
      citySid: '1308',
      provinceSid: '13'
    },
    {
      sid: '130825',
      name: '隆化县',
      citySid: '1308',
      provinceSid: '13'
    },
    {
      sid: '130826',
      name: '丰宁满族自治县',
      citySid: '1308',
      provinceSid: '13'
    },
    {
      sid: '130827',
      name: '宽城满族自治县',
      citySid: '1308',
      provinceSid: '13'
    },
    {
      sid: '130828',
      name: '围场满族蒙古族自治县',
      citySid: '1308',
      provinceSid: '13'
    },
    {
      sid: '130871',
      name: '承德高新技术产业开发区',
      citySid: '1308',
      provinceSid: '13'
    },
    {
      sid: '130881',
      name: '平泉市',
      citySid: '1308',
      provinceSid: '13'
    },
    {
      sid: '130902',
      name: '新华区',
      citySid: '1309',
      provinceSid: '13'
    },
    {
      sid: '130903',
      name: '运河区',
      citySid: '1309',
      provinceSid: '13'
    },
    {
      sid: '130921',
      name: '沧县',
      citySid: '1309',
      provinceSid: '13'
    },
    {
      sid: '130922',
      name: '青县',
      citySid: '1309',
      provinceSid: '13'
    },
    {
      sid: '130923',
      name: '东光县',
      citySid: '1309',
      provinceSid: '13'
    },
    {
      sid: '130924',
      name: '海兴县',
      citySid: '1309',
      provinceSid: '13'
    },
    {
      sid: '130925',
      name: '盐山县',
      citySid: '1309',
      provinceSid: '13'
    },
    {
      sid: '130926',
      name: '肃宁县',
      citySid: '1309',
      provinceSid: '13'
    },
    {
      sid: '130927',
      name: '南皮县',
      citySid: '1309',
      provinceSid: '13'
    },
    {
      sid: '130928',
      name: '吴桥县',
      citySid: '1309',
      provinceSid: '13'
    },
    {
      sid: '130929',
      name: '献县',
      citySid: '1309',
      provinceSid: '13'
    },
    {
      sid: '130930',
      name: '孟村回族自治县',
      citySid: '1309',
      provinceSid: '13'
    },
    {
      sid: '130971',
      name: '河北沧州经济开发区',
      citySid: '1309',
      provinceSid: '13'
    },
    {
      sid: '130972',
      name: '沧州高新技术产业开发区',
      citySid: '1309',
      provinceSid: '13'
    },
    {
      sid: '130973',
      name: '沧州渤海新区',
      citySid: '1309',
      provinceSid: '13'
    },
    {
      sid: '130981',
      name: '泊头市',
      citySid: '1309',
      provinceSid: '13'
    },
    {
      sid: '130982',
      name: '任丘市',
      citySid: '1309',
      provinceSid: '13'
    },
    {
      sid: '130983',
      name: '黄骅市',
      citySid: '1309',
      provinceSid: '13'
    },
    {
      sid: '130984',
      name: '河间市',
      citySid: '1309',
      provinceSid: '13'
    },
    {
      sid: '131002',
      name: '安次区',
      citySid: '1310',
      provinceSid: '13'
    },
    {
      sid: '131003',
      name: '广阳区',
      citySid: '1310',
      provinceSid: '13'
    },
    {
      sid: '131022',
      name: '固安县',
      citySid: '1310',
      provinceSid: '13'
    },
    {
      sid: '131023',
      name: '永清县',
      citySid: '1310',
      provinceSid: '13'
    },
    {
      sid: '131024',
      name: '香河县',
      citySid: '1310',
      provinceSid: '13'
    },
    {
      sid: '131025',
      name: '大城县',
      citySid: '1310',
      provinceSid: '13'
    },
    {
      sid: '131026',
      name: '文安县',
      citySid: '1310',
      provinceSid: '13'
    },
    {
      sid: '131028',
      name: '大厂回族自治县',
      citySid: '1310',
      provinceSid: '13'
    },
    {
      sid: '131071',
      name: '廊坊经济技术开发区',
      citySid: '1310',
      provinceSid: '13'
    },
    {
      sid: '131081',
      name: '霸州市',
      citySid: '1310',
      provinceSid: '13'
    },
    {
      sid: '131082',
      name: '三河市',
      citySid: '1310',
      provinceSid: '13'
    },
    {
      sid: '131102',
      name: '桃城区',
      citySid: '1311',
      provinceSid: '13'
    },
    {
      sid: '131103',
      name: '冀州区',
      citySid: '1311',
      provinceSid: '13'
    },
    {
      sid: '131121',
      name: '枣强县',
      citySid: '1311',
      provinceSid: '13'
    },
    {
      sid: '131122',
      name: '武邑县',
      citySid: '1311',
      provinceSid: '13'
    },
    {
      sid: '131123',
      name: '武强县',
      citySid: '1311',
      provinceSid: '13'
    },
    {
      sid: '131124',
      name: '饶阳县',
      citySid: '1311',
      provinceSid: '13'
    },
    {
      sid: '131125',
      name: '安平县',
      citySid: '1311',
      provinceSid: '13'
    },
    {
      sid: '131126',
      name: '故城县',
      citySid: '1311',
      provinceSid: '13'
    },
    {
      sid: '131127',
      name: '景县',
      citySid: '1311',
      provinceSid: '13'
    },
    {
      sid: '131128',
      name: '阜城县',
      citySid: '1311',
      provinceSid: '13'
    },
    {
      sid: '131171',
      name: '河北衡水经济开发区',
      citySid: '1311',
      provinceSid: '13'
    },
    {
      sid: '131172',
      name: '衡水滨湖新区',
      citySid: '1311',
      provinceSid: '13'
    },
    {
      sid: '131182',
      name: '深州市',
      citySid: '1311',
      provinceSid: '13'
    },
    {
      sid: '140105',
      name: '小店区',
      citySid: '1401',
      provinceSid: '14'
    },
    {
      sid: '140106',
      name: '迎泽区',
      citySid: '1401',
      provinceSid: '14'
    },
    {
      sid: '140107',
      name: '杏花岭区',
      citySid: '1401',
      provinceSid: '14'
    },
    {
      sid: '140108',
      name: '尖草坪区',
      citySid: '1401',
      provinceSid: '14'
    },
    {
      sid: '140109',
      name: '万柏林区',
      citySid: '1401',
      provinceSid: '14'
    },
    {
      sid: '140110',
      name: '晋源区',
      citySid: '1401',
      provinceSid: '14'
    },
    {
      sid: '140121',
      name: '清徐县',
      citySid: '1401',
      provinceSid: '14'
    },
    {
      sid: '140122',
      name: '阳曲县',
      citySid: '1401',
      provinceSid: '14'
    },
    {
      sid: '140123',
      name: '娄烦县',
      citySid: '1401',
      provinceSid: '14'
    },
    {
      sid: '140171',
      name: '山西转型综合改革示范区',
      citySid: '1401',
      provinceSid: '14'
    },
    {
      sid: '140181',
      name: '古交市',
      citySid: '1401',
      provinceSid: '14'
    },
    {
      sid: '140202',
      name: '城区',
      citySid: '1402',
      provinceSid: '14'
    },
    {
      sid: '140203',
      name: '矿区',
      citySid: '1402',
      provinceSid: '14'
    },
    {
      sid: '140211',
      name: '南郊区',
      citySid: '1402',
      provinceSid: '14'
    },
    {
      sid: '140212',
      name: '新荣区',
      citySid: '1402',
      provinceSid: '14'
    },
    {
      sid: '140221',
      name: '阳高县',
      citySid: '1402',
      provinceSid: '14'
    },
    {
      sid: '140222',
      name: '天镇县',
      citySid: '1402',
      provinceSid: '14'
    },
    {
      sid: '140223',
      name: '广灵县',
      citySid: '1402',
      provinceSid: '14'
    },
    {
      sid: '140224',
      name: '灵丘县',
      citySid: '1402',
      provinceSid: '14'
    },
    {
      sid: '140225',
      name: '浑源县',
      citySid: '1402',
      provinceSid: '14'
    },
    {
      sid: '140226',
      name: '左云县',
      citySid: '1402',
      provinceSid: '14'
    },
    {
      sid: '140227',
      name: '大同县',
      citySid: '1402',
      provinceSid: '14'
    },
    {
      sid: '140271',
      name: '山西大同经济开发区',
      citySid: '1402',
      provinceSid: '14'
    },
    {
      sid: '140302',
      name: '城区',
      citySid: '1403',
      provinceSid: '14'
    },
    {
      sid: '140303',
      name: '矿区',
      citySid: '1403',
      provinceSid: '14'
    },
    {
      sid: '140311',
      name: '郊区',
      citySid: '1403',
      provinceSid: '14'
    },
    {
      sid: '140321',
      name: '平定县',
      citySid: '1403',
      provinceSid: '14'
    },
    {
      sid: '140322',
      name: '盂县',
      citySid: '1403',
      provinceSid: '14'
    },
    {
      sid: '140371',
      name: '山西阳泉经济开发区',
      citySid: '1403',
      provinceSid: '14'
    },
    {
      sid: '140402',
      name: '城区',
      citySid: '1404',
      provinceSid: '14'
    },
    {
      sid: '140411',
      name: '郊区',
      citySid: '1404',
      provinceSid: '14'
    },
    {
      sid: '140421',
      name: '长治县',
      citySid: '1404',
      provinceSid: '14'
    },
    {
      sid: '140423',
      name: '襄垣县',
      citySid: '1404',
      provinceSid: '14'
    },
    {
      sid: '140424',
      name: '屯留县',
      citySid: '1404',
      provinceSid: '14'
    },
    {
      sid: '140425',
      name: '平顺县',
      citySid: '1404',
      provinceSid: '14'
    },
    {
      sid: '140426',
      name: '黎城县',
      citySid: '1404',
      provinceSid: '14'
    },
    {
      sid: '140427',
      name: '壶关县',
      citySid: '1404',
      provinceSid: '14'
    },
    {
      sid: '140428',
      name: '长子县',
      citySid: '1404',
      provinceSid: '14'
    },
    {
      sid: '140429',
      name: '武乡县',
      citySid: '1404',
      provinceSid: '14'
    },
    {
      sid: '140430',
      name: '沁县',
      citySid: '1404',
      provinceSid: '14'
    },
    {
      sid: '140431',
      name: '沁源县',
      citySid: '1404',
      provinceSid: '14'
    },
    {
      sid: '140471',
      name: '山西长治高新技术产业园区',
      citySid: '1404',
      provinceSid: '14'
    },
    {
      sid: '140481',
      name: '潞城市',
      citySid: '1404',
      provinceSid: '14'
    },
    {
      sid: '140502',
      name: '城区',
      citySid: '1405',
      provinceSid: '14'
    },
    {
      sid: '140521',
      name: '沁水县',
      citySid: '1405',
      provinceSid: '14'
    },
    {
      sid: '140522',
      name: '阳城县',
      citySid: '1405',
      provinceSid: '14'
    },
    {
      sid: '140524',
      name: '陵川县',
      citySid: '1405',
      provinceSid: '14'
    },
    {
      sid: '140525',
      name: '泽州县',
      citySid: '1405',
      provinceSid: '14'
    },
    {
      sid: '140581',
      name: '高平市',
      citySid: '1405',
      provinceSid: '14'
    },
    {
      sid: '140602',
      name: '朔城区',
      citySid: '1406',
      provinceSid: '14'
    },
    {
      sid: '140603',
      name: '平鲁区',
      citySid: '1406',
      provinceSid: '14'
    },
    {
      sid: '140621',
      name: '山阴县',
      citySid: '1406',
      provinceSid: '14'
    },
    {
      sid: '140622',
      name: '应县',
      citySid: '1406',
      provinceSid: '14'
    },
    {
      sid: '140623',
      name: '右玉县',
      citySid: '1406',
      provinceSid: '14'
    },
    {
      sid: '140624',
      name: '怀仁县',
      citySid: '1406',
      provinceSid: '14'
    },
    {
      sid: '140671',
      name: '山西朔州经济开发区',
      citySid: '1406',
      provinceSid: '14'
    },
    {
      sid: '140702',
      name: '榆次区',
      citySid: '1407',
      provinceSid: '14'
    },
    {
      sid: '140721',
      name: '榆社县',
      citySid: '1407',
      provinceSid: '14'
    },
    {
      sid: '140722',
      name: '左权县',
      citySid: '1407',
      provinceSid: '14'
    },
    {
      sid: '140723',
      name: '和顺县',
      citySid: '1407',
      provinceSid: '14'
    },
    {
      sid: '140724',
      name: '昔阳县',
      citySid: '1407',
      provinceSid: '14'
    },
    {
      sid: '140725',
      name: '寿阳县',
      citySid: '1407',
      provinceSid: '14'
    },
    {
      sid: '140726',
      name: '太谷县',
      citySid: '1407',
      provinceSid: '14'
    },
    {
      sid: '140727',
      name: '祁县',
      citySid: '1407',
      provinceSid: '14'
    },
    {
      sid: '140728',
      name: '平遥县',
      citySid: '1407',
      provinceSid: '14'
    },
    {
      sid: '140729',
      name: '灵石县',
      citySid: '1407',
      provinceSid: '14'
    },
    {
      sid: '140781',
      name: '介休市',
      citySid: '1407',
      provinceSid: '14'
    },
    {
      sid: '140802',
      name: '盐湖区',
      citySid: '1408',
      provinceSid: '14'
    },
    {
      sid: '140821',
      name: '临猗县',
      citySid: '1408',
      provinceSid: '14'
    },
    {
      sid: '140822',
      name: '万荣县',
      citySid: '1408',
      provinceSid: '14'
    },
    {
      sid: '140823',
      name: '闻喜县',
      citySid: '1408',
      provinceSid: '14'
    },
    {
      sid: '140824',
      name: '稷山县',
      citySid: '1408',
      provinceSid: '14'
    },
    {
      sid: '140825',
      name: '新绛县',
      citySid: '1408',
      provinceSid: '14'
    },
    {
      sid: '140826',
      name: '绛县',
      citySid: '1408',
      provinceSid: '14'
    },
    {
      sid: '140827',
      name: '垣曲县',
      citySid: '1408',
      provinceSid: '14'
    },
    {
      sid: '140828',
      name: '夏县',
      citySid: '1408',
      provinceSid: '14'
    },
    {
      sid: '140829',
      name: '平陆县',
      citySid: '1408',
      provinceSid: '14'
    },
    {
      sid: '140830',
      name: '芮城县',
      citySid: '1408',
      provinceSid: '14'
    },
    {
      sid: '140881',
      name: '永济市',
      citySid: '1408',
      provinceSid: '14'
    },
    {
      sid: '140882',
      name: '河津市',
      citySid: '1408',
      provinceSid: '14'
    },
    {
      sid: '140902',
      name: '忻府区',
      citySid: '1409',
      provinceSid: '14'
    },
    {
      sid: '140921',
      name: '定襄县',
      citySid: '1409',
      provinceSid: '14'
    },
    {
      sid: '140922',
      name: '五台县',
      citySid: '1409',
      provinceSid: '14'
    },
    {
      sid: '140923',
      name: '代县',
      citySid: '1409',
      provinceSid: '14'
    },
    {
      sid: '140924',
      name: '繁峙县',
      citySid: '1409',
      provinceSid: '14'
    },
    {
      sid: '140925',
      name: '宁武县',
      citySid: '1409',
      provinceSid: '14'
    },
    {
      sid: '140926',
      name: '静乐县',
      citySid: '1409',
      provinceSid: '14'
    },
    {
      sid: '140927',
      name: '神池县',
      citySid: '1409',
      provinceSid: '14'
    },
    {
      sid: '140928',
      name: '五寨县',
      citySid: '1409',
      provinceSid: '14'
    },
    {
      sid: '140929',
      name: '岢岚县',
      citySid: '1409',
      provinceSid: '14'
    },
    {
      sid: '140930',
      name: '河曲县',
      citySid: '1409',
      provinceSid: '14'
    },
    {
      sid: '140931',
      name: '保德县',
      citySid: '1409',
      provinceSid: '14'
    },
    {
      sid: '140932',
      name: '偏关县',
      citySid: '1409',
      provinceSid: '14'
    },
    {
      sid: '140971',
      name: '五台山风景名胜区',
      citySid: '1409',
      provinceSid: '14'
    },
    {
      sid: '140981',
      name: '原平市',
      citySid: '1409',
      provinceSid: '14'
    },
    {
      sid: '141002',
      name: '尧都区',
      citySid: '1410',
      provinceSid: '14'
    },
    {
      sid: '141021',
      name: '曲沃县',
      citySid: '1410',
      provinceSid: '14'
    },
    {
      sid: '141022',
      name: '翼城县',
      citySid: '1410',
      provinceSid: '14'
    },
    {
      sid: '141023',
      name: '襄汾县',
      citySid: '1410',
      provinceSid: '14'
    },
    {
      sid: '141024',
      name: '洪洞县',
      citySid: '1410',
      provinceSid: '14'
    },
    {
      sid: '141025',
      name: '古县',
      citySid: '1410',
      provinceSid: '14'
    },
    {
      sid: '141026',
      name: '安泽县',
      citySid: '1410',
      provinceSid: '14'
    },
    {
      sid: '141027',
      name: '浮山县',
      citySid: '1410',
      provinceSid: '14'
    },
    {
      sid: '141028',
      name: '吉县',
      citySid: '1410',
      provinceSid: '14'
    },
    {
      sid: '141029',
      name: '乡宁县',
      citySid: '1410',
      provinceSid: '14'
    },
    {
      sid: '141030',
      name: '大宁县',
      citySid: '1410',
      provinceSid: '14'
    },
    {
      sid: '141031',
      name: '隰县',
      citySid: '1410',
      provinceSid: '14'
    },
    {
      sid: '141032',
      name: '永和县',
      citySid: '1410',
      provinceSid: '14'
    },
    {
      sid: '141033',
      name: '蒲县',
      citySid: '1410',
      provinceSid: '14'
    },
    {
      sid: '141034',
      name: '汾西县',
      citySid: '1410',
      provinceSid: '14'
    },
    {
      sid: '141081',
      name: '侯马市',
      citySid: '1410',
      provinceSid: '14'
    },
    {
      sid: '141082',
      name: '霍州市',
      citySid: '1410',
      provinceSid: '14'
    },
    {
      sid: '141102',
      name: '离石区',
      citySid: '1411',
      provinceSid: '14'
    },
    {
      sid: '141121',
      name: '文水县',
      citySid: '1411',
      provinceSid: '14'
    },
    {
      sid: '141122',
      name: '交城县',
      citySid: '1411',
      provinceSid: '14'
    },
    {
      sid: '141123',
      name: '兴县',
      citySid: '1411',
      provinceSid: '14'
    },
    {
      sid: '141124',
      name: '临县',
      citySid: '1411',
      provinceSid: '14'
    },
    {
      sid: '141125',
      name: '柳林县',
      citySid: '1411',
      provinceSid: '14'
    },
    {
      sid: '141126',
      name: '石楼县',
      citySid: '1411',
      provinceSid: '14'
    },
    {
      sid: '141127',
      name: '岚县',
      citySid: '1411',
      provinceSid: '14'
    },
    {
      sid: '141128',
      name: '方山县',
      citySid: '1411',
      provinceSid: '14'
    },
    {
      sid: '141129',
      name: '中阳县',
      citySid: '1411',
      provinceSid: '14'
    },
    {
      sid: '141130',
      name: '交口县',
      citySid: '1411',
      provinceSid: '14'
    },
    {
      sid: '141181',
      name: '孝义市',
      citySid: '1411',
      provinceSid: '14'
    },
    {
      sid: '141182',
      name: '汾阳市',
      citySid: '1411',
      provinceSid: '14'
    },
    {
      sid: '150102',
      name: '新城区',
      citySid: '1501',
      provinceSid: '15'
    },
    {
      sid: '150103',
      name: '回民区',
      citySid: '1501',
      provinceSid: '15'
    },
    {
      sid: '150104',
      name: '玉泉区',
      citySid: '1501',
      provinceSid: '15'
    },
    {
      sid: '150105',
      name: '赛罕区',
      citySid: '1501',
      provinceSid: '15'
    },
    {
      sid: '150121',
      name: '土默特左旗',
      citySid: '1501',
      provinceSid: '15'
    },
    {
      sid: '150122',
      name: '托克托县',
      citySid: '1501',
      provinceSid: '15'
    },
    {
      sid: '150123',
      name: '和林格尔县',
      citySid: '1501',
      provinceSid: '15'
    },
    {
      sid: '150124',
      name: '清水河县',
      citySid: '1501',
      provinceSid: '15'
    },
    {
      sid: '150125',
      name: '武川县',
      citySid: '1501',
      provinceSid: '15'
    },
    {
      sid: '150171',
      name: '呼和浩特金海工业园区',
      citySid: '1501',
      provinceSid: '15'
    },
    {
      sid: '150172',
      name: '呼和浩特经济技术开发区',
      citySid: '1501',
      provinceSid: '15'
    },
    {
      sid: '150202',
      name: '东河区',
      citySid: '1502',
      provinceSid: '15'
    },
    {
      sid: '150203',
      name: '昆都仑区',
      citySid: '1502',
      provinceSid: '15'
    },
    {
      sid: '150204',
      name: '青山区',
      citySid: '1502',
      provinceSid: '15'
    },
    {
      sid: '150205',
      name: '石拐区',
      citySid: '1502',
      provinceSid: '15'
    },
    {
      sid: '150206',
      name: '白云鄂博矿区',
      citySid: '1502',
      provinceSid: '15'
    },
    {
      sid: '150207',
      name: '九原区',
      citySid: '1502',
      provinceSid: '15'
    },
    {
      sid: '150221',
      name: '土默特右旗',
      citySid: '1502',
      provinceSid: '15'
    },
    {
      sid: '150222',
      name: '固阳县',
      citySid: '1502',
      provinceSid: '15'
    },
    {
      sid: '150223',
      name: '达尔罕茂明安联合旗',
      citySid: '1502',
      provinceSid: '15'
    },
    {
      sid: '150271',
      name: '包头稀土高新技术产业开发区',
      citySid: '1502',
      provinceSid: '15'
    },
    {
      sid: '150302',
      name: '海勃湾区',
      citySid: '1503',
      provinceSid: '15'
    },
    {
      sid: '150303',
      name: '海南区',
      citySid: '1503',
      provinceSid: '15'
    },
    {
      sid: '150304',
      name: '乌达区',
      citySid: '1503',
      provinceSid: '15'
    },
    {
      sid: '150402',
      name: '红山区',
      citySid: '1504',
      provinceSid: '15'
    },
    {
      sid: '150403',
      name: '元宝山区',
      citySid: '1504',
      provinceSid: '15'
    },
    {
      sid: '150404',
      name: '松山区',
      citySid: '1504',
      provinceSid: '15'
    },
    {
      sid: '150421',
      name: '阿鲁科尔沁旗',
      citySid: '1504',
      provinceSid: '15'
    },
    {
      sid: '150422',
      name: '巴林左旗',
      citySid: '1504',
      provinceSid: '15'
    },
    {
      sid: '150423',
      name: '巴林右旗',
      citySid: '1504',
      provinceSid: '15'
    },
    {
      sid: '150424',
      name: '林西县',
      citySid: '1504',
      provinceSid: '15'
    },
    {
      sid: '150425',
      name: '克什克腾旗',
      citySid: '1504',
      provinceSid: '15'
    },
    {
      sid: '150426',
      name: '翁牛特旗',
      citySid: '1504',
      provinceSid: '15'
    },
    {
      sid: '150428',
      name: '喀喇沁旗',
      citySid: '1504',
      provinceSid: '15'
    },
    {
      sid: '150429',
      name: '宁城县',
      citySid: '1504',
      provinceSid: '15'
    },
    {
      sid: '150430',
      name: '敖汉旗',
      citySid: '1504',
      provinceSid: '15'
    },
    {
      sid: '150502',
      name: '科尔沁区',
      citySid: '1505',
      provinceSid: '15'
    },
    {
      sid: '150521',
      name: '科尔沁左翼中旗',
      citySid: '1505',
      provinceSid: '15'
    },
    {
      sid: '150522',
      name: '科尔沁左翼后旗',
      citySid: '1505',
      provinceSid: '15'
    },
    {
      sid: '150523',
      name: '开鲁县',
      citySid: '1505',
      provinceSid: '15'
    },
    {
      sid: '150524',
      name: '库伦旗',
      citySid: '1505',
      provinceSid: '15'
    },
    {
      sid: '150525',
      name: '奈曼旗',
      citySid: '1505',
      provinceSid: '15'
    },
    {
      sid: '150526',
      name: '扎鲁特旗',
      citySid: '1505',
      provinceSid: '15'
    },
    {
      sid: '150571',
      name: '通辽经济技术开发区',
      citySid: '1505',
      provinceSid: '15'
    },
    {
      sid: '150581',
      name: '霍林郭勒市',
      citySid: '1505',
      provinceSid: '15'
    },
    {
      sid: '150602',
      name: '东胜区',
      citySid: '1506',
      provinceSid: '15'
    },
    {
      sid: '150603',
      name: '康巴什区',
      citySid: '1506',
      provinceSid: '15'
    },
    {
      sid: '150621',
      name: '达拉特旗',
      citySid: '1506',
      provinceSid: '15'
    },
    {
      sid: '150622',
      name: '准格尔旗',
      citySid: '1506',
      provinceSid: '15'
    },
    {
      sid: '150623',
      name: '鄂托克前旗',
      citySid: '1506',
      provinceSid: '15'
    },
    {
      sid: '150624',
      name: '鄂托克旗',
      citySid: '1506',
      provinceSid: '15'
    },
    {
      sid: '150625',
      name: '杭锦旗',
      citySid: '1506',
      provinceSid: '15'
    },
    {
      sid: '150626',
      name: '乌审旗',
      citySid: '1506',
      provinceSid: '15'
    },
    {
      sid: '150627',
      name: '伊金霍洛旗',
      citySid: '1506',
      provinceSid: '15'
    },
    {
      sid: '150702',
      name: '海拉尔区',
      citySid: '1507',
      provinceSid: '15'
    },
    {
      sid: '150703',
      name: '扎赉诺尔区',
      citySid: '1507',
      provinceSid: '15'
    },
    {
      sid: '150721',
      name: '阿荣旗',
      citySid: '1507',
      provinceSid: '15'
    },
    {
      sid: '150722',
      name: '莫力达瓦达斡尔族自治旗',
      citySid: '1507',
      provinceSid: '15'
    },
    {
      sid: '150723',
      name: '鄂伦春自治旗',
      citySid: '1507',
      provinceSid: '15'
    },
    {
      sid: '150724',
      name: '鄂温克族自治旗',
      citySid: '1507',
      provinceSid: '15'
    },
    {
      sid: '150725',
      name: '陈巴尔虎旗',
      citySid: '1507',
      provinceSid: '15'
    },
    {
      sid: '150726',
      name: '新巴尔虎左旗',
      citySid: '1507',
      provinceSid: '15'
    },
    {
      sid: '150727',
      name: '新巴尔虎右旗',
      citySid: '1507',
      provinceSid: '15'
    },
    {
      sid: '150781',
      name: '满洲里市',
      citySid: '1507',
      provinceSid: '15'
    },
    {
      sid: '150782',
      name: '牙克石市',
      citySid: '1507',
      provinceSid: '15'
    },
    {
      sid: '150783',
      name: '扎兰屯市',
      citySid: '1507',
      provinceSid: '15'
    },
    {
      sid: '150784',
      name: '额尔古纳市',
      citySid: '1507',
      provinceSid: '15'
    },
    {
      sid: '150785',
      name: '根河市',
      citySid: '1507',
      provinceSid: '15'
    },
    {
      sid: '150802',
      name: '临河区',
      citySid: '1508',
      provinceSid: '15'
    },
    {
      sid: '150821',
      name: '五原县',
      citySid: '1508',
      provinceSid: '15'
    },
    {
      sid: '150822',
      name: '磴口县',
      citySid: '1508',
      provinceSid: '15'
    },
    {
      sid: '150823',
      name: '乌拉特前旗',
      citySid: '1508',
      provinceSid: '15'
    },
    {
      sid: '150824',
      name: '乌拉特中旗',
      citySid: '1508',
      provinceSid: '15'
    },
    {
      sid: '150825',
      name: '乌拉特后旗',
      citySid: '1508',
      provinceSid: '15'
    },
    {
      sid: '150826',
      name: '杭锦后旗',
      citySid: '1508',
      provinceSid: '15'
    },
    {
      sid: '150902',
      name: '集宁区',
      citySid: '1509',
      provinceSid: '15'
    },
    {
      sid: '150921',
      name: '卓资县',
      citySid: '1509',
      provinceSid: '15'
    },
    {
      sid: '150922',
      name: '化德县',
      citySid: '1509',
      provinceSid: '15'
    },
    {
      sid: '150923',
      name: '商都县',
      citySid: '1509',
      provinceSid: '15'
    },
    {
      sid: '150924',
      name: '兴和县',
      citySid: '1509',
      provinceSid: '15'
    },
    {
      sid: '150925',
      name: '凉城县',
      citySid: '1509',
      provinceSid: '15'
    },
    {
      sid: '150926',
      name: '察哈尔右翼前旗',
      citySid: '1509',
      provinceSid: '15'
    },
    {
      sid: '150927',
      name: '察哈尔右翼中旗',
      citySid: '1509',
      provinceSid: '15'
    },
    {
      sid: '150928',
      name: '察哈尔右翼后旗',
      citySid: '1509',
      provinceSid: '15'
    },
    {
      sid: '150929',
      name: '四子王旗',
      citySid: '1509',
      provinceSid: '15'
    },
    {
      sid: '150981',
      name: '丰镇市',
      citySid: '1509',
      provinceSid: '15'
    },
    {
      sid: '152201',
      name: '乌兰浩特市',
      citySid: '1522',
      provinceSid: '15'
    },
    {
      sid: '152202',
      name: '阿尔山市',
      citySid: '1522',
      provinceSid: '15'
    },
    {
      sid: '152221',
      name: '科尔沁右翼前旗',
      citySid: '1522',
      provinceSid: '15'
    },
    {
      sid: '152222',
      name: '科尔沁右翼中旗',
      citySid: '1522',
      provinceSid: '15'
    },
    {
      sid: '152223',
      name: '扎赉特旗',
      citySid: '1522',
      provinceSid: '15'
    },
    {
      sid: '152224',
      name: '突泉县',
      citySid: '1522',
      provinceSid: '15'
    },
    {
      sid: '152501',
      name: '二连浩特市',
      citySid: '1525',
      provinceSid: '15'
    },
    {
      sid: '152502',
      name: '锡林浩特市',
      citySid: '1525',
      provinceSid: '15'
    },
    {
      sid: '152522',
      name: '阿巴嘎旗',
      citySid: '1525',
      provinceSid: '15'
    },
    {
      sid: '152523',
      name: '苏尼特左旗',
      citySid: '1525',
      provinceSid: '15'
    },
    {
      sid: '152524',
      name: '苏尼特右旗',
      citySid: '1525',
      provinceSid: '15'
    },
    {
      sid: '152525',
      name: '东乌珠穆沁旗',
      citySid: '1525',
      provinceSid: '15'
    },
    {
      sid: '152526',
      name: '西乌珠穆沁旗',
      citySid: '1525',
      provinceSid: '15'
    },
    {
      sid: '152527',
      name: '太仆寺旗',
      citySid: '1525',
      provinceSid: '15'
    },
    {
      sid: '152528',
      name: '镶黄旗',
      citySid: '1525',
      provinceSid: '15'
    },
    {
      sid: '152529',
      name: '正镶白旗',
      citySid: '1525',
      provinceSid: '15'
    },
    {
      sid: '152530',
      name: '正蓝旗',
      citySid: '1525',
      provinceSid: '15'
    },
    {
      sid: '152531',
      name: '多伦县',
      citySid: '1525',
      provinceSid: '15'
    },
    {
      sid: '152571',
      name: '乌拉盖管委会',
      citySid: '1525',
      provinceSid: '15'
    },
    {
      sid: '152921',
      name: '阿拉善左旗',
      citySid: '1529',
      provinceSid: '15'
    },
    {
      sid: '152922',
      name: '阿拉善右旗',
      citySid: '1529',
      provinceSid: '15'
    },
    {
      sid: '152923',
      name: '额济纳旗',
      citySid: '1529',
      provinceSid: '15'
    },
    {
      sid: '152971',
      name: '内蒙古阿拉善经济开发区',
      citySid: '1529',
      provinceSid: '15'
    },
    {
      sid: '210102',
      name: '和平区',
      citySid: '2101',
      provinceSid: '21'
    },
    {
      sid: '210103',
      name: '沈河区',
      citySid: '2101',
      provinceSid: '21'
    },
    {
      sid: '210104',
      name: '大东区',
      citySid: '2101',
      provinceSid: '21'
    },
    {
      sid: '210105',
      name: '皇姑区',
      citySid: '2101',
      provinceSid: '21'
    },
    {
      sid: '210106',
      name: '铁西区',
      citySid: '2101',
      provinceSid: '21'
    },
    {
      sid: '210111',
      name: '苏家屯区',
      citySid: '2101',
      provinceSid: '21'
    },
    {
      sid: '210112',
      name: '浑南区',
      citySid: '2101',
      provinceSid: '21'
    },
    {
      sid: '210113',
      name: '沈北新区',
      citySid: '2101',
      provinceSid: '21'
    },
    {
      sid: '210114',
      name: '于洪区',
      citySid: '2101',
      provinceSid: '21'
    },
    {
      sid: '210115',
      name: '辽中区',
      citySid: '2101',
      provinceSid: '21'
    },
    {
      sid: '210123',
      name: '康平县',
      citySid: '2101',
      provinceSid: '21'
    },
    {
      sid: '210124',
      name: '法库县',
      citySid: '2101',
      provinceSid: '21'
    },
    {
      sid: '210181',
      name: '新民市',
      citySid: '2101',
      provinceSid: '21'
    },
    {
      sid: '210202',
      name: '中山区',
      citySid: '2102',
      provinceSid: '21'
    },
    {
      sid: '210203',
      name: '西岗区',
      citySid: '2102',
      provinceSid: '21'
    },
    {
      sid: '210204',
      name: '沙河口区',
      citySid: '2102',
      provinceSid: '21'
    },
    {
      sid: '210211',
      name: '甘井子区',
      citySid: '2102',
      provinceSid: '21'
    },
    {
      sid: '210212',
      name: '旅顺口区',
      citySid: '2102',
      provinceSid: '21'
    },
    {
      sid: '210213',
      name: '金州区',
      citySid: '2102',
      provinceSid: '21'
    },
    {
      sid: '210214',
      name: '普兰店区',
      citySid: '2102',
      provinceSid: '21'
    },
    {
      sid: '210224',
      name: '长海县',
      citySid: '2102',
      provinceSid: '21'
    },
    {
      sid: '210281',
      name: '瓦房店市',
      citySid: '2102',
      provinceSid: '21'
    },
    {
      sid: '210283',
      name: '庄河市',
      citySid: '2102',
      provinceSid: '21'
    },
    {
      sid: '210302',
      name: '铁东区',
      citySid: '2103',
      provinceSid: '21'
    },
    {
      sid: '210303',
      name: '铁西区',
      citySid: '2103',
      provinceSid: '21'
    },
    {
      sid: '210304',
      name: '立山区',
      citySid: '2103',
      provinceSid: '21'
    },
    {
      sid: '210311',
      name: '千山区',
      citySid: '2103',
      provinceSid: '21'
    },
    {
      sid: '210321',
      name: '台安县',
      citySid: '2103',
      provinceSid: '21'
    },
    {
      sid: '210323',
      name: '岫岩满族自治县',
      citySid: '2103',
      provinceSid: '21'
    },
    {
      sid: '210381',
      name: '海城市',
      citySid: '2103',
      provinceSid: '21'
    },
    {
      sid: '210402',
      name: '新抚区',
      citySid: '2104',
      provinceSid: '21'
    },
    {
      sid: '210403',
      name: '东洲区',
      citySid: '2104',
      provinceSid: '21'
    },
    {
      sid: '210404',
      name: '望花区',
      citySid: '2104',
      provinceSid: '21'
    },
    {
      sid: '210411',
      name: '顺城区',
      citySid: '2104',
      provinceSid: '21'
    },
    {
      sid: '210421',
      name: '抚顺县',
      citySid: '2104',
      provinceSid: '21'
    },
    {
      sid: '210422',
      name: '新宾满族自治县',
      citySid: '2104',
      provinceSid: '21'
    },
    {
      sid: '210423',
      name: '清原满族自治县',
      citySid: '2104',
      provinceSid: '21'
    },
    {
      sid: '210502',
      name: '平山区',
      citySid: '2105',
      provinceSid: '21'
    },
    {
      sid: '210503',
      name: '溪湖区',
      citySid: '2105',
      provinceSid: '21'
    },
    {
      sid: '210504',
      name: '明山区',
      citySid: '2105',
      provinceSid: '21'
    },
    {
      sid: '210505',
      name: '南芬区',
      citySid: '2105',
      provinceSid: '21'
    },
    {
      sid: '210521',
      name: '本溪满族自治县',
      citySid: '2105',
      provinceSid: '21'
    },
    {
      sid: '210522',
      name: '桓仁满族自治县',
      citySid: '2105',
      provinceSid: '21'
    },
    {
      sid: '210602',
      name: '元宝区',
      citySid: '2106',
      provinceSid: '21'
    },
    {
      sid: '210603',
      name: '振兴区',
      citySid: '2106',
      provinceSid: '21'
    },
    {
      sid: '210604',
      name: '振安区',
      citySid: '2106',
      provinceSid: '21'
    },
    {
      sid: '210624',
      name: '宽甸满族自治县',
      citySid: '2106',
      provinceSid: '21'
    },
    {
      sid: '210681',
      name: '东港市',
      citySid: '2106',
      provinceSid: '21'
    },
    {
      sid: '210682',
      name: '凤城市',
      citySid: '2106',
      provinceSid: '21'
    },
    {
      sid: '210702',
      name: '古塔区',
      citySid: '2107',
      provinceSid: '21'
    },
    {
      sid: '210703',
      name: '凌河区',
      citySid: '2107',
      provinceSid: '21'
    },
    {
      sid: '210711',
      name: '太和区',
      citySid: '2107',
      provinceSid: '21'
    },
    {
      sid: '210726',
      name: '黑山县',
      citySid: '2107',
      provinceSid: '21'
    },
    {
      sid: '210727',
      name: '义县',
      citySid: '2107',
      provinceSid: '21'
    },
    {
      sid: '210781',
      name: '凌海市',
      citySid: '2107',
      provinceSid: '21'
    },
    {
      sid: '210782',
      name: '北镇市',
      citySid: '2107',
      provinceSid: '21'
    },
    {
      sid: '210802',
      name: '站前区',
      citySid: '2108',
      provinceSid: '21'
    },
    {
      sid: '210803',
      name: '西市区',
      citySid: '2108',
      provinceSid: '21'
    },
    {
      sid: '210804',
      name: '鲅鱼圈区',
      citySid: '2108',
      provinceSid: '21'
    },
    {
      sid: '210811',
      name: '老边区',
      citySid: '2108',
      provinceSid: '21'
    },
    {
      sid: '210881',
      name: '盖州市',
      citySid: '2108',
      provinceSid: '21'
    },
    {
      sid: '210882',
      name: '大石桥市',
      citySid: '2108',
      provinceSid: '21'
    },
    {
      sid: '210902',
      name: '海州区',
      citySid: '2109',
      provinceSid: '21'
    },
    {
      sid: '210903',
      name: '新邱区',
      citySid: '2109',
      provinceSid: '21'
    },
    {
      sid: '210904',
      name: '太平区',
      citySid: '2109',
      provinceSid: '21'
    },
    {
      sid: '210905',
      name: '清河门区',
      citySid: '2109',
      provinceSid: '21'
    },
    {
      sid: '210911',
      name: '细河区',
      citySid: '2109',
      provinceSid: '21'
    },
    {
      sid: '210921',
      name: '阜新蒙古族自治县',
      citySid: '2109',
      provinceSid: '21'
    },
    {
      sid: '210922',
      name: '彰武县',
      citySid: '2109',
      provinceSid: '21'
    },
    {
      sid: '211002',
      name: '白塔区',
      citySid: '2110',
      provinceSid: '21'
    },
    {
      sid: '211003',
      name: '文圣区',
      citySid: '2110',
      provinceSid: '21'
    },
    {
      sid: '211004',
      name: '宏伟区',
      citySid: '2110',
      provinceSid: '21'
    },
    {
      sid: '211005',
      name: '弓长岭区',
      citySid: '2110',
      provinceSid: '21'
    },
    {
      sid: '211011',
      name: '太子河区',
      citySid: '2110',
      provinceSid: '21'
    },
    {
      sid: '211021',
      name: '辽阳县',
      citySid: '2110',
      provinceSid: '21'
    },
    {
      sid: '211081',
      name: '灯塔市',
      citySid: '2110',
      provinceSid: '21'
    },
    {
      sid: '211102',
      name: '双台子区',
      citySid: '2111',
      provinceSid: '21'
    },
    {
      sid: '211103',
      name: '兴隆台区',
      citySid: '2111',
      provinceSid: '21'
    },
    {
      sid: '211104',
      name: '大洼区',
      citySid: '2111',
      provinceSid: '21'
    },
    {
      sid: '211122',
      name: '盘山县',
      citySid: '2111',
      provinceSid: '21'
    },
    {
      sid: '211202',
      name: '银州区',
      citySid: '2112',
      provinceSid: '21'
    },
    {
      sid: '211204',
      name: '清河区',
      citySid: '2112',
      provinceSid: '21'
    },
    {
      sid: '211221',
      name: '铁岭县',
      citySid: '2112',
      provinceSid: '21'
    },
    {
      sid: '211223',
      name: '西丰县',
      citySid: '2112',
      provinceSid: '21'
    },
    {
      sid: '211224',
      name: '昌图县',
      citySid: '2112',
      provinceSid: '21'
    },
    {
      sid: '211281',
      name: '调兵山市',
      citySid: '2112',
      provinceSid: '21'
    },
    {
      sid: '211282',
      name: '开原市',
      citySid: '2112',
      provinceSid: '21'
    },
    {
      sid: '211302',
      name: '双塔区',
      citySid: '2113',
      provinceSid: '21'
    },
    {
      sid: '211303',
      name: '龙城区',
      citySid: '2113',
      provinceSid: '21'
    },
    {
      sid: '211321',
      name: '朝阳县',
      citySid: '2113',
      provinceSid: '21'
    },
    {
      sid: '211322',
      name: '建平县',
      citySid: '2113',
      provinceSid: '21'
    },
    {
      sid: '211324',
      name: '喀喇沁左翼蒙古族自治县',
      citySid: '2113',
      provinceSid: '21'
    },
    {
      sid: '211381',
      name: '北票市',
      citySid: '2113',
      provinceSid: '21'
    },
    {
      sid: '211382',
      name: '凌源市',
      citySid: '2113',
      provinceSid: '21'
    },
    {
      sid: '211402',
      name: '连山区',
      citySid: '2114',
      provinceSid: '21'
    },
    {
      sid: '211403',
      name: '龙港区',
      citySid: '2114',
      provinceSid: '21'
    },
    {
      sid: '211404',
      name: '南票区',
      citySid: '2114',
      provinceSid: '21'
    },
    {
      sid: '211421',
      name: '绥中县',
      citySid: '2114',
      provinceSid: '21'
    },
    {
      sid: '211422',
      name: '建昌县',
      citySid: '2114',
      provinceSid: '21'
    },
    {
      sid: '211481',
      name: '兴城市',
      citySid: '2114',
      provinceSid: '21'
    },
    {
      sid: '220102',
      name: '南关区',
      citySid: '2201',
      provinceSid: '22'
    },
    {
      sid: '220103',
      name: '宽城区',
      citySid: '2201',
      provinceSid: '22'
    },
    {
      sid: '220104',
      name: '朝阳区',
      citySid: '2201',
      provinceSid: '22'
    },
    {
      sid: '220105',
      name: '二道区',
      citySid: '2201',
      provinceSid: '22'
    },
    {
      sid: '220106',
      name: '绿园区',
      citySid: '2201',
      provinceSid: '22'
    },
    {
      sid: '220112',
      name: '双阳区',
      citySid: '2201',
      provinceSid: '22'
    },
    {
      sid: '220113',
      name: '九台区',
      citySid: '2201',
      provinceSid: '22'
    },
    {
      sid: '220122',
      name: '农安县',
      citySid: '2201',
      provinceSid: '22'
    },
    {
      sid: '220171',
      name: '长春经济技术开发区',
      citySid: '2201',
      provinceSid: '22'
    },
    {
      sid: '220172',
      name: '长春净月高新技术产业开发区',
      citySid: '2201',
      provinceSid: '22'
    },
    {
      sid: '220173',
      name: '长春高新技术产业开发区',
      citySid: '2201',
      provinceSid: '22'
    },
    {
      sid: '220174',
      name: '长春汽车经济技术开发区',
      citySid: '2201',
      provinceSid: '22'
    },
    {
      sid: '220182',
      name: '榆树市',
      citySid: '2201',
      provinceSid: '22'
    },
    {
      sid: '220183',
      name: '德惠市',
      citySid: '2201',
      provinceSid: '22'
    },
    {
      sid: '220202',
      name: '昌邑区',
      citySid: '2202',
      provinceSid: '22'
    },
    {
      sid: '220203',
      name: '龙潭区',
      citySid: '2202',
      provinceSid: '22'
    },
    {
      sid: '220204',
      name: '船营区',
      citySid: '2202',
      provinceSid: '22'
    },
    {
      sid: '220211',
      name: '丰满区',
      citySid: '2202',
      provinceSid: '22'
    },
    {
      sid: '220221',
      name: '永吉县',
      citySid: '2202',
      provinceSid: '22'
    },
    {
      sid: '220271',
      name: '吉林经济开发区',
      citySid: '2202',
      provinceSid: '22'
    },
    {
      sid: '220272',
      name: '吉林高新技术产业开发区',
      citySid: '2202',
      provinceSid: '22'
    },
    {
      sid: '220273',
      name: '吉林中国新加坡食品区',
      citySid: '2202',
      provinceSid: '22'
    },
    {
      sid: '220281',
      name: '蛟河市',
      citySid: '2202',
      provinceSid: '22'
    },
    {
      sid: '220282',
      name: '桦甸市',
      citySid: '2202',
      provinceSid: '22'
    },
    {
      sid: '220283',
      name: '舒兰市',
      citySid: '2202',
      provinceSid: '22'
    },
    {
      sid: '220284',
      name: '磐石市',
      citySid: '2202',
      provinceSid: '22'
    },
    {
      sid: '220302',
      name: '铁西区',
      citySid: '2203',
      provinceSid: '22'
    },
    {
      sid: '220303',
      name: '铁东区',
      citySid: '2203',
      provinceSid: '22'
    },
    {
      sid: '220322',
      name: '梨树县',
      citySid: '2203',
      provinceSid: '22'
    },
    {
      sid: '220323',
      name: '伊通满族自治县',
      citySid: '2203',
      provinceSid: '22'
    },
    {
      sid: '220381',
      name: '公主岭市',
      citySid: '2203',
      provinceSid: '22'
    },
    {
      sid: '220382',
      name: '双辽市',
      citySid: '2203',
      provinceSid: '22'
    },
    {
      sid: '220402',
      name: '龙山区',
      citySid: '2204',
      provinceSid: '22'
    },
    {
      sid: '220403',
      name: '西安区',
      citySid: '2204',
      provinceSid: '22'
    },
    {
      sid: '220421',
      name: '东丰县',
      citySid: '2204',
      provinceSid: '22'
    },
    {
      sid: '220422',
      name: '东辽县',
      citySid: '2204',
      provinceSid: '22'
    },
    {
      sid: '220502',
      name: '东昌区',
      citySid: '2205',
      provinceSid: '22'
    },
    {
      sid: '220503',
      name: '二道江区',
      citySid: '2205',
      provinceSid: '22'
    },
    {
      sid: '220521',
      name: '通化县',
      citySid: '2205',
      provinceSid: '22'
    },
    {
      sid: '220523',
      name: '辉南县',
      citySid: '2205',
      provinceSid: '22'
    },
    {
      sid: '220524',
      name: '柳河县',
      citySid: '2205',
      provinceSid: '22'
    },
    {
      sid: '220581',
      name: '梅河口市',
      citySid: '2205',
      provinceSid: '22'
    },
    {
      sid: '220582',
      name: '集安市',
      citySid: '2205',
      provinceSid: '22'
    },
    {
      sid: '220602',
      name: '浑江区',
      citySid: '2206',
      provinceSid: '22'
    },
    {
      sid: '220605',
      name: '江源区',
      citySid: '2206',
      provinceSid: '22'
    },
    {
      sid: '220621',
      name: '抚松县',
      citySid: '2206',
      provinceSid: '22'
    },
    {
      sid: '220622',
      name: '靖宇县',
      citySid: '2206',
      provinceSid: '22'
    },
    {
      sid: '220623',
      name: '长白朝鲜族自治县',
      citySid: '2206',
      provinceSid: '22'
    },
    {
      sid: '220681',
      name: '临江市',
      citySid: '2206',
      provinceSid: '22'
    },
    {
      sid: '220702',
      name: '宁江区',
      citySid: '2207',
      provinceSid: '22'
    },
    {
      sid: '220721',
      name: '前郭尔罗斯蒙古族自治县',
      citySid: '2207',
      provinceSid: '22'
    },
    {
      sid: '220722',
      name: '长岭县',
      citySid: '2207',
      provinceSid: '22'
    },
    {
      sid: '220723',
      name: '乾安县',
      citySid: '2207',
      provinceSid: '22'
    },
    {
      sid: '220771',
      name: '吉林松原经济开发区',
      citySid: '2207',
      provinceSid: '22'
    },
    {
      sid: '220781',
      name: '扶余市',
      citySid: '2207',
      provinceSid: '22'
    },
    {
      sid: '220802',
      name: '洮北区',
      citySid: '2208',
      provinceSid: '22'
    },
    {
      sid: '220821',
      name: '镇赉县',
      citySid: '2208',
      provinceSid: '22'
    },
    {
      sid: '220822',
      name: '通榆县',
      citySid: '2208',
      provinceSid: '22'
    },
    {
      sid: '220871',
      name: '吉林白城经济开发区',
      citySid: '2208',
      provinceSid: '22'
    },
    {
      sid: '220881',
      name: '洮南市',
      citySid: '2208',
      provinceSid: '22'
    },
    {
      sid: '220882',
      name: '大安市',
      citySid: '2208',
      provinceSid: '22'
    },
    {
      sid: '222401',
      name: '延吉市',
      citySid: '2224',
      provinceSid: '22'
    },
    {
      sid: '222402',
      name: '图们市',
      citySid: '2224',
      provinceSid: '22'
    },
    {
      sid: '222403',
      name: '敦化市',
      citySid: '2224',
      provinceSid: '22'
    },
    {
      sid: '222404',
      name: '珲春市',
      citySid: '2224',
      provinceSid: '22'
    },
    {
      sid: '222405',
      name: '龙井市',
      citySid: '2224',
      provinceSid: '22'
    },
    {
      sid: '222406',
      name: '和龙市',
      citySid: '2224',
      provinceSid: '22'
    },
    {
      sid: '222424',
      name: '汪清县',
      citySid: '2224',
      provinceSid: '22'
    },
    {
      sid: '222426',
      name: '安图县',
      citySid: '2224',
      provinceSid: '22'
    },
    {
      sid: '230102',
      name: '道里区',
      citySid: '2301',
      provinceSid: '23'
    },
    {
      sid: '230103',
      name: '南岗区',
      citySid: '2301',
      provinceSid: '23'
    },
    {
      sid: '230104',
      name: '道外区',
      citySid: '2301',
      provinceSid: '23'
    },
    {
      sid: '230108',
      name: '平房区',
      citySid: '2301',
      provinceSid: '23'
    },
    {
      sid: '230109',
      name: '松北区',
      citySid: '2301',
      provinceSid: '23'
    },
    {
      sid: '230110',
      name: '香坊区',
      citySid: '2301',
      provinceSid: '23'
    },
    {
      sid: '230111',
      name: '呼兰区',
      citySid: '2301',
      provinceSid: '23'
    },
    {
      sid: '230112',
      name: '阿城区',
      citySid: '2301',
      provinceSid: '23'
    },
    {
      sid: '230113',
      name: '双城区',
      citySid: '2301',
      provinceSid: '23'
    },
    {
      sid: '230123',
      name: '依兰县',
      citySid: '2301',
      provinceSid: '23'
    },
    {
      sid: '230124',
      name: '方正县',
      citySid: '2301',
      provinceSid: '23'
    },
    {
      sid: '230125',
      name: '宾县',
      citySid: '2301',
      provinceSid: '23'
    },
    {
      sid: '230126',
      name: '巴彦县',
      citySid: '2301',
      provinceSid: '23'
    },
    {
      sid: '230127',
      name: '木兰县',
      citySid: '2301',
      provinceSid: '23'
    },
    {
      sid: '230128',
      name: '通河县',
      citySid: '2301',
      provinceSid: '23'
    },
    {
      sid: '230129',
      name: '延寿县',
      citySid: '2301',
      provinceSid: '23'
    },
    {
      sid: '230183',
      name: '尚志市',
      citySid: '2301',
      provinceSid: '23'
    },
    {
      sid: '230184',
      name: '五常市',
      citySid: '2301',
      provinceSid: '23'
    },
    {
      sid: '230202',
      name: '龙沙区',
      citySid: '2302',
      provinceSid: '23'
    },
    {
      sid: '230203',
      name: '建华区',
      citySid: '2302',
      provinceSid: '23'
    },
    {
      sid: '230204',
      name: '铁锋区',
      citySid: '2302',
      provinceSid: '23'
    },
    {
      sid: '230205',
      name: '昂昂溪区',
      citySid: '2302',
      provinceSid: '23'
    },
    {
      sid: '230206',
      name: '富拉尔基区',
      citySid: '2302',
      provinceSid: '23'
    },
    {
      sid: '230207',
      name: '碾子山区',
      citySid: '2302',
      provinceSid: '23'
    },
    {
      sid: '230208',
      name: '梅里斯达斡尔族区',
      citySid: '2302',
      provinceSid: '23'
    },
    {
      sid: '230221',
      name: '龙江县',
      citySid: '2302',
      provinceSid: '23'
    },
    {
      sid: '230223',
      name: '依安县',
      citySid: '2302',
      provinceSid: '23'
    },
    {
      sid: '230224',
      name: '泰来县',
      citySid: '2302',
      provinceSid: '23'
    },
    {
      sid: '230225',
      name: '甘南县',
      citySid: '2302',
      provinceSid: '23'
    },
    {
      sid: '230227',
      name: '富裕县',
      citySid: '2302',
      provinceSid: '23'
    },
    {
      sid: '230229',
      name: '克山县',
      citySid: '2302',
      provinceSid: '23'
    },
    {
      sid: '230230',
      name: '克东县',
      citySid: '2302',
      provinceSid: '23'
    },
    {
      sid: '230231',
      name: '拜泉县',
      citySid: '2302',
      provinceSid: '23'
    },
    {
      sid: '230281',
      name: '讷河市',
      citySid: '2302',
      provinceSid: '23'
    },
    {
      sid: '230302',
      name: '鸡冠区',
      citySid: '2303',
      provinceSid: '23'
    },
    {
      sid: '230303',
      name: '恒山区',
      citySid: '2303',
      provinceSid: '23'
    },
    {
      sid: '230304',
      name: '滴道区',
      citySid: '2303',
      provinceSid: '23'
    },
    {
      sid: '230305',
      name: '梨树区',
      citySid: '2303',
      provinceSid: '23'
    },
    {
      sid: '230306',
      name: '城子河区',
      citySid: '2303',
      provinceSid: '23'
    },
    {
      sid: '230307',
      name: '麻山区',
      citySid: '2303',
      provinceSid: '23'
    },
    {
      sid: '230321',
      name: '鸡东县',
      citySid: '2303',
      provinceSid: '23'
    },
    {
      sid: '230381',
      name: '虎林市',
      citySid: '2303',
      provinceSid: '23'
    },
    {
      sid: '230382',
      name: '密山市',
      citySid: '2303',
      provinceSid: '23'
    },
    {
      sid: '230402',
      name: '向阳区',
      citySid: '2304',
      provinceSid: '23'
    },
    {
      sid: '230403',
      name: '工农区',
      citySid: '2304',
      provinceSid: '23'
    },
    {
      sid: '230404',
      name: '南山区',
      citySid: '2304',
      provinceSid: '23'
    },
    {
      sid: '230405',
      name: '兴安区',
      citySid: '2304',
      provinceSid: '23'
    },
    {
      sid: '230406',
      name: '东山区',
      citySid: '2304',
      provinceSid: '23'
    },
    {
      sid: '230407',
      name: '兴山区',
      citySid: '2304',
      provinceSid: '23'
    },
    {
      sid: '230421',
      name: '萝北县',
      citySid: '2304',
      provinceSid: '23'
    },
    {
      sid: '230422',
      name: '绥滨县',
      citySid: '2304',
      provinceSid: '23'
    },
    {
      sid: '230502',
      name: '尖山区',
      citySid: '2305',
      provinceSid: '23'
    },
    {
      sid: '230503',
      name: '岭东区',
      citySid: '2305',
      provinceSid: '23'
    },
    {
      sid: '230505',
      name: '四方台区',
      citySid: '2305',
      provinceSid: '23'
    },
    {
      sid: '230506',
      name: '宝山区',
      citySid: '2305',
      provinceSid: '23'
    },
    {
      sid: '230521',
      name: '集贤县',
      citySid: '2305',
      provinceSid: '23'
    },
    {
      sid: '230522',
      name: '友谊县',
      citySid: '2305',
      provinceSid: '23'
    },
    {
      sid: '230523',
      name: '宝清县',
      citySid: '2305',
      provinceSid: '23'
    },
    {
      sid: '230524',
      name: '饶河县',
      citySid: '2305',
      provinceSid: '23'
    },
    {
      sid: '230602',
      name: '萨尔图区',
      citySid: '2306',
      provinceSid: '23'
    },
    {
      sid: '230603',
      name: '龙凤区',
      citySid: '2306',
      provinceSid: '23'
    },
    {
      sid: '230604',
      name: '让胡路区',
      citySid: '2306',
      provinceSid: '23'
    },
    {
      sid: '230605',
      name: '红岗区',
      citySid: '2306',
      provinceSid: '23'
    },
    {
      sid: '230606',
      name: '大同区',
      citySid: '2306',
      provinceSid: '23'
    },
    {
      sid: '230621',
      name: '肇州县',
      citySid: '2306',
      provinceSid: '23'
    },
    {
      sid: '230622',
      name: '肇源县',
      citySid: '2306',
      provinceSid: '23'
    },
    {
      sid: '230623',
      name: '林甸县',
      citySid: '2306',
      provinceSid: '23'
    },
    {
      sid: '230624',
      name: '杜尔伯特蒙古族自治县',
      citySid: '2306',
      provinceSid: '23'
    },
    {
      sid: '230671',
      name: '大庆高新技术产业开发区',
      citySid: '2306',
      provinceSid: '23'
    },
    {
      sid: '230702',
      name: '伊春区',
      citySid: '2307',
      provinceSid: '23'
    },
    {
      sid: '230703',
      name: '南岔区',
      citySid: '2307',
      provinceSid: '23'
    },
    {
      sid: '230704',
      name: '友好区',
      citySid: '2307',
      provinceSid: '23'
    },
    {
      sid: '230705',
      name: '西林区',
      citySid: '2307',
      provinceSid: '23'
    },
    {
      sid: '230706',
      name: '翠峦区',
      citySid: '2307',
      provinceSid: '23'
    },
    {
      sid: '230707',
      name: '新青区',
      citySid: '2307',
      provinceSid: '23'
    },
    {
      sid: '230708',
      name: '美溪区',
      citySid: '2307',
      provinceSid: '23'
    },
    {
      sid: '230709',
      name: '金山屯区',
      citySid: '2307',
      provinceSid: '23'
    },
    {
      sid: '230710',
      name: '五营区',
      citySid: '2307',
      provinceSid: '23'
    },
    {
      sid: '230711',
      name: '乌马河区',
      citySid: '2307',
      provinceSid: '23'
    },
    {
      sid: '230712',
      name: '汤旺河区',
      citySid: '2307',
      provinceSid: '23'
    },
    {
      sid: '230713',
      name: '带岭区',
      citySid: '2307',
      provinceSid: '23'
    },
    {
      sid: '230714',
      name: '乌伊岭区',
      citySid: '2307',
      provinceSid: '23'
    },
    {
      sid: '230715',
      name: '红星区',
      citySid: '2307',
      provinceSid: '23'
    },
    {
      sid: '230716',
      name: '上甘岭区',
      citySid: '2307',
      provinceSid: '23'
    },
    {
      sid: '230722',
      name: '嘉荫县',
      citySid: '2307',
      provinceSid: '23'
    },
    {
      sid: '230781',
      name: '铁力市',
      citySid: '2307',
      provinceSid: '23'
    },
    {
      sid: '230803',
      name: '向阳区',
      citySid: '2308',
      provinceSid: '23'
    },
    {
      sid: '230804',
      name: '前进区',
      citySid: '2308',
      provinceSid: '23'
    },
    {
      sid: '230805',
      name: '东风区',
      citySid: '2308',
      provinceSid: '23'
    },
    {
      sid: '230811',
      name: '郊区',
      citySid: '2308',
      provinceSid: '23'
    },
    {
      sid: '230822',
      name: '桦南县',
      citySid: '2308',
      provinceSid: '23'
    },
    {
      sid: '230826',
      name: '桦川县',
      citySid: '2308',
      provinceSid: '23'
    },
    {
      sid: '230828',
      name: '汤原县',
      citySid: '2308',
      provinceSid: '23'
    },
    {
      sid: '230881',
      name: '同江市',
      citySid: '2308',
      provinceSid: '23'
    },
    {
      sid: '230882',
      name: '富锦市',
      citySid: '2308',
      provinceSid: '23'
    },
    {
      sid: '230883',
      name: '抚远市',
      citySid: '2308',
      provinceSid: '23'
    },
    {
      sid: '230902',
      name: '新兴区',
      citySid: '2309',
      provinceSid: '23'
    },
    {
      sid: '230903',
      name: '桃山区',
      citySid: '2309',
      provinceSid: '23'
    },
    {
      sid: '230904',
      name: '茄子河区',
      citySid: '2309',
      provinceSid: '23'
    },
    {
      sid: '230921',
      name: '勃利县',
      citySid: '2309',
      provinceSid: '23'
    },
    {
      sid: '231002',
      name: '东安区',
      citySid: '2310',
      provinceSid: '23'
    },
    {
      sid: '231003',
      name: '阳明区',
      citySid: '2310',
      provinceSid: '23'
    },
    {
      sid: '231004',
      name: '爱民区',
      citySid: '2310',
      provinceSid: '23'
    },
    {
      sid: '231005',
      name: '西安区',
      citySid: '2310',
      provinceSid: '23'
    },
    {
      sid: '231025',
      name: '林口县',
      citySid: '2310',
      provinceSid: '23'
    },
    {
      sid: '231071',
      name: '牡丹江经济技术开发区',
      citySid: '2310',
      provinceSid: '23'
    },
    {
      sid: '231081',
      name: '绥芬河市',
      citySid: '2310',
      provinceSid: '23'
    },
    {
      sid: '231083',
      name: '海林市',
      citySid: '2310',
      provinceSid: '23'
    },
    {
      sid: '231084',
      name: '宁安市',
      citySid: '2310',
      provinceSid: '23'
    },
    {
      sid: '231085',
      name: '穆棱市',
      citySid: '2310',
      provinceSid: '23'
    },
    {
      sid: '231086',
      name: '东宁市',
      citySid: '2310',
      provinceSid: '23'
    },
    {
      sid: '231102',
      name: '爱辉区',
      citySid: '2311',
      provinceSid: '23'
    },
    {
      sid: '231121',
      name: '嫩江县',
      citySid: '2311',
      provinceSid: '23'
    },
    {
      sid: '231123',
      name: '逊克县',
      citySid: '2311',
      provinceSid: '23'
    },
    {
      sid: '231124',
      name: '孙吴县',
      citySid: '2311',
      provinceSid: '23'
    },
    {
      sid: '231181',
      name: '北安市',
      citySid: '2311',
      provinceSid: '23'
    },
    {
      sid: '231182',
      name: '五大连池市',
      citySid: '2311',
      provinceSid: '23'
    },
    {
      sid: '231202',
      name: '北林区',
      citySid: '2312',
      provinceSid: '23'
    },
    {
      sid: '231221',
      name: '望奎县',
      citySid: '2312',
      provinceSid: '23'
    },
    {
      sid: '231222',
      name: '兰西县',
      citySid: '2312',
      provinceSid: '23'
    },
    {
      sid: '231223',
      name: '青冈县',
      citySid: '2312',
      provinceSid: '23'
    },
    {
      sid: '231224',
      name: '庆安县',
      citySid: '2312',
      provinceSid: '23'
    },
    {
      sid: '231225',
      name: '明水县',
      citySid: '2312',
      provinceSid: '23'
    },
    {
      sid: '231226',
      name: '绥棱县',
      citySid: '2312',
      provinceSid: '23'
    },
    {
      sid: '231281',
      name: '安达市',
      citySid: '2312',
      provinceSid: '23'
    },
    {
      sid: '231282',
      name: '肇东市',
      citySid: '2312',
      provinceSid: '23'
    },
    {
      sid: '231283',
      name: '海伦市',
      citySid: '2312',
      provinceSid: '23'
    },
    {
      sid: '232701',
      name: '加格达奇区',
      citySid: '2327',
      provinceSid: '23'
    },
    {
      sid: '232702',
      name: '松岭区',
      citySid: '2327',
      provinceSid: '23'
    },
    {
      sid: '232703',
      name: '新林区',
      citySid: '2327',
      provinceSid: '23'
    },
    {
      sid: '232704',
      name: '呼中区',
      citySid: '2327',
      provinceSid: '23'
    },
    {
      sid: '232721',
      name: '呼玛县',
      citySid: '2327',
      provinceSid: '23'
    },
    {
      sid: '232722',
      name: '塔河县',
      citySid: '2327',
      provinceSid: '23'
    },
    {
      sid: '232723',
      name: '漠河县',
      citySid: '2327',
      provinceSid: '23'
    },
    {
      sid: '310101',
      name: '黄浦区',
      citySid: '3101',
      provinceSid: '31'
    },
    {
      sid: '310104',
      name: '徐汇区',
      citySid: '3101',
      provinceSid: '31'
    },
    {
      sid: '310105',
      name: '长宁区',
      citySid: '3101',
      provinceSid: '31'
    },
    {
      sid: '310106',
      name: '静安区',
      citySid: '3101',
      provinceSid: '31'
    },
    {
      sid: '310107',
      name: '普陀区',
      citySid: '3101',
      provinceSid: '31'
    },
    {
      sid: '310109',
      name: '虹口区',
      citySid: '3101',
      provinceSid: '31'
    },
    {
      sid: '310110',
      name: '杨浦区',
      citySid: '3101',
      provinceSid: '31'
    },
    {
      sid: '310112',
      name: '闵行区',
      citySid: '3101',
      provinceSid: '31'
    },
    {
      sid: '310113',
      name: '宝山区',
      citySid: '3101',
      provinceSid: '31'
    },
    {
      sid: '310114',
      name: '嘉定区',
      citySid: '3101',
      provinceSid: '31'
    },
    {
      sid: '310115',
      name: '浦东新区',
      citySid: '3101',
      provinceSid: '31'
    },
    {
      sid: '310116',
      name: '金山区',
      citySid: '3101',
      provinceSid: '31'
    },
    {
      sid: '310117',
      name: '松江区',
      citySid: '3101',
      provinceSid: '31'
    },
    {
      sid: '310118',
      name: '青浦区',
      citySid: '3101',
      provinceSid: '31'
    },
    {
      sid: '310120',
      name: '奉贤区',
      citySid: '3101',
      provinceSid: '31'
    },
    {
      sid: '310151',
      name: '崇明区',
      citySid: '3101',
      provinceSid: '31'
    },
    {
      sid: '320102',
      name: '玄武区',
      citySid: '3201',
      provinceSid: '32'
    },
    {
      sid: '320104',
      name: '秦淮区',
      citySid: '3201',
      provinceSid: '32'
    },
    {
      sid: '320105',
      name: '建邺区',
      citySid: '3201',
      provinceSid: '32'
    },
    {
      sid: '320106',
      name: '鼓楼区',
      citySid: '3201',
      provinceSid: '32'
    },
    {
      sid: '320111',
      name: '浦口区',
      citySid: '3201',
      provinceSid: '32'
    },
    {
      sid: '320113',
      name: '栖霞区',
      citySid: '3201',
      provinceSid: '32'
    },
    {
      sid: '320114',
      name: '雨花台区',
      citySid: '3201',
      provinceSid: '32'
    },
    {
      sid: '320115',
      name: '江宁区',
      citySid: '3201',
      provinceSid: '32'
    },
    {
      sid: '320116',
      name: '六合区',
      citySid: '3201',
      provinceSid: '32'
    },
    {
      sid: '320117',
      name: '溧水区',
      citySid: '3201',
      provinceSid: '32'
    },
    {
      sid: '320118',
      name: '高淳区',
      citySid: '3201',
      provinceSid: '32'
    },
    {
      sid: '320205',
      name: '锡山区',
      citySid: '3202',
      provinceSid: '32'
    },
    {
      sid: '320206',
      name: '惠山区',
      citySid: '3202',
      provinceSid: '32'
    },
    {
      sid: '320211',
      name: '滨湖区',
      citySid: '3202',
      provinceSid: '32'
    },
    {
      sid: '320213',
      name: '梁溪区',
      citySid: '3202',
      provinceSid: '32'
    },
    {
      sid: '320214',
      name: '新吴区',
      citySid: '3202',
      provinceSid: '32'
    },
    {
      sid: '320281',
      name: '江阴市',
      citySid: '3202',
      provinceSid: '32'
    },
    {
      sid: '320282',
      name: '宜兴市',
      citySid: '3202',
      provinceSid: '32'
    },
    {
      sid: '320302',
      name: '鼓楼区',
      citySid: '3203',
      provinceSid: '32'
    },
    {
      sid: '320303',
      name: '云龙区',
      citySid: '3203',
      provinceSid: '32'
    },
    {
      sid: '320305',
      name: '贾汪区',
      citySid: '3203',
      provinceSid: '32'
    },
    {
      sid: '320311',
      name: '泉山区',
      citySid: '3203',
      provinceSid: '32'
    },
    {
      sid: '320312',
      name: '铜山区',
      citySid: '3203',
      provinceSid: '32'
    },
    {
      sid: '320321',
      name: '丰县',
      citySid: '3203',
      provinceSid: '32'
    },
    {
      sid: '320322',
      name: '沛县',
      citySid: '3203',
      provinceSid: '32'
    },
    {
      sid: '320324',
      name: '睢宁县',
      citySid: '3203',
      provinceSid: '32'
    },
    {
      sid: '320371',
      name: '徐州经济技术开发区',
      citySid: '3203',
      provinceSid: '32'
    },
    {
      sid: '320381',
      name: '新沂市',
      citySid: '3203',
      provinceSid: '32'
    },
    {
      sid: '320382',
      name: '邳州市',
      citySid: '3203',
      provinceSid: '32'
    },
    {
      sid: '320402',
      name: '天宁区',
      citySid: '3204',
      provinceSid: '32'
    },
    {
      sid: '320404',
      name: '钟楼区',
      citySid: '3204',
      provinceSid: '32'
    },
    {
      sid: '320411',
      name: '新北区',
      citySid: '3204',
      provinceSid: '32'
    },
    {
      sid: '320412',
      name: '武进区',
      citySid: '3204',
      provinceSid: '32'
    },
    {
      sid: '320413',
      name: '金坛区',
      citySid: '3204',
      provinceSid: '32'
    },
    {
      sid: '320481',
      name: '溧阳市',
      citySid: '3204',
      provinceSid: '32'
    },
    {
      sid: '320505',
      name: '虎丘区',
      citySid: '3205',
      provinceSid: '32'
    },
    {
      sid: '320506',
      name: '吴中区',
      citySid: '3205',
      provinceSid: '32'
    },
    {
      sid: '320507',
      name: '相城区',
      citySid: '3205',
      provinceSid: '32'
    },
    {
      sid: '320508',
      name: '姑苏区',
      citySid: '3205',
      provinceSid: '32'
    },
    {
      sid: '320509',
      name: '吴江区',
      citySid: '3205',
      provinceSid: '32'
    },
    {
      sid: '320571',
      name: '苏州工业园区',
      citySid: '3205',
      provinceSid: '32'
    },
    {
      sid: '320581',
      name: '常熟市',
      citySid: '3205',
      provinceSid: '32'
    },
    {
      sid: '320582',
      name: '张家港市',
      citySid: '3205',
      provinceSid: '32'
    },
    {
      sid: '320583',
      name: '昆山市',
      citySid: '3205',
      provinceSid: '32'
    },
    {
      sid: '320585',
      name: '太仓市',
      citySid: '3205',
      provinceSid: '32'
    },
    {
      sid: '320602',
      name: '崇川区',
      citySid: '3206',
      provinceSid: '32'
    },
    {
      sid: '320611',
      name: '港闸区',
      citySid: '3206',
      provinceSid: '32'
    },
    {
      sid: '320612',
      name: '通州区',
      citySid: '3206',
      provinceSid: '32'
    },
    {
      sid: '320621',
      name: '海安县',
      citySid: '3206',
      provinceSid: '32'
    },
    {
      sid: '320623',
      name: '如东县',
      citySid: '3206',
      provinceSid: '32'
    },
    {
      sid: '320671',
      name: '南通经济技术开发区',
      citySid: '3206',
      provinceSid: '32'
    },
    {
      sid: '320681',
      name: '启东市',
      citySid: '3206',
      provinceSid: '32'
    },
    {
      sid: '320682',
      name: '如皋市',
      citySid: '3206',
      provinceSid: '32'
    },
    {
      sid: '320684',
      name: '海门市',
      citySid: '3206',
      provinceSid: '32'
    },
    {
      sid: '320703',
      name: '连云区',
      citySid: '3207',
      provinceSid: '32'
    },
    {
      sid: '320706',
      name: '海州区',
      citySid: '3207',
      provinceSid: '32'
    },
    {
      sid: '320707',
      name: '赣榆区',
      citySid: '3207',
      provinceSid: '32'
    },
    {
      sid: '320722',
      name: '东海县',
      citySid: '3207',
      provinceSid: '32'
    },
    {
      sid: '320723',
      name: '灌云县',
      citySid: '3207',
      provinceSid: '32'
    },
    {
      sid: '320724',
      name: '灌南县',
      citySid: '3207',
      provinceSid: '32'
    },
    {
      sid: '320771',
      name: '连云港经济技术开发区',
      citySid: '3207',
      provinceSid: '32'
    },
    {
      sid: '320772',
      name: '连云港高新技术产业开发区',
      citySid: '3207',
      provinceSid: '32'
    },
    {
      sid: '320803',
      name: '淮安区',
      citySid: '3208',
      provinceSid: '32'
    },
    {
      sid: '320804',
      name: '淮阴区',
      citySid: '3208',
      provinceSid: '32'
    },
    {
      sid: '320812',
      name: '清江浦区',
      citySid: '3208',
      provinceSid: '32'
    },
    {
      sid: '320813',
      name: '洪泽区',
      citySid: '3208',
      provinceSid: '32'
    },
    {
      sid: '320826',
      name: '涟水县',
      citySid: '3208',
      provinceSid: '32'
    },
    {
      sid: '320830',
      name: '盱眙县',
      citySid: '3208',
      provinceSid: '32'
    },
    {
      sid: '320831',
      name: '金湖县',
      citySid: '3208',
      provinceSid: '32'
    },
    {
      sid: '320871',
      name: '淮安经济技术开发区',
      citySid: '3208',
      provinceSid: '32'
    },
    {
      sid: '320902',
      name: '亭湖区',
      citySid: '3209',
      provinceSid: '32'
    },
    {
      sid: '320903',
      name: '盐都区',
      citySid: '3209',
      provinceSid: '32'
    },
    {
      sid: '320904',
      name: '大丰区',
      citySid: '3209',
      provinceSid: '32'
    },
    {
      sid: '320921',
      name: '响水县',
      citySid: '3209',
      provinceSid: '32'
    },
    {
      sid: '320922',
      name: '滨海县',
      citySid: '3209',
      provinceSid: '32'
    },
    {
      sid: '320923',
      name: '阜宁县',
      citySid: '3209',
      provinceSid: '32'
    },
    {
      sid: '320924',
      name: '射阳县',
      citySid: '3209',
      provinceSid: '32'
    },
    {
      sid: '320925',
      name: '建湖县',
      citySid: '3209',
      provinceSid: '32'
    },
    {
      sid: '320971',
      name: '盐城经济技术开发区',
      citySid: '3209',
      provinceSid: '32'
    },
    {
      sid: '320981',
      name: '东台市',
      citySid: '3209',
      provinceSid: '32'
    },
    {
      sid: '321002',
      name: '广陵区',
      citySid: '3210',
      provinceSid: '32'
    },
    {
      sid: '321003',
      name: '邗江区',
      citySid: '3210',
      provinceSid: '32'
    },
    {
      sid: '321012',
      name: '江都区',
      citySid: '3210',
      provinceSid: '32'
    },
    {
      sid: '321023',
      name: '宝应县',
      citySid: '3210',
      provinceSid: '32'
    },
    {
      sid: '321071',
      name: '扬州经济技术开发区',
      citySid: '3210',
      provinceSid: '32'
    },
    {
      sid: '321081',
      name: '仪征市',
      citySid: '3210',
      provinceSid: '32'
    },
    {
      sid: '321084',
      name: '高邮市',
      citySid: '3210',
      provinceSid: '32'
    },
    {
      sid: '321102',
      name: '京口区',
      citySid: '3211',
      provinceSid: '32'
    },
    {
      sid: '321111',
      name: '润州区',
      citySid: '3211',
      provinceSid: '32'
    },
    {
      sid: '321112',
      name: '丹徒区',
      citySid: '3211',
      provinceSid: '32'
    },
    {
      sid: '321171',
      name: '镇江新区',
      citySid: '3211',
      provinceSid: '32'
    },
    {
      sid: '321181',
      name: '丹阳市',
      citySid: '3211',
      provinceSid: '32'
    },
    {
      sid: '321182',
      name: '扬中市',
      citySid: '3211',
      provinceSid: '32'
    },
    {
      sid: '321183',
      name: '句容市',
      citySid: '3211',
      provinceSid: '32'
    },
    {
      sid: '321202',
      name: '海陵区',
      citySid: '3212',
      provinceSid: '32'
    },
    {
      sid: '321203',
      name: '高港区',
      citySid: '3212',
      provinceSid: '32'
    },
    {
      sid: '321204',
      name: '姜堰区',
      citySid: '3212',
      provinceSid: '32'
    },
    {
      sid: '321271',
      name: '泰州医药高新技术产业开发区',
      citySid: '3212',
      provinceSid: '32'
    },
    {
      sid: '321281',
      name: '兴化市',
      citySid: '3212',
      provinceSid: '32'
    },
    {
      sid: '321282',
      name: '靖江市',
      citySid: '3212',
      provinceSid: '32'
    },
    {
      sid: '321283',
      name: '泰兴市',
      citySid: '3212',
      provinceSid: '32'
    },
    {
      sid: '321302',
      name: '宿城区',
      citySid: '3213',
      provinceSid: '32'
    },
    {
      sid: '321311',
      name: '宿豫区',
      citySid: '3213',
      provinceSid: '32'
    },
    {
      sid: '321322',
      name: '沭阳县',
      citySid: '3213',
      provinceSid: '32'
    },
    {
      sid: '321323',
      name: '泗阳县',
      citySid: '3213',
      provinceSid: '32'
    },
    {
      sid: '321324',
      name: '泗洪县',
      citySid: '3213',
      provinceSid: '32'
    },
    {
      sid: '321371',
      name: '宿迁经济技术开发区',
      citySid: '3213',
      provinceSid: '32'
    },
    {
      sid: '330102',
      name: '上城区',
      citySid: '3301',
      provinceSid: '33'
    },
    {
      sid: '330103',
      name: '下城区',
      citySid: '3301',
      provinceSid: '33'
    },
    {
      sid: '330104',
      name: '江干区',
      citySid: '3301',
      provinceSid: '33'
    },
    {
      sid: '330105',
      name: '拱墅区',
      citySid: '3301',
      provinceSid: '33'
    },
    {
      sid: '330106',
      name: '西湖区',
      citySid: '3301',
      provinceSid: '33'
    },
    {
      sid: '330108',
      name: '滨江区',
      citySid: '3301',
      provinceSid: '33'
    },
    {
      sid: '330109',
      name: '萧山区',
      citySid: '3301',
      provinceSid: '33'
    },
    {
      sid: '330110',
      name: '余杭区',
      citySid: '3301',
      provinceSid: '33'
    },
    {
      sid: '330111',
      name: '富阳区',
      citySid: '3301',
      provinceSid: '33'
    },
    {
      sid: '330112',
      name: '临安区',
      citySid: '3301',
      provinceSid: '33'
    },
    {
      sid: '330122',
      name: '桐庐县',
      citySid: '3301',
      provinceSid: '33'
    },
    {
      sid: '330127',
      name: '淳安县',
      citySid: '3301',
      provinceSid: '33'
    },
    {
      sid: '330182',
      name: '建德市',
      citySid: '3301',
      provinceSid: '33'
    },
    {
      sid: '330203',
      name: '海曙区',
      citySid: '3302',
      provinceSid: '33'
    },
    {
      sid: '330205',
      name: '江北区',
      citySid: '3302',
      provinceSid: '33'
    },
    {
      sid: '330206',
      name: '北仑区',
      citySid: '3302',
      provinceSid: '33'
    },
    {
      sid: '330211',
      name: '镇海区',
      citySid: '3302',
      provinceSid: '33'
    },
    {
      sid: '330212',
      name: '鄞州区',
      citySid: '3302',
      provinceSid: '33'
    },
    {
      sid: '330213',
      name: '奉化区',
      citySid: '3302',
      provinceSid: '33'
    },
    {
      sid: '330225',
      name: '象山县',
      citySid: '3302',
      provinceSid: '33'
    },
    {
      sid: '330226',
      name: '宁海县',
      citySid: '3302',
      provinceSid: '33'
    },
    {
      sid: '330281',
      name: '余姚市',
      citySid: '3302',
      provinceSid: '33'
    },
    {
      sid: '330282',
      name: '慈溪市',
      citySid: '3302',
      provinceSid: '33'
    },
    {
      sid: '330302',
      name: '鹿城区',
      citySid: '3303',
      provinceSid: '33'
    },
    {
      sid: '330303',
      name: '龙湾区',
      citySid: '3303',
      provinceSid: '33'
    },
    {
      sid: '330304',
      name: '瓯海区',
      citySid: '3303',
      provinceSid: '33'
    },
    {
      sid: '330305',
      name: '洞头区',
      citySid: '3303',
      provinceSid: '33'
    },
    {
      sid: '330324',
      name: '永嘉县',
      citySid: '3303',
      provinceSid: '33'
    },
    {
      sid: '330326',
      name: '平阳县',
      citySid: '3303',
      provinceSid: '33'
    },
    {
      sid: '330327',
      name: '苍南县',
      citySid: '3303',
      provinceSid: '33'
    },
    {
      sid: '330328',
      name: '文成县',
      citySid: '3303',
      provinceSid: '33'
    },
    {
      sid: '330329',
      name: '泰顺县',
      citySid: '3303',
      provinceSid: '33'
    },
    {
      sid: '330371',
      name: '温州经济技术开发区',
      citySid: '3303',
      provinceSid: '33'
    },
    {
      sid: '330381',
      name: '瑞安市',
      citySid: '3303',
      provinceSid: '33'
    },
    {
      sid: '330382',
      name: '乐清市',
      citySid: '3303',
      provinceSid: '33'
    },
    {
      sid: '330402',
      name: '南湖区',
      citySid: '3304',
      provinceSid: '33'
    },
    {
      sid: '330411',
      name: '秀洲区',
      citySid: '3304',
      provinceSid: '33'
    },
    {
      sid: '330421',
      name: '嘉善县',
      citySid: '3304',
      provinceSid: '33'
    },
    {
      sid: '330424',
      name: '海盐县',
      citySid: '3304',
      provinceSid: '33'
    },
    {
      sid: '330481',
      name: '海宁市',
      citySid: '3304',
      provinceSid: '33'
    },
    {
      sid: '330482',
      name: '平湖市',
      citySid: '3304',
      provinceSid: '33'
    },
    {
      sid: '330483',
      name: '桐乡市',
      citySid: '3304',
      provinceSid: '33'
    },
    {
      sid: '330502',
      name: '吴兴区',
      citySid: '3305',
      provinceSid: '33'
    },
    {
      sid: '330503',
      name: '南浔区',
      citySid: '3305',
      provinceSid: '33'
    },
    {
      sid: '330521',
      name: '德清县',
      citySid: '3305',
      provinceSid: '33'
    },
    {
      sid: '330522',
      name: '长兴县',
      citySid: '3305',
      provinceSid: '33'
    },
    {
      sid: '330523',
      name: '安吉县',
      citySid: '3305',
      provinceSid: '33'
    },
    {
      sid: '330602',
      name: '越城区',
      citySid: '3306',
      provinceSid: '33'
    },
    {
      sid: '330603',
      name: '柯桥区',
      citySid: '3306',
      provinceSid: '33'
    },
    {
      sid: '330604',
      name: '上虞区',
      citySid: '3306',
      provinceSid: '33'
    },
    {
      sid: '330624',
      name: '新昌县',
      citySid: '3306',
      provinceSid: '33'
    },
    {
      sid: '330681',
      name: '诸暨市',
      citySid: '3306',
      provinceSid: '33'
    },
    {
      sid: '330683',
      name: '嵊州市',
      citySid: '3306',
      provinceSid: '33'
    },
    {
      sid: '330702',
      name: '婺城区',
      citySid: '3307',
      provinceSid: '33'
    },
    {
      sid: '330703',
      name: '金东区',
      citySid: '3307',
      provinceSid: '33'
    },
    {
      sid: '330723',
      name: '武义县',
      citySid: '3307',
      provinceSid: '33'
    },
    {
      sid: '330726',
      name: '浦江县',
      citySid: '3307',
      provinceSid: '33'
    },
    {
      sid: '330727',
      name: '磐安县',
      citySid: '3307',
      provinceSid: '33'
    },
    {
      sid: '330781',
      name: '兰溪市',
      citySid: '3307',
      provinceSid: '33'
    },
    {
      sid: '330782',
      name: '义乌市',
      citySid: '3307',
      provinceSid: '33'
    },
    {
      sid: '330783',
      name: '东阳市',
      citySid: '3307',
      provinceSid: '33'
    },
    {
      sid: '330784',
      name: '永康市',
      citySid: '3307',
      provinceSid: '33'
    },
    {
      sid: '330802',
      name: '柯城区',
      citySid: '3308',
      provinceSid: '33'
    },
    {
      sid: '330803',
      name: '衢江区',
      citySid: '3308',
      provinceSid: '33'
    },
    {
      sid: '330822',
      name: '常山县',
      citySid: '3308',
      provinceSid: '33'
    },
    {
      sid: '330824',
      name: '开化县',
      citySid: '3308',
      provinceSid: '33'
    },
    {
      sid: '330825',
      name: '龙游县',
      citySid: '3308',
      provinceSid: '33'
    },
    {
      sid: '330881',
      name: '江山市',
      citySid: '3308',
      provinceSid: '33'
    },
    {
      sid: '330902',
      name: '定海区',
      citySid: '3309',
      provinceSid: '33'
    },
    {
      sid: '330903',
      name: '普陀区',
      citySid: '3309',
      provinceSid: '33'
    },
    {
      sid: '330921',
      name: '岱山县',
      citySid: '3309',
      provinceSid: '33'
    },
    {
      sid: '330922',
      name: '嵊泗县',
      citySid: '3309',
      provinceSid: '33'
    },
    {
      sid: '331002',
      name: '椒江区',
      citySid: '3310',
      provinceSid: '33'
    },
    {
      sid: '331003',
      name: '黄岩区',
      citySid: '3310',
      provinceSid: '33'
    },
    {
      sid: '331004',
      name: '路桥区',
      citySid: '3310',
      provinceSid: '33'
    },
    {
      sid: '331022',
      name: '三门县',
      citySid: '3310',
      provinceSid: '33'
    },
    {
      sid: '331023',
      name: '天台县',
      citySid: '3310',
      provinceSid: '33'
    },
    {
      sid: '331024',
      name: '仙居县',
      citySid: '3310',
      provinceSid: '33'
    },
    {
      sid: '331081',
      name: '温岭市',
      citySid: '3310',
      provinceSid: '33'
    },
    {
      sid: '331082',
      name: '临海市',
      citySid: '3310',
      provinceSid: '33'
    },
    {
      sid: '331083',
      name: '玉环市',
      citySid: '3310',
      provinceSid: '33'
    },
    {
      sid: '331102',
      name: '莲都区',
      citySid: '3311',
      provinceSid: '33'
    },
    {
      sid: '331121',
      name: '青田县',
      citySid: '3311',
      provinceSid: '33'
    },
    {
      sid: '331122',
      name: '缙云县',
      citySid: '3311',
      provinceSid: '33'
    },
    {
      sid: '331123',
      name: '遂昌县',
      citySid: '3311',
      provinceSid: '33'
    },
    {
      sid: '331124',
      name: '松阳县',
      citySid: '3311',
      provinceSid: '33'
    },
    {
      sid: '331125',
      name: '云和县',
      citySid: '3311',
      provinceSid: '33'
    },
    {
      sid: '331126',
      name: '庆元县',
      citySid: '3311',
      provinceSid: '33'
    },
    {
      sid: '331127',
      name: '景宁畲族自治县',
      citySid: '3311',
      provinceSid: '33'
    },
    {
      sid: '331181',
      name: '龙泉市',
      citySid: '3311',
      provinceSid: '33'
    },
    {
      sid: '340102',
      name: '瑶海区',
      citySid: '3401',
      provinceSid: '34'
    },
    {
      sid: '340103',
      name: '庐阳区',
      citySid: '3401',
      provinceSid: '34'
    },
    {
      sid: '340104',
      name: '蜀山区',
      citySid: '3401',
      provinceSid: '34'
    },
    {
      sid: '340111',
      name: '包河区',
      citySid: '3401',
      provinceSid: '34'
    },
    {
      sid: '340121',
      name: '长丰县',
      citySid: '3401',
      provinceSid: '34'
    },
    {
      sid: '340122',
      name: '肥东县',
      citySid: '3401',
      provinceSid: '34'
    },
    {
      sid: '340123',
      name: '肥西县',
      citySid: '3401',
      provinceSid: '34'
    },
    {
      sid: '340124',
      name: '庐江县',
      citySid: '3401',
      provinceSid: '34'
    },
    {
      sid: '340171',
      name: '合肥高新技术产业开发区',
      citySid: '3401',
      provinceSid: '34'
    },
    {
      sid: '340172',
      name: '合肥经济技术开发区',
      citySid: '3401',
      provinceSid: '34'
    },
    {
      sid: '340173',
      name: '合肥新站高新技术产业开发区',
      citySid: '3401',
      provinceSid: '34'
    },
    {
      sid: '340181',
      name: '巢湖市',
      citySid: '3401',
      provinceSid: '34'
    },
    {
      sid: '340202',
      name: '镜湖区',
      citySid: '3402',
      provinceSid: '34'
    },
    {
      sid: '340203',
      name: '弋江区',
      citySid: '3402',
      provinceSid: '34'
    },
    {
      sid: '340207',
      name: '鸠江区',
      citySid: '3402',
      provinceSid: '34'
    },
    {
      sid: '340208',
      name: '三山区',
      citySid: '3402',
      provinceSid: '34'
    },
    {
      sid: '340221',
      name: '芜湖县',
      citySid: '3402',
      provinceSid: '34'
    },
    {
      sid: '340222',
      name: '繁昌县',
      citySid: '3402',
      provinceSid: '34'
    },
    {
      sid: '340223',
      name: '南陵县',
      citySid: '3402',
      provinceSid: '34'
    },
    {
      sid: '340225',
      name: '无为县',
      citySid: '3402',
      provinceSid: '34'
    },
    {
      sid: '340271',
      name: '芜湖经济技术开发区',
      citySid: '3402',
      provinceSid: '34'
    },
    {
      sid: '340272',
      name: '安徽芜湖长江大桥经济开发区',
      citySid: '3402',
      provinceSid: '34'
    },
    {
      sid: '340302',
      name: '龙子湖区',
      citySid: '3403',
      provinceSid: '34'
    },
    {
      sid: '340303',
      name: '蚌山区',
      citySid: '3403',
      provinceSid: '34'
    },
    {
      sid: '340304',
      name: '禹会区',
      citySid: '3403',
      provinceSid: '34'
    },
    {
      sid: '340311',
      name: '淮上区',
      citySid: '3403',
      provinceSid: '34'
    },
    {
      sid: '340321',
      name: '怀远县',
      citySid: '3403',
      provinceSid: '34'
    },
    {
      sid: '340322',
      name: '五河县',
      citySid: '3403',
      provinceSid: '34'
    },
    {
      sid: '340323',
      name: '固镇县',
      citySid: '3403',
      provinceSid: '34'
    },
    {
      sid: '340371',
      name: '蚌埠市高新技术开发区',
      citySid: '3403',
      provinceSid: '34'
    },
    {
      sid: '340372',
      name: '蚌埠市经济开发区',
      citySid: '3403',
      provinceSid: '34'
    },
    {
      sid: '340402',
      name: '大通区',
      citySid: '3404',
      provinceSid: '34'
    },
    {
      sid: '340403',
      name: '田家庵区',
      citySid: '3404',
      provinceSid: '34'
    },
    {
      sid: '340404',
      name: '谢家集区',
      citySid: '3404',
      provinceSid: '34'
    },
    {
      sid: '340405',
      name: '八公山区',
      citySid: '3404',
      provinceSid: '34'
    },
    {
      sid: '340406',
      name: '潘集区',
      citySid: '3404',
      provinceSid: '34'
    },
    {
      sid: '340421',
      name: '凤台县',
      citySid: '3404',
      provinceSid: '34'
    },
    {
      sid: '340422',
      name: '寿县',
      citySid: '3404',
      provinceSid: '34'
    },
    {
      sid: '340503',
      name: '花山区',
      citySid: '3405',
      provinceSid: '34'
    },
    {
      sid: '340504',
      name: '雨山区',
      citySid: '3405',
      provinceSid: '34'
    },
    {
      sid: '340506',
      name: '博望区',
      citySid: '3405',
      provinceSid: '34'
    },
    {
      sid: '340521',
      name: '当涂县',
      citySid: '3405',
      provinceSid: '34'
    },
    {
      sid: '340522',
      name: '含山县',
      citySid: '3405',
      provinceSid: '34'
    },
    {
      sid: '340523',
      name: '和县',
      citySid: '3405',
      provinceSid: '34'
    },
    {
      sid: '340602',
      name: '杜集区',
      citySid: '3406',
      provinceSid: '34'
    },
    {
      sid: '340603',
      name: '相山区',
      citySid: '3406',
      provinceSid: '34'
    },
    {
      sid: '340604',
      name: '烈山区',
      citySid: '3406',
      provinceSid: '34'
    },
    {
      sid: '340621',
      name: '濉溪县',
      citySid: '3406',
      provinceSid: '34'
    },
    {
      sid: '340705',
      name: '铜官区',
      citySid: '3407',
      provinceSid: '34'
    },
    {
      sid: '340706',
      name: '义安区',
      citySid: '3407',
      provinceSid: '34'
    },
    {
      sid: '340711',
      name: '郊区',
      citySid: '3407',
      provinceSid: '34'
    },
    {
      sid: '340722',
      name: '枞阳县',
      citySid: '3407',
      provinceSid: '34'
    },
    {
      sid: '340802',
      name: '迎江区',
      citySid: '3408',
      provinceSid: '34'
    },
    {
      sid: '340803',
      name: '大观区',
      citySid: '3408',
      provinceSid: '34'
    },
    {
      sid: '340811',
      name: '宜秀区',
      citySid: '3408',
      provinceSid: '34'
    },
    {
      sid: '340822',
      name: '怀宁县',
      citySid: '3408',
      provinceSid: '34'
    },
    {
      sid: '340824',
      name: '潜山县',
      citySid: '3408',
      provinceSid: '34'
    },
    {
      sid: '340825',
      name: '太湖县',
      citySid: '3408',
      provinceSid: '34'
    },
    {
      sid: '340826',
      name: '宿松县',
      citySid: '3408',
      provinceSid: '34'
    },
    {
      sid: '340827',
      name: '望江县',
      citySid: '3408',
      provinceSid: '34'
    },
    {
      sid: '340828',
      name: '岳西县',
      citySid: '3408',
      provinceSid: '34'
    },
    {
      sid: '340871',
      name: '安徽安庆经济开发区',
      citySid: '3408',
      provinceSid: '34'
    },
    {
      sid: '340881',
      name: '桐城市',
      citySid: '3408',
      provinceSid: '34'
    },
    {
      sid: '341002',
      name: '屯溪区',
      citySid: '3410',
      provinceSid: '34'
    },
    {
      sid: '341003',
      name: '黄山区',
      citySid: '3410',
      provinceSid: '34'
    },
    {
      sid: '341004',
      name: '徽州区',
      citySid: '3410',
      provinceSid: '34'
    },
    {
      sid: '341021',
      name: '歙县',
      citySid: '3410',
      provinceSid: '34'
    },
    {
      sid: '341022',
      name: '休宁县',
      citySid: '3410',
      provinceSid: '34'
    },
    {
      sid: '341023',
      name: '黟县',
      citySid: '3410',
      provinceSid: '34'
    },
    {
      sid: '341024',
      name: '祁门县',
      citySid: '3410',
      provinceSid: '34'
    },
    {
      sid: '341102',
      name: '琅琊区',
      citySid: '3411',
      provinceSid: '34'
    },
    {
      sid: '341103',
      name: '南谯区',
      citySid: '3411',
      provinceSid: '34'
    },
    {
      sid: '341122',
      name: '来安县',
      citySid: '3411',
      provinceSid: '34'
    },
    {
      sid: '341124',
      name: '全椒县',
      citySid: '3411',
      provinceSid: '34'
    },
    {
      sid: '341125',
      name: '定远县',
      citySid: '3411',
      provinceSid: '34'
    },
    {
      sid: '341126',
      name: '凤阳县',
      citySid: '3411',
      provinceSid: '34'
    },
    {
      sid: '341171',
      name: '苏滁现代产业园',
      citySid: '3411',
      provinceSid: '34'
    },
    {
      sid: '341172',
      name: '滁州经济技术开发区',
      citySid: '3411',
      provinceSid: '34'
    },
    {
      sid: '341181',
      name: '天长市',
      citySid: '3411',
      provinceSid: '34'
    },
    {
      sid: '341182',
      name: '明光市',
      citySid: '3411',
      provinceSid: '34'
    },
    {
      sid: '341202',
      name: '颍州区',
      citySid: '3412',
      provinceSid: '34'
    },
    {
      sid: '341203',
      name: '颍东区',
      citySid: '3412',
      provinceSid: '34'
    },
    {
      sid: '341204',
      name: '颍泉区',
      citySid: '3412',
      provinceSid: '34'
    },
    {
      sid: '341221',
      name: '临泉县',
      citySid: '3412',
      provinceSid: '34'
    },
    {
      sid: '341222',
      name: '太和县',
      citySid: '3412',
      provinceSid: '34'
    },
    {
      sid: '341225',
      name: '阜南县',
      citySid: '3412',
      provinceSid: '34'
    },
    {
      sid: '341226',
      name: '颍上县',
      citySid: '3412',
      provinceSid: '34'
    },
    {
      sid: '341271',
      name: '阜阳合肥现代产业园区',
      citySid: '3412',
      provinceSid: '34'
    },
    {
      sid: '341272',
      name: '阜阳经济技术开发区',
      citySid: '3412',
      provinceSid: '34'
    },
    {
      sid: '341282',
      name: '界首市',
      citySid: '3412',
      provinceSid: '34'
    },
    {
      sid: '341302',
      name: '埇桥区',
      citySid: '3413',
      provinceSid: '34'
    },
    {
      sid: '341321',
      name: '砀山县',
      citySid: '3413',
      provinceSid: '34'
    },
    {
      sid: '341322',
      name: '萧县',
      citySid: '3413',
      provinceSid: '34'
    },
    {
      sid: '341323',
      name: '灵璧县',
      citySid: '3413',
      provinceSid: '34'
    },
    {
      sid: '341324',
      name: '泗县',
      citySid: '3413',
      provinceSid: '34'
    },
    {
      sid: '341371',
      name: '宿州马鞍山现代产业园区',
      citySid: '3413',
      provinceSid: '34'
    },
    {
      sid: '341372',
      name: '宿州经济技术开发区',
      citySid: '3413',
      provinceSid: '34'
    },
    {
      sid: '341502',
      name: '金安区',
      citySid: '3415',
      provinceSid: '34'
    },
    {
      sid: '341503',
      name: '裕安区',
      citySid: '3415',
      provinceSid: '34'
    },
    {
      sid: '341504',
      name: '叶集区',
      citySid: '3415',
      provinceSid: '34'
    },
    {
      sid: '341522',
      name: '霍邱县',
      citySid: '3415',
      provinceSid: '34'
    },
    {
      sid: '341523',
      name: '舒城县',
      citySid: '3415',
      provinceSid: '34'
    },
    {
      sid: '341524',
      name: '金寨县',
      citySid: '3415',
      provinceSid: '34'
    },
    {
      sid: '341525',
      name: '霍山县',
      citySid: '3415',
      provinceSid: '34'
    },
    {
      sid: '341602',
      name: '谯城区',
      citySid: '3416',
      provinceSid: '34'
    },
    {
      sid: '341621',
      name: '涡阳县',
      citySid: '3416',
      provinceSid: '34'
    },
    {
      sid: '341622',
      name: '蒙城县',
      citySid: '3416',
      provinceSid: '34'
    },
    {
      sid: '341623',
      name: '利辛县',
      citySid: '3416',
      provinceSid: '34'
    },
    {
      sid: '341702',
      name: '贵池区',
      citySid: '3417',
      provinceSid: '34'
    },
    {
      sid: '341721',
      name: '东至县',
      citySid: '3417',
      provinceSid: '34'
    },
    {
      sid: '341722',
      name: '石台县',
      citySid: '3417',
      provinceSid: '34'
    },
    {
      sid: '341723',
      name: '青阳县',
      citySid: '3417',
      provinceSid: '34'
    },
    {
      sid: '341802',
      name: '宣州区',
      citySid: '3418',
      provinceSid: '34'
    },
    {
      sid: '341821',
      name: '郎溪县',
      citySid: '3418',
      provinceSid: '34'
    },
    {
      sid: '341822',
      name: '广德县',
      citySid: '3418',
      provinceSid: '34'
    },
    {
      sid: '341823',
      name: '泾县',
      citySid: '3418',
      provinceSid: '34'
    },
    {
      sid: '341824',
      name: '绩溪县',
      citySid: '3418',
      provinceSid: '34'
    },
    {
      sid: '341825',
      name: '旌德县',
      citySid: '3418',
      provinceSid: '34'
    },
    {
      sid: '341871',
      name: '宣城市经济开发区',
      citySid: '3418',
      provinceSid: '34'
    },
    {
      sid: '341881',
      name: '宁国市',
      citySid: '3418',
      provinceSid: '34'
    },
    {
      sid: '350102',
      name: '鼓楼区',
      citySid: '3501',
      provinceSid: '35'
    },
    {
      sid: '350103',
      name: '台江区',
      citySid: '3501',
      provinceSid: '35'
    },
    {
      sid: '350104',
      name: '仓山区',
      citySid: '3501',
      provinceSid: '35'
    },
    {
      sid: '350105',
      name: '马尾区',
      citySid: '3501',
      provinceSid: '35'
    },
    {
      sid: '350111',
      name: '晋安区',
      citySid: '3501',
      provinceSid: '35'
    },
    {
      sid: '350121',
      name: '闽侯县',
      citySid: '3501',
      provinceSid: '35'
    },
    {
      sid: '350122',
      name: '连江县',
      citySid: '3501',
      provinceSid: '35'
    },
    {
      sid: '350123',
      name: '罗源县',
      citySid: '3501',
      provinceSid: '35'
    },
    {
      sid: '350124',
      name: '闽清县',
      citySid: '3501',
      provinceSid: '35'
    },
    {
      sid: '350125',
      name: '永泰县',
      citySid: '3501',
      provinceSid: '35'
    },
    {
      sid: '350128',
      name: '平潭县',
      citySid: '3501',
      provinceSid: '35'
    },
    {
      sid: '350181',
      name: '福清市',
      citySid: '3501',
      provinceSid: '35'
    },
    {
      sid: '350182',
      name: '长乐市',
      citySid: '3501',
      provinceSid: '35'
    },
    {
      sid: '350203',
      name: '思明区',
      citySid: '3502',
      provinceSid: '35'
    },
    {
      sid: '350205',
      name: '海沧区',
      citySid: '3502',
      provinceSid: '35'
    },
    {
      sid: '350206',
      name: '湖里区',
      citySid: '3502',
      provinceSid: '35'
    },
    {
      sid: '350211',
      name: '集美区',
      citySid: '3502',
      provinceSid: '35'
    },
    {
      sid: '350212',
      name: '同安区',
      citySid: '3502',
      provinceSid: '35'
    },
    {
      sid: '350213',
      name: '翔安区',
      citySid: '3502',
      provinceSid: '35'
    },
    {
      sid: '350302',
      name: '城厢区',
      citySid: '3503',
      provinceSid: '35'
    },
    {
      sid: '350303',
      name: '涵江区',
      citySid: '3503',
      provinceSid: '35'
    },
    {
      sid: '350304',
      name: '荔城区',
      citySid: '3503',
      provinceSid: '35'
    },
    {
      sid: '350305',
      name: '秀屿区',
      citySid: '3503',
      provinceSid: '35'
    },
    {
      sid: '350322',
      name: '仙游县',
      citySid: '3503',
      provinceSid: '35'
    },
    {
      sid: '350402',
      name: '梅列区',
      citySid: '3504',
      provinceSid: '35'
    },
    {
      sid: '350403',
      name: '三元区',
      citySid: '3504',
      provinceSid: '35'
    },
    {
      sid: '350421',
      name: '明溪县',
      citySid: '3504',
      provinceSid: '35'
    },
    {
      sid: '350423',
      name: '清流县',
      citySid: '3504',
      provinceSid: '35'
    },
    {
      sid: '350424',
      name: '宁化县',
      citySid: '3504',
      provinceSid: '35'
    },
    {
      sid: '350425',
      name: '大田县',
      citySid: '3504',
      provinceSid: '35'
    },
    {
      sid: '350426',
      name: '尤溪县',
      citySid: '3504',
      provinceSid: '35'
    },
    {
      sid: '350427',
      name: '沙县',
      citySid: '3504',
      provinceSid: '35'
    },
    {
      sid: '350428',
      name: '将乐县',
      citySid: '3504',
      provinceSid: '35'
    },
    {
      sid: '350429',
      name: '泰宁县',
      citySid: '3504',
      provinceSid: '35'
    },
    {
      sid: '350430',
      name: '建宁县',
      citySid: '3504',
      provinceSid: '35'
    },
    {
      sid: '350481',
      name: '永安市',
      citySid: '3504',
      provinceSid: '35'
    },
    {
      sid: '350502',
      name: '鲤城区',
      citySid: '3505',
      provinceSid: '35'
    },
    {
      sid: '350503',
      name: '丰泽区',
      citySid: '3505',
      provinceSid: '35'
    },
    {
      sid: '350504',
      name: '洛江区',
      citySid: '3505',
      provinceSid: '35'
    },
    {
      sid: '350505',
      name: '泉港区',
      citySid: '3505',
      provinceSid: '35'
    },
    {
      sid: '350521',
      name: '惠安县',
      citySid: '3505',
      provinceSid: '35'
    },
    {
      sid: '350524',
      name: '安溪县',
      citySid: '3505',
      provinceSid: '35'
    },
    {
      sid: '350525',
      name: '永春县',
      citySid: '3505',
      provinceSid: '35'
    },
    {
      sid: '350526',
      name: '德化县',
      citySid: '3505',
      provinceSid: '35'
    },
    {
      sid: '350527',
      name: '金门县',
      citySid: '3505',
      provinceSid: '35'
    },
    {
      sid: '350581',
      name: '石狮市',
      citySid: '3505',
      provinceSid: '35'
    },
    {
      sid: '350582',
      name: '晋江市',
      citySid: '3505',
      provinceSid: '35'
    },
    {
      sid: '350583',
      name: '南安市',
      citySid: '3505',
      provinceSid: '35'
    },
    {
      sid: '350602',
      name: '芗城区',
      citySid: '3506',
      provinceSid: '35'
    },
    {
      sid: '350603',
      name: '龙文区',
      citySid: '3506',
      provinceSid: '35'
    },
    {
      sid: '350622',
      name: '云霄县',
      citySid: '3506',
      provinceSid: '35'
    },
    {
      sid: '350623',
      name: '漳浦县',
      citySid: '3506',
      provinceSid: '35'
    },
    {
      sid: '350624',
      name: '诏安县',
      citySid: '3506',
      provinceSid: '35'
    },
    {
      sid: '350625',
      name: '长泰县',
      citySid: '3506',
      provinceSid: '35'
    },
    {
      sid: '350626',
      name: '东山县',
      citySid: '3506',
      provinceSid: '35'
    },
    {
      sid: '350627',
      name: '南靖县',
      citySid: '3506',
      provinceSid: '35'
    },
    {
      sid: '350628',
      name: '平和县',
      citySid: '3506',
      provinceSid: '35'
    },
    {
      sid: '350629',
      name: '华安县',
      citySid: '3506',
      provinceSid: '35'
    },
    {
      sid: '350681',
      name: '龙海市',
      citySid: '3506',
      provinceSid: '35'
    },
    {
      sid: '350702',
      name: '延平区',
      citySid: '3507',
      provinceSid: '35'
    },
    {
      sid: '350703',
      name: '建阳区',
      citySid: '3507',
      provinceSid: '35'
    },
    {
      sid: '350721',
      name: '顺昌县',
      citySid: '3507',
      provinceSid: '35'
    },
    {
      sid: '350722',
      name: '浦城县',
      citySid: '3507',
      provinceSid: '35'
    },
    {
      sid: '350723',
      name: '光泽县',
      citySid: '3507',
      provinceSid: '35'
    },
    {
      sid: '350724',
      name: '松溪县',
      citySid: '3507',
      provinceSid: '35'
    },
    {
      sid: '350725',
      name: '政和县',
      citySid: '3507',
      provinceSid: '35'
    },
    {
      sid: '350781',
      name: '邵武市',
      citySid: '3507',
      provinceSid: '35'
    },
    {
      sid: '350782',
      name: '武夷山市',
      citySid: '3507',
      provinceSid: '35'
    },
    {
      sid: '350783',
      name: '建瓯市',
      citySid: '3507',
      provinceSid: '35'
    },
    {
      sid: '350802',
      name: '新罗区',
      citySid: '3508',
      provinceSid: '35'
    },
    {
      sid: '350803',
      name: '永定区',
      citySid: '3508',
      provinceSid: '35'
    },
    {
      sid: '350821',
      name: '长汀县',
      citySid: '3508',
      provinceSid: '35'
    },
    {
      sid: '350823',
      name: '上杭县',
      citySid: '3508',
      provinceSid: '35'
    },
    {
      sid: '350824',
      name: '武平县',
      citySid: '3508',
      provinceSid: '35'
    },
    {
      sid: '350825',
      name: '连城县',
      citySid: '3508',
      provinceSid: '35'
    },
    {
      sid: '350881',
      name: '漳平市',
      citySid: '3508',
      provinceSid: '35'
    },
    {
      sid: '350902',
      name: '蕉城区',
      citySid: '3509',
      provinceSid: '35'
    },
    {
      sid: '350921',
      name: '霞浦县',
      citySid: '3509',
      provinceSid: '35'
    },
    {
      sid: '350922',
      name: '古田县',
      citySid: '3509',
      provinceSid: '35'
    },
    {
      sid: '350923',
      name: '屏南县',
      citySid: '3509',
      provinceSid: '35'
    },
    {
      sid: '350924',
      name: '寿宁县',
      citySid: '3509',
      provinceSid: '35'
    },
    {
      sid: '350925',
      name: '周宁县',
      citySid: '3509',
      provinceSid: '35'
    },
    {
      sid: '350926',
      name: '柘荣县',
      citySid: '3509',
      provinceSid: '35'
    },
    {
      sid: '350981',
      name: '福安市',
      citySid: '3509',
      provinceSid: '35'
    },
    {
      sid: '350982',
      name: '福鼎市',
      citySid: '3509',
      provinceSid: '35'
    },
    {
      sid: '360102',
      name: '东湖区',
      citySid: '3601',
      provinceSid: '36'
    },
    {
      sid: '360103',
      name: '西湖区',
      citySid: '3601',
      provinceSid: '36'
    },
    {
      sid: '360104',
      name: '青云谱区',
      citySid: '3601',
      provinceSid: '36'
    },
    {
      sid: '360105',
      name: '湾里区',
      citySid: '3601',
      provinceSid: '36'
    },
    {
      sid: '360111',
      name: '青山湖区',
      citySid: '3601',
      provinceSid: '36'
    },
    {
      sid: '360112',
      name: '新建区',
      citySid: '3601',
      provinceSid: '36'
    },
    {
      sid: '360121',
      name: '南昌县',
      citySid: '3601',
      provinceSid: '36'
    },
    {
      sid: '360123',
      name: '安义县',
      citySid: '3601',
      provinceSid: '36'
    },
    {
      sid: '360124',
      name: '进贤县',
      citySid: '3601',
      provinceSid: '36'
    },
    {
      sid: '360202',
      name: '昌江区',
      citySid: '3602',
      provinceSid: '36'
    },
    {
      sid: '360203',
      name: '珠山区',
      citySid: '3602',
      provinceSid: '36'
    },
    {
      sid: '360222',
      name: '浮梁县',
      citySid: '3602',
      provinceSid: '36'
    },
    {
      sid: '360281',
      name: '乐平市',
      citySid: '3602',
      provinceSid: '36'
    },
    {
      sid: '360302',
      name: '安源区',
      citySid: '3603',
      provinceSid: '36'
    },
    {
      sid: '360313',
      name: '湘东区',
      citySid: '3603',
      provinceSid: '36'
    },
    {
      sid: '360321',
      name: '莲花县',
      citySid: '3603',
      provinceSid: '36'
    },
    {
      sid: '360322',
      name: '上栗县',
      citySid: '3603',
      provinceSid: '36'
    },
    {
      sid: '360323',
      name: '芦溪县',
      citySid: '3603',
      provinceSid: '36'
    },
    {
      sid: '360402',
      name: '濂溪区',
      citySid: '3604',
      provinceSid: '36'
    },
    {
      sid: '360403',
      name: '浔阳区',
      citySid: '3604',
      provinceSid: '36'
    },
    {
      sid: '360404',
      name: '柴桑区',
      citySid: '3604',
      provinceSid: '36'
    },
    {
      sid: '360423',
      name: '武宁县',
      citySid: '3604',
      provinceSid: '36'
    },
    {
      sid: '360424',
      name: '修水县',
      citySid: '3604',
      provinceSid: '36'
    },
    {
      sid: '360425',
      name: '永修县',
      citySid: '3604',
      provinceSid: '36'
    },
    {
      sid: '360426',
      name: '德安县',
      citySid: '3604',
      provinceSid: '36'
    },
    {
      sid: '360428',
      name: '都昌县',
      citySid: '3604',
      provinceSid: '36'
    },
    {
      sid: '360429',
      name: '湖口县',
      citySid: '3604',
      provinceSid: '36'
    },
    {
      sid: '360430',
      name: '彭泽县',
      citySid: '3604',
      provinceSid: '36'
    },
    {
      sid: '360481',
      name: '瑞昌市',
      citySid: '3604',
      provinceSid: '36'
    },
    {
      sid: '360482',
      name: '共青城市',
      citySid: '3604',
      provinceSid: '36'
    },
    {
      sid: '360483',
      name: '庐山市',
      citySid: '3604',
      provinceSid: '36'
    },
    {
      sid: '360502',
      name: '渝水区',
      citySid: '3605',
      provinceSid: '36'
    },
    {
      sid: '360521',
      name: '分宜县',
      citySid: '3605',
      provinceSid: '36'
    },
    {
      sid: '360602',
      name: '月湖区',
      citySid: '3606',
      provinceSid: '36'
    },
    {
      sid: '360622',
      name: '余江县',
      citySid: '3606',
      provinceSid: '36'
    },
    {
      sid: '360681',
      name: '贵溪市',
      citySid: '3606',
      provinceSid: '36'
    },
    {
      sid: '360702',
      name: '章贡区',
      citySid: '3607',
      provinceSid: '36'
    },
    {
      sid: '360703',
      name: '南康区',
      citySid: '3607',
      provinceSid: '36'
    },
    {
      sid: '360704',
      name: '赣县区',
      citySid: '3607',
      provinceSid: '36'
    },
    {
      sid: '360722',
      name: '信丰县',
      citySid: '3607',
      provinceSid: '36'
    },
    {
      sid: '360723',
      name: '大余县',
      citySid: '3607',
      provinceSid: '36'
    },
    {
      sid: '360724',
      name: '上犹县',
      citySid: '3607',
      provinceSid: '36'
    },
    {
      sid: '360725',
      name: '崇义县',
      citySid: '3607',
      provinceSid: '36'
    },
    {
      sid: '360726',
      name: '安远县',
      citySid: '3607',
      provinceSid: '36'
    },
    {
      sid: '360727',
      name: '龙南县',
      citySid: '3607',
      provinceSid: '36'
    },
    {
      sid: '360728',
      name: '定南县',
      citySid: '3607',
      provinceSid: '36'
    },
    {
      sid: '360729',
      name: '全南县',
      citySid: '3607',
      provinceSid: '36'
    },
    {
      sid: '360730',
      name: '宁都县',
      citySid: '3607',
      provinceSid: '36'
    },
    {
      sid: '360731',
      name: '于都县',
      citySid: '3607',
      provinceSid: '36'
    },
    {
      sid: '360732',
      name: '兴国县',
      citySid: '3607',
      provinceSid: '36'
    },
    {
      sid: '360733',
      name: '会昌县',
      citySid: '3607',
      provinceSid: '36'
    },
    {
      sid: '360734',
      name: '寻乌县',
      citySid: '3607',
      provinceSid: '36'
    },
    {
      sid: '360735',
      name: '石城县',
      citySid: '3607',
      provinceSid: '36'
    },
    {
      sid: '360781',
      name: '瑞金市',
      citySid: '3607',
      provinceSid: '36'
    },
    {
      sid: '360802',
      name: '吉州区',
      citySid: '3608',
      provinceSid: '36'
    },
    {
      sid: '360803',
      name: '青原区',
      citySid: '3608',
      provinceSid: '36'
    },
    {
      sid: '360821',
      name: '吉安县',
      citySid: '3608',
      provinceSid: '36'
    },
    {
      sid: '360822',
      name: '吉水县',
      citySid: '3608',
      provinceSid: '36'
    },
    {
      sid: '360823',
      name: '峡江县',
      citySid: '3608',
      provinceSid: '36'
    },
    {
      sid: '360824',
      name: '新干县',
      citySid: '3608',
      provinceSid: '36'
    },
    {
      sid: '360825',
      name: '永丰县',
      citySid: '3608',
      provinceSid: '36'
    },
    {
      sid: '360826',
      name: '泰和县',
      citySid: '3608',
      provinceSid: '36'
    },
    {
      sid: '360827',
      name: '遂川县',
      citySid: '3608',
      provinceSid: '36'
    },
    {
      sid: '360828',
      name: '万安县',
      citySid: '3608',
      provinceSid: '36'
    },
    {
      sid: '360829',
      name: '安福县',
      citySid: '3608',
      provinceSid: '36'
    },
    {
      sid: '360830',
      name: '永新县',
      citySid: '3608',
      provinceSid: '36'
    },
    {
      sid: '360881',
      name: '井冈山市',
      citySid: '3608',
      provinceSid: '36'
    },
    {
      sid: '360902',
      name: '袁州区',
      citySid: '3609',
      provinceSid: '36'
    },
    {
      sid: '360921',
      name: '奉新县',
      citySid: '3609',
      provinceSid: '36'
    },
    {
      sid: '360922',
      name: '万载县',
      citySid: '3609',
      provinceSid: '36'
    },
    {
      sid: '360923',
      name: '上高县',
      citySid: '3609',
      provinceSid: '36'
    },
    {
      sid: '360924',
      name: '宜丰县',
      citySid: '3609',
      provinceSid: '36'
    },
    {
      sid: '360925',
      name: '靖安县',
      citySid: '3609',
      provinceSid: '36'
    },
    {
      sid: '360926',
      name: '铜鼓县',
      citySid: '3609',
      provinceSid: '36'
    },
    {
      sid: '360981',
      name: '丰城市',
      citySid: '3609',
      provinceSid: '36'
    },
    {
      sid: '360982',
      name: '樟树市',
      citySid: '3609',
      provinceSid: '36'
    },
    {
      sid: '360983',
      name: '高安市',
      citySid: '3609',
      provinceSid: '36'
    },
    {
      sid: '361002',
      name: '临川区',
      citySid: '3610',
      provinceSid: '36'
    },
    {
      sid: '361003',
      name: '东乡区',
      citySid: '3610',
      provinceSid: '36'
    },
    {
      sid: '361021',
      name: '南城县',
      citySid: '3610',
      provinceSid: '36'
    },
    {
      sid: '361022',
      name: '黎川县',
      citySid: '3610',
      provinceSid: '36'
    },
    {
      sid: '361023',
      name: '南丰县',
      citySid: '3610',
      provinceSid: '36'
    },
    {
      sid: '361024',
      name: '崇仁县',
      citySid: '3610',
      provinceSid: '36'
    },
    {
      sid: '361025',
      name: '乐安县',
      citySid: '3610',
      provinceSid: '36'
    },
    {
      sid: '361026',
      name: '宜黄县',
      citySid: '3610',
      provinceSid: '36'
    },
    {
      sid: '361027',
      name: '金溪县',
      citySid: '3610',
      provinceSid: '36'
    },
    {
      sid: '361028',
      name: '资溪县',
      citySid: '3610',
      provinceSid: '36'
    },
    {
      sid: '361030',
      name: '广昌县',
      citySid: '3610',
      provinceSid: '36'
    },
    {
      sid: '361102',
      name: '信州区',
      citySid: '3611',
      provinceSid: '36'
    },
    {
      sid: '361103',
      name: '广丰区',
      citySid: '3611',
      provinceSid: '36'
    },
    {
      sid: '361121',
      name: '上饶县',
      citySid: '3611',
      provinceSid: '36'
    },
    {
      sid: '361123',
      name: '玉山县',
      citySid: '3611',
      provinceSid: '36'
    },
    {
      sid: '361124',
      name: '铅山县',
      citySid: '3611',
      provinceSid: '36'
    },
    {
      sid: '361125',
      name: '横峰县',
      citySid: '3611',
      provinceSid: '36'
    },
    {
      sid: '361126',
      name: '弋阳县',
      citySid: '3611',
      provinceSid: '36'
    },
    {
      sid: '361127',
      name: '余干县',
      citySid: '3611',
      provinceSid: '36'
    },
    {
      sid: '361128',
      name: '鄱阳县',
      citySid: '3611',
      provinceSid: '36'
    },
    {
      sid: '361129',
      name: '万年县',
      citySid: '3611',
      provinceSid: '36'
    },
    {
      sid: '361130',
      name: '婺源县',
      citySid: '3611',
      provinceSid: '36'
    },
    {
      sid: '361181',
      name: '德兴市',
      citySid: '3611',
      provinceSid: '36'
    },
    {
      sid: '370102',
      name: '历下区',
      citySid: '3701',
      provinceSid: '37'
    },
    {
      sid: '370103',
      name: '市中区',
      citySid: '3701',
      provinceSid: '37'
    },
    {
      sid: '370104',
      name: '槐荫区',
      citySid: '3701',
      provinceSid: '37'
    },
    {
      sid: '370105',
      name: '天桥区',
      citySid: '3701',
      provinceSid: '37'
    },
    {
      sid: '370112',
      name: '历城区',
      citySid: '3701',
      provinceSid: '37'
    },
    {
      sid: '370113',
      name: '长清区',
      citySid: '3701',
      provinceSid: '37'
    },
    {
      sid: '370114',
      name: '章丘区',
      citySid: '3701',
      provinceSid: '37'
    },
    {
      sid: '370124',
      name: '平阴县',
      citySid: '3701',
      provinceSid: '37'
    },
    {
      sid: '370125',
      name: '济阳县',
      citySid: '3701',
      provinceSid: '37'
    },
    {
      sid: '370126',
      name: '商河县',
      citySid: '3701',
      provinceSid: '37'
    },
    {
      sid: '370171',
      name: '济南高新技术产业开发区',
      citySid: '3701',
      provinceSid: '37'
    },
    {
      sid: '370202',
      name: '市南区',
      citySid: '3702',
      provinceSid: '37'
    },
    {
      sid: '370203',
      name: '市北区',
      citySid: '3702',
      provinceSid: '37'
    },
    {
      sid: '370211',
      name: '黄岛区',
      citySid: '3702',
      provinceSid: '37'
    },
    {
      sid: '370212',
      name: '崂山区',
      citySid: '3702',
      provinceSid: '37'
    },
    {
      sid: '370213',
      name: '李沧区',
      citySid: '3702',
      provinceSid: '37'
    },
    {
      sid: '370214',
      name: '城阳区',
      citySid: '3702',
      provinceSid: '37'
    },
    {
      sid: '370215',
      name: '即墨区',
      citySid: '3702',
      provinceSid: '37'
    },
    {
      sid: '370271',
      name: '青岛高新技术产业开发区',
      citySid: '3702',
      provinceSid: '37'
    },
    {
      sid: '370281',
      name: '胶州市',
      citySid: '3702',
      provinceSid: '37'
    },
    {
      sid: '370283',
      name: '平度市',
      citySid: '3702',
      provinceSid: '37'
    },
    {
      sid: '370285',
      name: '莱西市',
      citySid: '3702',
      provinceSid: '37'
    },
    {
      sid: '370302',
      name: '淄川区',
      citySid: '3703',
      provinceSid: '37'
    },
    {
      sid: '370303',
      name: '张店区',
      citySid: '3703',
      provinceSid: '37'
    },
    {
      sid: '370304',
      name: '博山区',
      citySid: '3703',
      provinceSid: '37'
    },
    {
      sid: '370305',
      name: '临淄区',
      citySid: '3703',
      provinceSid: '37'
    },
    {
      sid: '370306',
      name: '周村区',
      citySid: '3703',
      provinceSid: '37'
    },
    {
      sid: '370321',
      name: '桓台县',
      citySid: '3703',
      provinceSid: '37'
    },
    {
      sid: '370322',
      name: '高青县',
      citySid: '3703',
      provinceSid: '37'
    },
    {
      sid: '370323',
      name: '沂源县',
      citySid: '3703',
      provinceSid: '37'
    },
    {
      sid: '370402',
      name: '市中区',
      citySid: '3704',
      provinceSid: '37'
    },
    {
      sid: '370403',
      name: '薛城区',
      citySid: '3704',
      provinceSid: '37'
    },
    {
      sid: '370404',
      name: '峄城区',
      citySid: '3704',
      provinceSid: '37'
    },
    {
      sid: '370405',
      name: '台儿庄区',
      citySid: '3704',
      provinceSid: '37'
    },
    {
      sid: '370406',
      name: '山亭区',
      citySid: '3704',
      provinceSid: '37'
    },
    {
      sid: '370481',
      name: '滕州市',
      citySid: '3704',
      provinceSid: '37'
    },
    {
      sid: '370502',
      name: '东营区',
      citySid: '3705',
      provinceSid: '37'
    },
    {
      sid: '370503',
      name: '河口区',
      citySid: '3705',
      provinceSid: '37'
    },
    {
      sid: '370505',
      name: '垦利区',
      citySid: '3705',
      provinceSid: '37'
    },
    {
      sid: '370522',
      name: '利津县',
      citySid: '3705',
      provinceSid: '37'
    },
    {
      sid: '370523',
      name: '广饶县',
      citySid: '3705',
      provinceSid: '37'
    },
    {
      sid: '370571',
      name: '东营经济技术开发区',
      citySid: '3705',
      provinceSid: '37'
    },
    {
      sid: '370572',
      name: '东营港经济开发区',
      citySid: '3705',
      provinceSid: '37'
    },
    {
      sid: '370602',
      name: '芝罘区',
      citySid: '3706',
      provinceSid: '37'
    },
    {
      sid: '370611',
      name: '福山区',
      citySid: '3706',
      provinceSid: '37'
    },
    {
      sid: '370612',
      name: '牟平区',
      citySid: '3706',
      provinceSid: '37'
    },
    {
      sid: '370613',
      name: '莱山区',
      citySid: '3706',
      provinceSid: '37'
    },
    {
      sid: '370634',
      name: '长岛县',
      citySid: '3706',
      provinceSid: '37'
    },
    {
      sid: '370671',
      name: '烟台高新技术产业开发区',
      citySid: '3706',
      provinceSid: '37'
    },
    {
      sid: '370672',
      name: '烟台经济技术开发区',
      citySid: '3706',
      provinceSid: '37'
    },
    {
      sid: '370681',
      name: '龙口市',
      citySid: '3706',
      provinceSid: '37'
    },
    {
      sid: '370682',
      name: '莱阳市',
      citySid: '3706',
      provinceSid: '37'
    },
    {
      sid: '370683',
      name: '莱州市',
      citySid: '3706',
      provinceSid: '37'
    },
    {
      sid: '370684',
      name: '蓬莱市',
      citySid: '3706',
      provinceSid: '37'
    },
    {
      sid: '370685',
      name: '招远市',
      citySid: '3706',
      provinceSid: '37'
    },
    {
      sid: '370686',
      name: '栖霞市',
      citySid: '3706',
      provinceSid: '37'
    },
    {
      sid: '370687',
      name: '海阳市',
      citySid: '3706',
      provinceSid: '37'
    },
    {
      sid: '370702',
      name: '潍城区',
      citySid: '3707',
      provinceSid: '37'
    },
    {
      sid: '370703',
      name: '寒亭区',
      citySid: '3707',
      provinceSid: '37'
    },
    {
      sid: '370704',
      name: '坊子区',
      citySid: '3707',
      provinceSid: '37'
    },
    {
      sid: '370705',
      name: '奎文区',
      citySid: '3707',
      provinceSid: '37'
    },
    {
      sid: '370724',
      name: '临朐县',
      citySid: '3707',
      provinceSid: '37'
    },
    {
      sid: '370725',
      name: '昌乐县',
      citySid: '3707',
      provinceSid: '37'
    },
    {
      sid: '370772',
      name: '潍坊滨海经济技术开发区',
      citySid: '3707',
      provinceSid: '37'
    },
    {
      sid: '370781',
      name: '青州市',
      citySid: '3707',
      provinceSid: '37'
    },
    {
      sid: '370782',
      name: '诸城市',
      citySid: '3707',
      provinceSid: '37'
    },
    {
      sid: '370783',
      name: '寿光市',
      citySid: '3707',
      provinceSid: '37'
    },
    {
      sid: '370784',
      name: '安丘市',
      citySid: '3707',
      provinceSid: '37'
    },
    {
      sid: '370785',
      name: '高密市',
      citySid: '3707',
      provinceSid: '37'
    },
    {
      sid: '370786',
      name: '昌邑市',
      citySid: '3707',
      provinceSid: '37'
    },
    {
      sid: '370811',
      name: '任城区',
      citySid: '3708',
      provinceSid: '37'
    },
    {
      sid: '370812',
      name: '兖州区',
      citySid: '3708',
      provinceSid: '37'
    },
    {
      sid: '370826',
      name: '微山县',
      citySid: '3708',
      provinceSid: '37'
    },
    {
      sid: '370827',
      name: '鱼台县',
      citySid: '3708',
      provinceSid: '37'
    },
    {
      sid: '370828',
      name: '金乡县',
      citySid: '3708',
      provinceSid: '37'
    },
    {
      sid: '370829',
      name: '嘉祥县',
      citySid: '3708',
      provinceSid: '37'
    },
    {
      sid: '370830',
      name: '汶上县',
      citySid: '3708',
      provinceSid: '37'
    },
    {
      sid: '370831',
      name: '泗水县',
      citySid: '3708',
      provinceSid: '37'
    },
    {
      sid: '370832',
      name: '梁山县',
      citySid: '3708',
      provinceSid: '37'
    },
    {
      sid: '370871',
      name: '济宁高新技术产业开发区',
      citySid: '3708',
      provinceSid: '37'
    },
    {
      sid: '370881',
      name: '曲阜市',
      citySid: '3708',
      provinceSid: '37'
    },
    {
      sid: '370883',
      name: '邹城市',
      citySid: '3708',
      provinceSid: '37'
    },
    {
      sid: '370902',
      name: '泰山区',
      citySid: '3709',
      provinceSid: '37'
    },
    {
      sid: '370911',
      name: '岱岳区',
      citySid: '3709',
      provinceSid: '37'
    },
    {
      sid: '370921',
      name: '宁阳县',
      citySid: '3709',
      provinceSid: '37'
    },
    {
      sid: '370923',
      name: '东平县',
      citySid: '3709',
      provinceSid: '37'
    },
    {
      sid: '370982',
      name: '新泰市',
      citySid: '3709',
      provinceSid: '37'
    },
    {
      sid: '370983',
      name: '肥城市',
      citySid: '3709',
      provinceSid: '37'
    },
    {
      sid: '371002',
      name: '环翠区',
      citySid: '3710',
      provinceSid: '37'
    },
    {
      sid: '371003',
      name: '文登区',
      citySid: '3710',
      provinceSid: '37'
    },
    {
      sid: '371071',
      name: '威海火炬高技术产业开发区',
      citySid: '3710',
      provinceSid: '37'
    },
    {
      sid: '371072',
      name: '威海经济技术开发区',
      citySid: '3710',
      provinceSid: '37'
    },
    {
      sid: '371073',
      name: '威海临港经济技术开发区',
      citySid: '3710',
      provinceSid: '37'
    },
    {
      sid: '371082',
      name: '荣成市',
      citySid: '3710',
      provinceSid: '37'
    },
    {
      sid: '371083',
      name: '乳山市',
      citySid: '3710',
      provinceSid: '37'
    },
    {
      sid: '371102',
      name: '东港区',
      citySid: '3711',
      provinceSid: '37'
    },
    {
      sid: '371103',
      name: '岚山区',
      citySid: '3711',
      provinceSid: '37'
    },
    {
      sid: '371121',
      name: '五莲县',
      citySid: '3711',
      provinceSid: '37'
    },
    {
      sid: '371122',
      name: '莒县',
      citySid: '3711',
      provinceSid: '37'
    },
    {
      sid: '371171',
      name: '日照经济技术开发区',
      citySid: '3711',
      provinceSid: '37'
    },
    {
      sid: '371172',
      name: '日照国际海洋城',
      citySid: '3711',
      provinceSid: '37'
    },
    {
      sid: '371202',
      name: '莱城区',
      citySid: '3712',
      provinceSid: '37'
    },
    {
      sid: '371203',
      name: '钢城区',
      citySid: '3712',
      provinceSid: '37'
    },
    {
      sid: '371302',
      name: '兰山区',
      citySid: '3713',
      provinceSid: '37'
    },
    {
      sid: '371311',
      name: '罗庄区',
      citySid: '3713',
      provinceSid: '37'
    },
    {
      sid: '371312',
      name: '河东区',
      citySid: '3713',
      provinceSid: '37'
    },
    {
      sid: '371321',
      name: '沂南县',
      citySid: '3713',
      provinceSid: '37'
    },
    {
      sid: '371322',
      name: '郯城县',
      citySid: '3713',
      provinceSid: '37'
    },
    {
      sid: '371323',
      name: '沂水县',
      citySid: '3713',
      provinceSid: '37'
    },
    {
      sid: '371324',
      name: '兰陵县',
      citySid: '3713',
      provinceSid: '37'
    },
    {
      sid: '371325',
      name: '费县',
      citySid: '3713',
      provinceSid: '37'
    },
    {
      sid: '371326',
      name: '平邑县',
      citySid: '3713',
      provinceSid: '37'
    },
    {
      sid: '371327',
      name: '莒南县',
      citySid: '3713',
      provinceSid: '37'
    },
    {
      sid: '371328',
      name: '蒙阴县',
      citySid: '3713',
      provinceSid: '37'
    },
    {
      sid: '371329',
      name: '临沭县',
      citySid: '3713',
      provinceSid: '37'
    },
    {
      sid: '371371',
      name: '临沂高新技术产业开发区',
      citySid: '3713',
      provinceSid: '37'
    },
    {
      sid: '371372',
      name: '临沂经济技术开发区',
      citySid: '3713',
      provinceSid: '37'
    },
    {
      sid: '371373',
      name: '临沂临港经济开发区',
      citySid: '3713',
      provinceSid: '37'
    },
    {
      sid: '371402',
      name: '德城区',
      citySid: '3714',
      provinceSid: '37'
    },
    {
      sid: '371403',
      name: '陵城区',
      citySid: '3714',
      provinceSid: '37'
    },
    {
      sid: '371422',
      name: '宁津县',
      citySid: '3714',
      provinceSid: '37'
    },
    {
      sid: '371423',
      name: '庆云县',
      citySid: '3714',
      provinceSid: '37'
    },
    {
      sid: '371424',
      name: '临邑县',
      citySid: '3714',
      provinceSid: '37'
    },
    {
      sid: '371425',
      name: '齐河县',
      citySid: '3714',
      provinceSid: '37'
    },
    {
      sid: '371426',
      name: '平原县',
      citySid: '3714',
      provinceSid: '37'
    },
    {
      sid: '371427',
      name: '夏津县',
      citySid: '3714',
      provinceSid: '37'
    },
    {
      sid: '371428',
      name: '武城县',
      citySid: '3714',
      provinceSid: '37'
    },
    {
      sid: '371471',
      name: '德州经济技术开发区',
      citySid: '3714',
      provinceSid: '37'
    },
    {
      sid: '371472',
      name: '德州运河经济开发区',
      citySid: '3714',
      provinceSid: '37'
    },
    {
      sid: '371481',
      name: '乐陵市',
      citySid: '3714',
      provinceSid: '37'
    },
    {
      sid: '371482',
      name: '禹城市',
      citySid: '3714',
      provinceSid: '37'
    },
    {
      sid: '371502',
      name: '东昌府区',
      citySid: '3715',
      provinceSid: '37'
    },
    {
      sid: '371521',
      name: '阳谷县',
      citySid: '3715',
      provinceSid: '37'
    },
    {
      sid: '371522',
      name: '莘县',
      citySid: '3715',
      provinceSid: '37'
    },
    {
      sid: '371523',
      name: '茌平县',
      citySid: '3715',
      provinceSid: '37'
    },
    {
      sid: '371524',
      name: '东阿县',
      citySid: '3715',
      provinceSid: '37'
    },
    {
      sid: '371525',
      name: '冠县',
      citySid: '3715',
      provinceSid: '37'
    },
    {
      sid: '371526',
      name: '高唐县',
      citySid: '3715',
      provinceSid: '37'
    },
    {
      sid: '371581',
      name: '临清市',
      citySid: '3715',
      provinceSid: '37'
    },
    {
      sid: '371602',
      name: '滨城区',
      citySid: '3716',
      provinceSid: '37'
    },
    {
      sid: '371603',
      name: '沾化区',
      citySid: '3716',
      provinceSid: '37'
    },
    {
      sid: '371621',
      name: '惠民县',
      citySid: '3716',
      provinceSid: '37'
    },
    {
      sid: '371622',
      name: '阳信县',
      citySid: '3716',
      provinceSid: '37'
    },
    {
      sid: '371623',
      name: '无棣县',
      citySid: '3716',
      provinceSid: '37'
    },
    {
      sid: '371625',
      name: '博兴县',
      citySid: '3716',
      provinceSid: '37'
    },
    {
      sid: '371626',
      name: '邹平县',
      citySid: '3716',
      provinceSid: '37'
    },
    {
      sid: '371702',
      name: '牡丹区',
      citySid: '3717',
      provinceSid: '37'
    },
    {
      sid: '371703',
      name: '定陶区',
      citySid: '3717',
      provinceSid: '37'
    },
    {
      sid: '371721',
      name: '曹县',
      citySid: '3717',
      provinceSid: '37'
    },
    {
      sid: '371722',
      name: '单县',
      citySid: '3717',
      provinceSid: '37'
    },
    {
      sid: '371723',
      name: '成武县',
      citySid: '3717',
      provinceSid: '37'
    },
    {
      sid: '371724',
      name: '巨野县',
      citySid: '3717',
      provinceSid: '37'
    },
    {
      sid: '371725',
      name: '郓城县',
      citySid: '3717',
      provinceSid: '37'
    },
    {
      sid: '371726',
      name: '鄄城县',
      citySid: '3717',
      provinceSid: '37'
    },
    {
      sid: '371728',
      name: '东明县',
      citySid: '3717',
      provinceSid: '37'
    },
    {
      sid: '371771',
      name: '菏泽经济技术开发区',
      citySid: '3717',
      provinceSid: '37'
    },
    {
      sid: '371772',
      name: '菏泽高新技术开发区',
      citySid: '3717',
      provinceSid: '37'
    },
    {
      sid: '410102',
      name: '中原区',
      citySid: '4101',
      provinceSid: '41'
    },
    {
      sid: '410103',
      name: '二七区',
      citySid: '4101',
      provinceSid: '41'
    },
    {
      sid: '410104',
      name: '管城回族区',
      citySid: '4101',
      provinceSid: '41'
    },
    {
      sid: '410105',
      name: '金水区',
      citySid: '4101',
      provinceSid: '41'
    },
    {
      sid: '410106',
      name: '上街区',
      citySid: '4101',
      provinceSid: '41'
    },
    {
      sid: '410108',
      name: '惠济区',
      citySid: '4101',
      provinceSid: '41'
    },
    {
      sid: '410122',
      name: '中牟县',
      citySid: '4101',
      provinceSid: '41'
    },
    {
      sid: '410171',
      name: '郑州经济技术开发区',
      citySid: '4101',
      provinceSid: '41'
    },
    {
      sid: '410172',
      name: '郑州高新技术产业开发区',
      citySid: '4101',
      provinceSid: '41'
    },
    {
      sid: '410173',
      name: '郑州航空港经济综合实验区',
      citySid: '4101',
      provinceSid: '41'
    },
    {
      sid: '410181',
      name: '巩义市',
      citySid: '4101',
      provinceSid: '41'
    },
    {
      sid: '410182',
      name: '荥阳市',
      citySid: '4101',
      provinceSid: '41'
    },
    {
      sid: '410183',
      name: '新密市',
      citySid: '4101',
      provinceSid: '41'
    },
    {
      sid: '410184',
      name: '新郑市',
      citySid: '4101',
      provinceSid: '41'
    },
    {
      sid: '410185',
      name: '登封市',
      citySid: '4101',
      provinceSid: '41'
    },
    {
      sid: '410202',
      name: '龙亭区',
      citySid: '4102',
      provinceSid: '41'
    },
    {
      sid: '410203',
      name: '顺河回族区',
      citySid: '4102',
      provinceSid: '41'
    },
    {
      sid: '410204',
      name: '鼓楼区',
      citySid: '4102',
      provinceSid: '41'
    },
    {
      sid: '410205',
      name: '禹王台区',
      citySid: '4102',
      provinceSid: '41'
    },
    {
      sid: '410212',
      name: '祥符区',
      citySid: '4102',
      provinceSid: '41'
    },
    {
      sid: '410221',
      name: '杞县',
      citySid: '4102',
      provinceSid: '41'
    },
    {
      sid: '410222',
      name: '通许县',
      citySid: '4102',
      provinceSid: '41'
    },
    {
      sid: '410223',
      name: '尉氏县',
      citySid: '4102',
      provinceSid: '41'
    },
    {
      sid: '410225',
      name: '兰考县',
      citySid: '4102',
      provinceSid: '41'
    },
    {
      sid: '410302',
      name: '老城区',
      citySid: '4103',
      provinceSid: '41'
    },
    {
      sid: '410303',
      name: '西工区',
      citySid: '4103',
      provinceSid: '41'
    },
    {
      sid: '410304',
      name: '瀍河回族区',
      citySid: '4103',
      provinceSid: '41'
    },
    {
      sid: '410305',
      name: '涧西区',
      citySid: '4103',
      provinceSid: '41'
    },
    {
      sid: '410306',
      name: '吉利区',
      citySid: '4103',
      provinceSid: '41'
    },
    {
      sid: '410311',
      name: '洛龙区',
      citySid: '4103',
      provinceSid: '41'
    },
    {
      sid: '410322',
      name: '孟津县',
      citySid: '4103',
      provinceSid: '41'
    },
    {
      sid: '410323',
      name: '新安县',
      citySid: '4103',
      provinceSid: '41'
    },
    {
      sid: '410324',
      name: '栾川县',
      citySid: '4103',
      provinceSid: '41'
    },
    {
      sid: '410325',
      name: '嵩县',
      citySid: '4103',
      provinceSid: '41'
    },
    {
      sid: '410326',
      name: '汝阳县',
      citySid: '4103',
      provinceSid: '41'
    },
    {
      sid: '410327',
      name: '宜阳县',
      citySid: '4103',
      provinceSid: '41'
    },
    {
      sid: '410328',
      name: '洛宁县',
      citySid: '4103',
      provinceSid: '41'
    },
    {
      sid: '410329',
      name: '伊川县',
      citySid: '4103',
      provinceSid: '41'
    },
    {
      sid: '410371',
      name: '洛阳高新技术产业开发区',
      citySid: '4103',
      provinceSid: '41'
    },
    {
      sid: '410381',
      name: '偃师市',
      citySid: '4103',
      provinceSid: '41'
    },
    {
      sid: '410402',
      name: '新华区',
      citySid: '4104',
      provinceSid: '41'
    },
    {
      sid: '410403',
      name: '卫东区',
      citySid: '4104',
      provinceSid: '41'
    },
    {
      sid: '410404',
      name: '石龙区',
      citySid: '4104',
      provinceSid: '41'
    },
    {
      sid: '410411',
      name: '湛河区',
      citySid: '4104',
      provinceSid: '41'
    },
    {
      sid: '410421',
      name: '宝丰县',
      citySid: '4104',
      provinceSid: '41'
    },
    {
      sid: '410422',
      name: '叶县',
      citySid: '4104',
      provinceSid: '41'
    },
    {
      sid: '410423',
      name: '鲁山县',
      citySid: '4104',
      provinceSid: '41'
    },
    {
      sid: '410425',
      name: '郏县',
      citySid: '4104',
      provinceSid: '41'
    },
    {
      sid: '410471',
      name: '平顶山高新技术产业开发区',
      citySid: '4104',
      provinceSid: '41'
    },
    {
      sid: '410472',
      name: '平顶山市新城区',
      citySid: '4104',
      provinceSid: '41'
    },
    {
      sid: '410481',
      name: '舞钢市',
      citySid: '4104',
      provinceSid: '41'
    },
    {
      sid: '410482',
      name: '汝州市',
      citySid: '4104',
      provinceSid: '41'
    },
    {
      sid: '410502',
      name: '文峰区',
      citySid: '4105',
      provinceSid: '41'
    },
    {
      sid: '410503',
      name: '北关区',
      citySid: '4105',
      provinceSid: '41'
    },
    {
      sid: '410505',
      name: '殷都区',
      citySid: '4105',
      provinceSid: '41'
    },
    {
      sid: '410506',
      name: '龙安区',
      citySid: '4105',
      provinceSid: '41'
    },
    {
      sid: '410522',
      name: '安阳县',
      citySid: '4105',
      provinceSid: '41'
    },
    {
      sid: '410523',
      name: '汤阴县',
      citySid: '4105',
      provinceSid: '41'
    },
    {
      sid: '410526',
      name: '滑县',
      citySid: '4105',
      provinceSid: '41'
    },
    {
      sid: '410527',
      name: '内黄县',
      citySid: '4105',
      provinceSid: '41'
    },
    {
      sid: '410571',
      name: '安阳高新技术产业开发区',
      citySid: '4105',
      provinceSid: '41'
    },
    {
      sid: '410581',
      name: '林州市',
      citySid: '4105',
      provinceSid: '41'
    },
    {
      sid: '410602',
      name: '鹤山区',
      citySid: '4106',
      provinceSid: '41'
    },
    {
      sid: '410603',
      name: '山城区',
      citySid: '4106',
      provinceSid: '41'
    },
    {
      sid: '410611',
      name: '淇滨区',
      citySid: '4106',
      provinceSid: '41'
    },
    {
      sid: '410621',
      name: '浚县',
      citySid: '4106',
      provinceSid: '41'
    },
    {
      sid: '410622',
      name: '淇县',
      citySid: '4106',
      provinceSid: '41'
    },
    {
      sid: '410671',
      name: '鹤壁经济技术开发区',
      citySid: '4106',
      provinceSid: '41'
    },
    {
      sid: '410702',
      name: '红旗区',
      citySid: '4107',
      provinceSid: '41'
    },
    {
      sid: '410703',
      name: '卫滨区',
      citySid: '4107',
      provinceSid: '41'
    },
    {
      sid: '410704',
      name: '凤泉区',
      citySid: '4107',
      provinceSid: '41'
    },
    {
      sid: '410711',
      name: '牧野区',
      citySid: '4107',
      provinceSid: '41'
    },
    {
      sid: '410721',
      name: '新乡县',
      citySid: '4107',
      provinceSid: '41'
    },
    {
      sid: '410724',
      name: '获嘉县',
      citySid: '4107',
      provinceSid: '41'
    },
    {
      sid: '410725',
      name: '原阳县',
      citySid: '4107',
      provinceSid: '41'
    },
    {
      sid: '410726',
      name: '延津县',
      citySid: '4107',
      provinceSid: '41'
    },
    {
      sid: '410727',
      name: '封丘县',
      citySid: '4107',
      provinceSid: '41'
    },
    {
      sid: '410728',
      name: '长垣县',
      citySid: '4107',
      provinceSid: '41'
    },
    {
      sid: '410771',
      name: '新乡高新技术产业开发区',
      citySid: '4107',
      provinceSid: '41'
    },
    {
      sid: '410772',
      name: '新乡经济技术开发区',
      citySid: '4107',
      provinceSid: '41'
    },
    {
      sid: '410773',
      name: '新乡市平原城乡一体化示范区',
      citySid: '4107',
      provinceSid: '41'
    },
    {
      sid: '410781',
      name: '卫辉市',
      citySid: '4107',
      provinceSid: '41'
    },
    {
      sid: '410782',
      name: '辉县市',
      citySid: '4107',
      provinceSid: '41'
    },
    {
      sid: '410802',
      name: '解放区',
      citySid: '4108',
      provinceSid: '41'
    },
    {
      sid: '410803',
      name: '中站区',
      citySid: '4108',
      provinceSid: '41'
    },
    {
      sid: '410804',
      name: '马村区',
      citySid: '4108',
      provinceSid: '41'
    },
    {
      sid: '410811',
      name: '山阳区',
      citySid: '4108',
      provinceSid: '41'
    },
    {
      sid: '410821',
      name: '修武县',
      citySid: '4108',
      provinceSid: '41'
    },
    {
      sid: '410822',
      name: '博爱县',
      citySid: '4108',
      provinceSid: '41'
    },
    {
      sid: '410823',
      name: '武陟县',
      citySid: '4108',
      provinceSid: '41'
    },
    {
      sid: '410825',
      name: '温县',
      citySid: '4108',
      provinceSid: '41'
    },
    {
      sid: '410871',
      name: '焦作城乡一体化示范区',
      citySid: '4108',
      provinceSid: '41'
    },
    {
      sid: '410882',
      name: '沁阳市',
      citySid: '4108',
      provinceSid: '41'
    },
    {
      sid: '410883',
      name: '孟州市',
      citySid: '4108',
      provinceSid: '41'
    },
    {
      sid: '410902',
      name: '华龙区',
      citySid: '4109',
      provinceSid: '41'
    },
    {
      sid: '410922',
      name: '清丰县',
      citySid: '4109',
      provinceSid: '41'
    },
    {
      sid: '410923',
      name: '南乐县',
      citySid: '4109',
      provinceSid: '41'
    },
    {
      sid: '410926',
      name: '范县',
      citySid: '4109',
      provinceSid: '41'
    },
    {
      sid: '410927',
      name: '台前县',
      citySid: '4109',
      provinceSid: '41'
    },
    {
      sid: '410928',
      name: '濮阳县',
      citySid: '4109',
      provinceSid: '41'
    },
    {
      sid: '410971',
      name: '河南濮阳工业园区',
      citySid: '4109',
      provinceSid: '41'
    },
    {
      sid: '410972',
      name: '濮阳经济技术开发区',
      citySid: '4109',
      provinceSid: '41'
    },
    {
      sid: '411002',
      name: '魏都区',
      citySid: '4110',
      provinceSid: '41'
    },
    {
      sid: '411003',
      name: '建安区',
      citySid: '4110',
      provinceSid: '41'
    },
    {
      sid: '411024',
      name: '鄢陵县',
      citySid: '4110',
      provinceSid: '41'
    },
    {
      sid: '411025',
      name: '襄城县',
      citySid: '4110',
      provinceSid: '41'
    },
    {
      sid: '411071',
      name: '许昌经济技术开发区',
      citySid: '4110',
      provinceSid: '41'
    },
    {
      sid: '411081',
      name: '禹州市',
      citySid: '4110',
      provinceSid: '41'
    },
    {
      sid: '411082',
      name: '长葛市',
      citySid: '4110',
      provinceSid: '41'
    },
    {
      sid: '411102',
      name: '源汇区',
      citySid: '4111',
      provinceSid: '41'
    },
    {
      sid: '411103',
      name: '郾城区',
      citySid: '4111',
      provinceSid: '41'
    },
    {
      sid: '411104',
      name: '召陵区',
      citySid: '4111',
      provinceSid: '41'
    },
    {
      sid: '411121',
      name: '舞阳县',
      citySid: '4111',
      provinceSid: '41'
    },
    {
      sid: '411122',
      name: '临颍县',
      citySid: '4111',
      provinceSid: '41'
    },
    {
      sid: '411171',
      name: '漯河经济技术开发区',
      citySid: '4111',
      provinceSid: '41'
    },
    {
      sid: '411202',
      name: '湖滨区',
      citySid: '4112',
      provinceSid: '41'
    },
    {
      sid: '411203',
      name: '陕州区',
      citySid: '4112',
      provinceSid: '41'
    },
    {
      sid: '411221',
      name: '渑池县',
      citySid: '4112',
      provinceSid: '41'
    },
    {
      sid: '411224',
      name: '卢氏县',
      citySid: '4112',
      provinceSid: '41'
    },
    {
      sid: '411271',
      name: '河南三门峡经济开发区',
      citySid: '4112',
      provinceSid: '41'
    },
    {
      sid: '411281',
      name: '义马市',
      citySid: '4112',
      provinceSid: '41'
    },
    {
      sid: '411282',
      name: '灵宝市',
      citySid: '4112',
      provinceSid: '41'
    },
    {
      sid: '411302',
      name: '宛城区',
      citySid: '4113',
      provinceSid: '41'
    },
    {
      sid: '411303',
      name: '卧龙区',
      citySid: '4113',
      provinceSid: '41'
    },
    {
      sid: '411321',
      name: '南召县',
      citySid: '4113',
      provinceSid: '41'
    },
    {
      sid: '411322',
      name: '方城县',
      citySid: '4113',
      provinceSid: '41'
    },
    {
      sid: '411323',
      name: '西峡县',
      citySid: '4113',
      provinceSid: '41'
    },
    {
      sid: '411324',
      name: '镇平县',
      citySid: '4113',
      provinceSid: '41'
    },
    {
      sid: '411325',
      name: '内乡县',
      citySid: '4113',
      provinceSid: '41'
    },
    {
      sid: '411326',
      name: '淅川县',
      citySid: '4113',
      provinceSid: '41'
    },
    {
      sid: '411327',
      name: '社旗县',
      citySid: '4113',
      provinceSid: '41'
    },
    {
      sid: '411328',
      name: '唐河县',
      citySid: '4113',
      provinceSid: '41'
    },
    {
      sid: '411329',
      name: '新野县',
      citySid: '4113',
      provinceSid: '41'
    },
    {
      sid: '411330',
      name: '桐柏县',
      citySid: '4113',
      provinceSid: '41'
    },
    {
      sid: '411371',
      name: '南阳高新技术产业开发区',
      citySid: '4113',
      provinceSid: '41'
    },
    {
      sid: '411372',
      name: '南阳市城乡一体化示范区',
      citySid: '4113',
      provinceSid: '41'
    },
    {
      sid: '411381',
      name: '邓州市',
      citySid: '4113',
      provinceSid: '41'
    },
    {
      sid: '411402',
      name: '梁园区',
      citySid: '4114',
      provinceSid: '41'
    },
    {
      sid: '411403',
      name: '睢阳区',
      citySid: '4114',
      provinceSid: '41'
    },
    {
      sid: '411421',
      name: '民权县',
      citySid: '4114',
      provinceSid: '41'
    },
    {
      sid: '411422',
      name: '睢县',
      citySid: '4114',
      provinceSid: '41'
    },
    {
      sid: '411423',
      name: '宁陵县',
      citySid: '4114',
      provinceSid: '41'
    },
    {
      sid: '411424',
      name: '柘城县',
      citySid: '4114',
      provinceSid: '41'
    },
    {
      sid: '411425',
      name: '虞城县',
      citySid: '4114',
      provinceSid: '41'
    },
    {
      sid: '411426',
      name: '夏邑县',
      citySid: '4114',
      provinceSid: '41'
    },
    {
      sid: '411471',
      name: '豫东综合物流产业聚集区',
      citySid: '4114',
      provinceSid: '41'
    },
    {
      sid: '411472',
      name: '河南商丘经济开发区',
      citySid: '4114',
      provinceSid: '41'
    },
    {
      sid: '411481',
      name: '永城市',
      citySid: '4114',
      provinceSid: '41'
    },
    {
      sid: '411502',
      name: '浉河区',
      citySid: '4115',
      provinceSid: '41'
    },
    {
      sid: '411503',
      name: '平桥区',
      citySid: '4115',
      provinceSid: '41'
    },
    {
      sid: '411521',
      name: '罗山县',
      citySid: '4115',
      provinceSid: '41'
    },
    {
      sid: '411522',
      name: '光山县',
      citySid: '4115',
      provinceSid: '41'
    },
    {
      sid: '411523',
      name: '新县',
      citySid: '4115',
      provinceSid: '41'
    },
    {
      sid: '411524',
      name: '商城县',
      citySid: '4115',
      provinceSid: '41'
    },
    {
      sid: '411525',
      name: '固始县',
      citySid: '4115',
      provinceSid: '41'
    },
    {
      sid: '411526',
      name: '潢川县',
      citySid: '4115',
      provinceSid: '41'
    },
    {
      sid: '411527',
      name: '淮滨县',
      citySid: '4115',
      provinceSid: '41'
    },
    {
      sid: '411528',
      name: '息县',
      citySid: '4115',
      provinceSid: '41'
    },
    {
      sid: '411571',
      name: '信阳高新技术产业开发区',
      citySid: '4115',
      provinceSid: '41'
    },
    {
      sid: '411602',
      name: '川汇区',
      citySid: '4116',
      provinceSid: '41'
    },
    {
      sid: '411621',
      name: '扶沟县',
      citySid: '4116',
      provinceSid: '41'
    },
    {
      sid: '411622',
      name: '西华县',
      citySid: '4116',
      provinceSid: '41'
    },
    {
      sid: '411623',
      name: '商水县',
      citySid: '4116',
      provinceSid: '41'
    },
    {
      sid: '411624',
      name: '沈丘县',
      citySid: '4116',
      provinceSid: '41'
    },
    {
      sid: '411625',
      name: '郸城县',
      citySid: '4116',
      provinceSid: '41'
    },
    {
      sid: '411626',
      name: '淮阳县',
      citySid: '4116',
      provinceSid: '41'
    },
    {
      sid: '411627',
      name: '太康县',
      citySid: '4116',
      provinceSid: '41'
    },
    {
      sid: '411628',
      name: '鹿邑县',
      citySid: '4116',
      provinceSid: '41'
    },
    {
      sid: '411671',
      name: '河南周口经济开发区',
      citySid: '4116',
      provinceSid: '41'
    },
    {
      sid: '411681',
      name: '项城市',
      citySid: '4116',
      provinceSid: '41'
    },
    {
      sid: '411702',
      name: '驿城区',
      citySid: '4117',
      provinceSid: '41'
    },
    {
      sid: '411721',
      name: '西平县',
      citySid: '4117',
      provinceSid: '41'
    },
    {
      sid: '411722',
      name: '上蔡县',
      citySid: '4117',
      provinceSid: '41'
    },
    {
      sid: '411723',
      name: '平舆县',
      citySid: '4117',
      provinceSid: '41'
    },
    {
      sid: '411724',
      name: '正阳县',
      citySid: '4117',
      provinceSid: '41'
    },
    {
      sid: '411725',
      name: '确山县',
      citySid: '4117',
      provinceSid: '41'
    },
    {
      sid: '411726',
      name: '泌阳县',
      citySid: '4117',
      provinceSid: '41'
    },
    {
      sid: '411727',
      name: '汝南县',
      citySid: '4117',
      provinceSid: '41'
    },
    {
      sid: '411728',
      name: '遂平县',
      citySid: '4117',
      provinceSid: '41'
    },
    {
      sid: '411729',
      name: '新蔡县',
      citySid: '4117',
      provinceSid: '41'
    },
    {
      sid: '411771',
      name: '河南驻马店经济开发区',
      citySid: '4117',
      provinceSid: '41'
    },
    {
      sid: '419001',
      name: '济源市',
      citySid: '4190',
      provinceSid: '41'
    },
    {
      sid: '420102',
      name: '江岸区',
      citySid: '4201',
      provinceSid: '42'
    },
    {
      sid: '420103',
      name: '江汉区',
      citySid: '4201',
      provinceSid: '42'
    },
    {
      sid: '420104',
      name: '硚口区',
      citySid: '4201',
      provinceSid: '42'
    },
    {
      sid: '420105',
      name: '汉阳区',
      citySid: '4201',
      provinceSid: '42'
    },
    {
      sid: '420106',
      name: '武昌区',
      citySid: '4201',
      provinceSid: '42'
    },
    {
      sid: '420107',
      name: '青山区',
      citySid: '4201',
      provinceSid: '42'
    },
    {
      sid: '420111',
      name: '洪山区',
      citySid: '4201',
      provinceSid: '42'
    },
    {
      sid: '420112',
      name: '东西湖区',
      citySid: '4201',
      provinceSid: '42'
    },
    {
      sid: '420113',
      name: '汉南区',
      citySid: '4201',
      provinceSid: '42'
    },
    {
      sid: '420114',
      name: '蔡甸区',
      citySid: '4201',
      provinceSid: '42'
    },
    {
      sid: '420115',
      name: '江夏区',
      citySid: '4201',
      provinceSid: '42'
    },
    {
      sid: '420116',
      name: '黄陂区',
      citySid: '4201',
      provinceSid: '42'
    },
    {
      sid: '420117',
      name: '新洲区',
      citySid: '4201',
      provinceSid: '42'
    },
    {
      sid: '420202',
      name: '黄石港区',
      citySid: '4202',
      provinceSid: '42'
    },
    {
      sid: '420203',
      name: '西塞山区',
      citySid: '4202',
      provinceSid: '42'
    },
    {
      sid: '420204',
      name: '下陆区',
      citySid: '4202',
      provinceSid: '42'
    },
    {
      sid: '420205',
      name: '铁山区',
      citySid: '4202',
      provinceSid: '42'
    },
    {
      sid: '420222',
      name: '阳新县',
      citySid: '4202',
      provinceSid: '42'
    },
    {
      sid: '420281',
      name: '大冶市',
      citySid: '4202',
      provinceSid: '42'
    },
    {
      sid: '420302',
      name: '茅箭区',
      citySid: '4203',
      provinceSid: '42'
    },
    {
      sid: '420303',
      name: '张湾区',
      citySid: '4203',
      provinceSid: '42'
    },
    {
      sid: '420304',
      name: '郧阳区',
      citySid: '4203',
      provinceSid: '42'
    },
    {
      sid: '420322',
      name: '郧西县',
      citySid: '4203',
      provinceSid: '42'
    },
    {
      sid: '420323',
      name: '竹山县',
      citySid: '4203',
      provinceSid: '42'
    },
    {
      sid: '420324',
      name: '竹溪县',
      citySid: '4203',
      provinceSid: '42'
    },
    {
      sid: '420325',
      name: '房县',
      citySid: '4203',
      provinceSid: '42'
    },
    {
      sid: '420381',
      name: '丹江口市',
      citySid: '4203',
      provinceSid: '42'
    },
    {
      sid: '420502',
      name: '西陵区',
      citySid: '4205',
      provinceSid: '42'
    },
    {
      sid: '420503',
      name: '伍家岗区',
      citySid: '4205',
      provinceSid: '42'
    },
    {
      sid: '420504',
      name: '点军区',
      citySid: '4205',
      provinceSid: '42'
    },
    {
      sid: '420505',
      name: '猇亭区',
      citySid: '4205',
      provinceSid: '42'
    },
    {
      sid: '420506',
      name: '夷陵区',
      citySid: '4205',
      provinceSid: '42'
    },
    {
      sid: '420525',
      name: '远安县',
      citySid: '4205',
      provinceSid: '42'
    },
    {
      sid: '420526',
      name: '兴山县',
      citySid: '4205',
      provinceSid: '42'
    },
    {
      sid: '420527',
      name: '秭归县',
      citySid: '4205',
      provinceSid: '42'
    },
    {
      sid: '420528',
      name: '长阳土家族自治县',
      citySid: '4205',
      provinceSid: '42'
    },
    {
      sid: '420529',
      name: '五峰土家族自治县',
      citySid: '4205',
      provinceSid: '42'
    },
    {
      sid: '420581',
      name: '宜都市',
      citySid: '4205',
      provinceSid: '42'
    },
    {
      sid: '420582',
      name: '当阳市',
      citySid: '4205',
      provinceSid: '42'
    },
    {
      sid: '420583',
      name: '枝江市',
      citySid: '4205',
      provinceSid: '42'
    },
    {
      sid: '420602',
      name: '襄城区',
      citySid: '4206',
      provinceSid: '42'
    },
    {
      sid: '420606',
      name: '樊城区',
      citySid: '4206',
      provinceSid: '42'
    },
    {
      sid: '420607',
      name: '襄州区',
      citySid: '4206',
      provinceSid: '42'
    },
    {
      sid: '420624',
      name: '南漳县',
      citySid: '4206',
      provinceSid: '42'
    },
    {
      sid: '420625',
      name: '谷城县',
      citySid: '4206',
      provinceSid: '42'
    },
    {
      sid: '420626',
      name: '保康县',
      citySid: '4206',
      provinceSid: '42'
    },
    {
      sid: '420682',
      name: '老河口市',
      citySid: '4206',
      provinceSid: '42'
    },
    {
      sid: '420683',
      name: '枣阳市',
      citySid: '4206',
      provinceSid: '42'
    },
    {
      sid: '420684',
      name: '宜城市',
      citySid: '4206',
      provinceSid: '42'
    },
    {
      sid: '420702',
      name: '梁子湖区',
      citySid: '4207',
      provinceSid: '42'
    },
    {
      sid: '420703',
      name: '华容区',
      citySid: '4207',
      provinceSid: '42'
    },
    {
      sid: '420704',
      name: '鄂城区',
      citySid: '4207',
      provinceSid: '42'
    },
    {
      sid: '420802',
      name: '东宝区',
      citySid: '4208',
      provinceSid: '42'
    },
    {
      sid: '420804',
      name: '掇刀区',
      citySid: '4208',
      provinceSid: '42'
    },
    {
      sid: '420821',
      name: '京山县',
      citySid: '4208',
      provinceSid: '42'
    },
    {
      sid: '420822',
      name: '沙洋县',
      citySid: '4208',
      provinceSid: '42'
    },
    {
      sid: '420881',
      name: '钟祥市',
      citySid: '4208',
      provinceSid: '42'
    },
    {
      sid: '420902',
      name: '孝南区',
      citySid: '4209',
      provinceSid: '42'
    },
    {
      sid: '420921',
      name: '孝昌县',
      citySid: '4209',
      provinceSid: '42'
    },
    {
      sid: '420922',
      name: '大悟县',
      citySid: '4209',
      provinceSid: '42'
    },
    {
      sid: '420923',
      name: '云梦县',
      citySid: '4209',
      provinceSid: '42'
    },
    {
      sid: '420981',
      name: '应城市',
      citySid: '4209',
      provinceSid: '42'
    },
    {
      sid: '420982',
      name: '安陆市',
      citySid: '4209',
      provinceSid: '42'
    },
    {
      sid: '420984',
      name: '汉川市',
      citySid: '4209',
      provinceSid: '42'
    },
    {
      sid: '421002',
      name: '沙市区',
      citySid: '4210',
      provinceSid: '42'
    },
    {
      sid: '421003',
      name: '荆州区',
      citySid: '4210',
      provinceSid: '42'
    },
    {
      sid: '421022',
      name: '公安县',
      citySid: '4210',
      provinceSid: '42'
    },
    {
      sid: '421023',
      name: '监利县',
      citySid: '4210',
      provinceSid: '42'
    },
    {
      sid: '421024',
      name: '江陵县',
      citySid: '4210',
      provinceSid: '42'
    },
    {
      sid: '421071',
      name: '荆州经济技术开发区',
      citySid: '4210',
      provinceSid: '42'
    },
    {
      sid: '421081',
      name: '石首市',
      citySid: '4210',
      provinceSid: '42'
    },
    {
      sid: '421083',
      name: '洪湖市',
      citySid: '4210',
      provinceSid: '42'
    },
    {
      sid: '421087',
      name: '松滋市',
      citySid: '4210',
      provinceSid: '42'
    },
    {
      sid: '421102',
      name: '黄州区',
      citySid: '4211',
      provinceSid: '42'
    },
    {
      sid: '421121',
      name: '团风县',
      citySid: '4211',
      provinceSid: '42'
    },
    {
      sid: '421122',
      name: '红安县',
      citySid: '4211',
      provinceSid: '42'
    },
    {
      sid: '421123',
      name: '罗田县',
      citySid: '4211',
      provinceSid: '42'
    },
    {
      sid: '421124',
      name: '英山县',
      citySid: '4211',
      provinceSid: '42'
    },
    {
      sid: '421125',
      name: '浠水县',
      citySid: '4211',
      provinceSid: '42'
    },
    {
      sid: '421126',
      name: '蕲春县',
      citySid: '4211',
      provinceSid: '42'
    },
    {
      sid: '421127',
      name: '黄梅县',
      citySid: '4211',
      provinceSid: '42'
    },
    {
      sid: '421171',
      name: '龙感湖管理区',
      citySid: '4211',
      provinceSid: '42'
    },
    {
      sid: '421181',
      name: '麻城市',
      citySid: '4211',
      provinceSid: '42'
    },
    {
      sid: '421182',
      name: '武穴市',
      citySid: '4211',
      provinceSid: '42'
    },
    {
      sid: '421202',
      name: '咸安区',
      citySid: '4212',
      provinceSid: '42'
    },
    {
      sid: '421221',
      name: '嘉鱼县',
      citySid: '4212',
      provinceSid: '42'
    },
    {
      sid: '421222',
      name: '通城县',
      citySid: '4212',
      provinceSid: '42'
    },
    {
      sid: '421223',
      name: '崇阳县',
      citySid: '4212',
      provinceSid: '42'
    },
    {
      sid: '421224',
      name: '通山县',
      citySid: '4212',
      provinceSid: '42'
    },
    {
      sid: '421281',
      name: '赤壁市',
      citySid: '4212',
      provinceSid: '42'
    },
    {
      sid: '421303',
      name: '曾都区',
      citySid: '4213',
      provinceSid: '42'
    },
    {
      sid: '421321',
      name: '随县',
      citySid: '4213',
      provinceSid: '42'
    },
    {
      sid: '421381',
      name: '广水市',
      citySid: '4213',
      provinceSid: '42'
    },
    {
      sid: '422801',
      name: '恩施市',
      citySid: '4228',
      provinceSid: '42'
    },
    {
      sid: '422802',
      name: '利川市',
      citySid: '4228',
      provinceSid: '42'
    },
    {
      sid: '422822',
      name: '建始县',
      citySid: '4228',
      provinceSid: '42'
    },
    {
      sid: '422823',
      name: '巴东县',
      citySid: '4228',
      provinceSid: '42'
    },
    {
      sid: '422825',
      name: '宣恩县',
      citySid: '4228',
      provinceSid: '42'
    },
    {
      sid: '422826',
      name: '咸丰县',
      citySid: '4228',
      provinceSid: '42'
    },
    {
      sid: '422827',
      name: '来凤县',
      citySid: '4228',
      provinceSid: '42'
    },
    {
      sid: '422828',
      name: '鹤峰县',
      citySid: '4228',
      provinceSid: '42'
    },
    {
      sid: '429004',
      name: '仙桃市',
      citySid: '4290',
      provinceSid: '42'
    },
    {
      sid: '429005',
      name: '潜江市',
      citySid: '4290',
      provinceSid: '42'
    },
    {
      sid: '429006',
      name: '天门市',
      citySid: '4290',
      provinceSid: '42'
    },
    {
      sid: '429021',
      name: '神农架林区',
      citySid: '4290',
      provinceSid: '42'
    },
    {
      sid: '430102',
      name: '芙蓉区',
      citySid: '4301',
      provinceSid: '43'
    },
    {
      sid: '430103',
      name: '天心区',
      citySid: '4301',
      provinceSid: '43'
    },
    {
      sid: '430104',
      name: '岳麓区',
      citySid: '4301',
      provinceSid: '43'
    },
    {
      sid: '430105',
      name: '开福区',
      citySid: '4301',
      provinceSid: '43'
    },
    {
      sid: '430111',
      name: '雨花区',
      citySid: '4301',
      provinceSid: '43'
    },
    {
      sid: '430112',
      name: '望城区',
      citySid: '4301',
      provinceSid: '43'
    },
    {
      sid: '430121',
      name: '长沙县',
      citySid: '4301',
      provinceSid: '43'
    },
    {
      sid: '430181',
      name: '浏阳市',
      citySid: '4301',
      provinceSid: '43'
    },
    {
      sid: '430182',
      name: '宁乡市',
      citySid: '4301',
      provinceSid: '43'
    },
    {
      sid: '430202',
      name: '荷塘区',
      citySid: '4302',
      provinceSid: '43'
    },
    {
      sid: '430203',
      name: '芦淞区',
      citySid: '4302',
      provinceSid: '43'
    },
    {
      sid: '430204',
      name: '石峰区',
      citySid: '4302',
      provinceSid: '43'
    },
    {
      sid: '430211',
      name: '天元区',
      citySid: '4302',
      provinceSid: '43'
    },
    {
      sid: '430221',
      name: '株洲县',
      citySid: '4302',
      provinceSid: '43'
    },
    {
      sid: '430223',
      name: '攸县',
      citySid: '4302',
      provinceSid: '43'
    },
    {
      sid: '430224',
      name: '茶陵县',
      citySid: '4302',
      provinceSid: '43'
    },
    {
      sid: '430225',
      name: '炎陵县',
      citySid: '4302',
      provinceSid: '43'
    },
    {
      sid: '430271',
      name: '云龙示范区',
      citySid: '4302',
      provinceSid: '43'
    },
    {
      sid: '430281',
      name: '醴陵市',
      citySid: '4302',
      provinceSid: '43'
    },
    {
      sid: '430302',
      name: '雨湖区',
      citySid: '4303',
      provinceSid: '43'
    },
    {
      sid: '430304',
      name: '岳塘区',
      citySid: '4303',
      provinceSid: '43'
    },
    {
      sid: '430321',
      name: '湘潭县',
      citySid: '4303',
      provinceSid: '43'
    },
    {
      sid: '430371',
      name: '湖南湘潭高新技术产业园区',
      citySid: '4303',
      provinceSid: '43'
    },
    {
      sid: '430372',
      name: '湘潭昭山示范区',
      citySid: '4303',
      provinceSid: '43'
    },
    {
      sid: '430373',
      name: '湘潭九华示范区',
      citySid: '4303',
      provinceSid: '43'
    },
    {
      sid: '430381',
      name: '湘乡市',
      citySid: '4303',
      provinceSid: '43'
    },
    {
      sid: '430382',
      name: '韶山市',
      citySid: '4303',
      provinceSid: '43'
    },
    {
      sid: '430405',
      name: '珠晖区',
      citySid: '4304',
      provinceSid: '43'
    },
    {
      sid: '430406',
      name: '雁峰区',
      citySid: '4304',
      provinceSid: '43'
    },
    {
      sid: '430407',
      name: '石鼓区',
      citySid: '4304',
      provinceSid: '43'
    },
    {
      sid: '430408',
      name: '蒸湘区',
      citySid: '4304',
      provinceSid: '43'
    },
    {
      sid: '430412',
      name: '南岳区',
      citySid: '4304',
      provinceSid: '43'
    },
    {
      sid: '430421',
      name: '衡阳县',
      citySid: '4304',
      provinceSid: '43'
    },
    {
      sid: '430422',
      name: '衡南县',
      citySid: '4304',
      provinceSid: '43'
    },
    {
      sid: '430423',
      name: '衡山县',
      citySid: '4304',
      provinceSid: '43'
    },
    {
      sid: '430424',
      name: '衡东县',
      citySid: '4304',
      provinceSid: '43'
    },
    {
      sid: '430426',
      name: '祁东县',
      citySid: '4304',
      provinceSid: '43'
    },
    {
      sid: '430471',
      name: '衡阳综合保税区',
      citySid: '4304',
      provinceSid: '43'
    },
    {
      sid: '430472',
      name: '湖南衡阳高新技术产业园区',
      citySid: '4304',
      provinceSid: '43'
    },
    {
      sid: '430473',
      name: '湖南衡阳松木经济开发区',
      citySid: '4304',
      provinceSid: '43'
    },
    {
      sid: '430481',
      name: '耒阳市',
      citySid: '4304',
      provinceSid: '43'
    },
    {
      sid: '430482',
      name: '常宁市',
      citySid: '4304',
      provinceSid: '43'
    },
    {
      sid: '430502',
      name: '双清区',
      citySid: '4305',
      provinceSid: '43'
    },
    {
      sid: '430503',
      name: '大祥区',
      citySid: '4305',
      provinceSid: '43'
    },
    {
      sid: '430511',
      name: '北塔区',
      citySid: '4305',
      provinceSid: '43'
    },
    {
      sid: '430521',
      name: '邵东县',
      citySid: '4305',
      provinceSid: '43'
    },
    {
      sid: '430522',
      name: '新邵县',
      citySid: '4305',
      provinceSid: '43'
    },
    {
      sid: '430523',
      name: '邵阳县',
      citySid: '4305',
      provinceSid: '43'
    },
    {
      sid: '430524',
      name: '隆回县',
      citySid: '4305',
      provinceSid: '43'
    },
    {
      sid: '430525',
      name: '洞口县',
      citySid: '4305',
      provinceSid: '43'
    },
    {
      sid: '430527',
      name: '绥宁县',
      citySid: '4305',
      provinceSid: '43'
    },
    {
      sid: '430528',
      name: '新宁县',
      citySid: '4305',
      provinceSid: '43'
    },
    {
      sid: '430529',
      name: '城步苗族自治县',
      citySid: '4305',
      provinceSid: '43'
    },
    {
      sid: '430581',
      name: '武冈市',
      citySid: '4305',
      provinceSid: '43'
    },
    {
      sid: '430602',
      name: '岳阳楼区',
      citySid: '4306',
      provinceSid: '43'
    },
    {
      sid: '430603',
      name: '云溪区',
      citySid: '4306',
      provinceSid: '43'
    },
    {
      sid: '430611',
      name: '君山区',
      citySid: '4306',
      provinceSid: '43'
    },
    {
      sid: '430621',
      name: '岳阳县',
      citySid: '4306',
      provinceSid: '43'
    },
    {
      sid: '430623',
      name: '华容县',
      citySid: '4306',
      provinceSid: '43'
    },
    {
      sid: '430624',
      name: '湘阴县',
      citySid: '4306',
      provinceSid: '43'
    },
    {
      sid: '430626',
      name: '平江县',
      citySid: '4306',
      provinceSid: '43'
    },
    {
      sid: '430671',
      name: '岳阳市屈原管理区',
      citySid: '4306',
      provinceSid: '43'
    },
    {
      sid: '430681',
      name: '汨罗市',
      citySid: '4306',
      provinceSid: '43'
    },
    {
      sid: '430682',
      name: '临湘市',
      citySid: '4306',
      provinceSid: '43'
    },
    {
      sid: '430702',
      name: '武陵区',
      citySid: '4307',
      provinceSid: '43'
    },
    {
      sid: '430703',
      name: '鼎城区',
      citySid: '4307',
      provinceSid: '43'
    },
    {
      sid: '430721',
      name: '安乡县',
      citySid: '4307',
      provinceSid: '43'
    },
    {
      sid: '430722',
      name: '汉寿县',
      citySid: '4307',
      provinceSid: '43'
    },
    {
      sid: '430723',
      name: '澧县',
      citySid: '4307',
      provinceSid: '43'
    },
    {
      sid: '430724',
      name: '临澧县',
      citySid: '4307',
      provinceSid: '43'
    },
    {
      sid: '430725',
      name: '桃源县',
      citySid: '4307',
      provinceSid: '43'
    },
    {
      sid: '430726',
      name: '石门县',
      citySid: '4307',
      provinceSid: '43'
    },
    {
      sid: '430771',
      name: '常德市西洞庭管理区',
      citySid: '4307',
      provinceSid: '43'
    },
    {
      sid: '430781',
      name: '津市市',
      citySid: '4307',
      provinceSid: '43'
    },
    {
      sid: '430802',
      name: '永定区',
      citySid: '4308',
      provinceSid: '43'
    },
    {
      sid: '430811',
      name: '武陵源区',
      citySid: '4308',
      provinceSid: '43'
    },
    {
      sid: '430821',
      name: '慈利县',
      citySid: '4308',
      provinceSid: '43'
    },
    {
      sid: '430822',
      name: '桑植县',
      citySid: '4308',
      provinceSid: '43'
    },
    {
      sid: '430902',
      name: '资阳区',
      citySid: '4309',
      provinceSid: '43'
    },
    {
      sid: '430903',
      name: '赫山区',
      citySid: '4309',
      provinceSid: '43'
    },
    {
      sid: '430921',
      name: '南县',
      citySid: '4309',
      provinceSid: '43'
    },
    {
      sid: '430922',
      name: '桃江县',
      citySid: '4309',
      provinceSid: '43'
    },
    {
      sid: '430923',
      name: '安化县',
      citySid: '4309',
      provinceSid: '43'
    },
    {
      sid: '430971',
      name: '益阳市大通湖管理区',
      citySid: '4309',
      provinceSid: '43'
    },
    {
      sid: '430972',
      name: '湖南益阳高新技术产业园区',
      citySid: '4309',
      provinceSid: '43'
    },
    {
      sid: '430981',
      name: '沅江市',
      citySid: '4309',
      provinceSid: '43'
    },
    {
      sid: '431002',
      name: '北湖区',
      citySid: '4310',
      provinceSid: '43'
    },
    {
      sid: '431003',
      name: '苏仙区',
      citySid: '4310',
      provinceSid: '43'
    },
    {
      sid: '431021',
      name: '桂阳县',
      citySid: '4310',
      provinceSid: '43'
    },
    {
      sid: '431022',
      name: '宜章县',
      citySid: '4310',
      provinceSid: '43'
    },
    {
      sid: '431023',
      name: '永兴县',
      citySid: '4310',
      provinceSid: '43'
    },
    {
      sid: '431024',
      name: '嘉禾县',
      citySid: '4310',
      provinceSid: '43'
    },
    {
      sid: '431025',
      name: '临武县',
      citySid: '4310',
      provinceSid: '43'
    },
    {
      sid: '431026',
      name: '汝城县',
      citySid: '4310',
      provinceSid: '43'
    },
    {
      sid: '431027',
      name: '桂东县',
      citySid: '4310',
      provinceSid: '43'
    },
    {
      sid: '431028',
      name: '安仁县',
      citySid: '4310',
      provinceSid: '43'
    },
    {
      sid: '431081',
      name: '资兴市',
      citySid: '4310',
      provinceSid: '43'
    },
    {
      sid: '431102',
      name: '零陵区',
      citySid: '4311',
      provinceSid: '43'
    },
    {
      sid: '431103',
      name: '冷水滩区',
      citySid: '4311',
      provinceSid: '43'
    },
    {
      sid: '431121',
      name: '祁阳县',
      citySid: '4311',
      provinceSid: '43'
    },
    {
      sid: '431122',
      name: '东安县',
      citySid: '4311',
      provinceSid: '43'
    },
    {
      sid: '431123',
      name: '双牌县',
      citySid: '4311',
      provinceSid: '43'
    },
    {
      sid: '431124',
      name: '道县',
      citySid: '4311',
      provinceSid: '43'
    },
    {
      sid: '431125',
      name: '江永县',
      citySid: '4311',
      provinceSid: '43'
    },
    {
      sid: '431126',
      name: '宁远县',
      citySid: '4311',
      provinceSid: '43'
    },
    {
      sid: '431127',
      name: '蓝山县',
      citySid: '4311',
      provinceSid: '43'
    },
    {
      sid: '431128',
      name: '新田县',
      citySid: '4311',
      provinceSid: '43'
    },
    {
      sid: '431129',
      name: '江华瑶族自治县',
      citySid: '4311',
      provinceSid: '43'
    },
    {
      sid: '431171',
      name: '永州经济技术开发区',
      citySid: '4311',
      provinceSid: '43'
    },
    {
      sid: '431172',
      name: '永州市金洞管理区',
      citySid: '4311',
      provinceSid: '43'
    },
    {
      sid: '431173',
      name: '永州市回龙圩管理区',
      citySid: '4311',
      provinceSid: '43'
    },
    {
      sid: '431202',
      name: '鹤城区',
      citySid: '4312',
      provinceSid: '43'
    },
    {
      sid: '431221',
      name: '中方县',
      citySid: '4312',
      provinceSid: '43'
    },
    {
      sid: '431222',
      name: '沅陵县',
      citySid: '4312',
      provinceSid: '43'
    },
    {
      sid: '431223',
      name: '辰溪县',
      citySid: '4312',
      provinceSid: '43'
    },
    {
      sid: '431224',
      name: '溆浦县',
      citySid: '4312',
      provinceSid: '43'
    },
    {
      sid: '431225',
      name: '会同县',
      citySid: '4312',
      provinceSid: '43'
    },
    {
      sid: '431226',
      name: '麻阳苗族自治县',
      citySid: '4312',
      provinceSid: '43'
    },
    {
      sid: '431227',
      name: '新晃侗族自治县',
      citySid: '4312',
      provinceSid: '43'
    },
    {
      sid: '431228',
      name: '芷江侗族自治县',
      citySid: '4312',
      provinceSid: '43'
    },
    {
      sid: '431229',
      name: '靖州苗族侗族自治县',
      citySid: '4312',
      provinceSid: '43'
    },
    {
      sid: '431230',
      name: '通道侗族自治县',
      citySid: '4312',
      provinceSid: '43'
    },
    {
      sid: '431271',
      name: '怀化市洪江管理区',
      citySid: '4312',
      provinceSid: '43'
    },
    {
      sid: '431281',
      name: '洪江市',
      citySid: '4312',
      provinceSid: '43'
    },
    {
      sid: '431302',
      name: '娄星区',
      citySid: '4313',
      provinceSid: '43'
    },
    {
      sid: '431321',
      name: '双峰县',
      citySid: '4313',
      provinceSid: '43'
    },
    {
      sid: '431322',
      name: '新化县',
      citySid: '4313',
      provinceSid: '43'
    },
    {
      sid: '431381',
      name: '冷水江市',
      citySid: '4313',
      provinceSid: '43'
    },
    {
      sid: '431382',
      name: '涟源市',
      citySid: '4313',
      provinceSid: '43'
    },
    {
      sid: '433101',
      name: '吉首市',
      citySid: '4331',
      provinceSid: '43'
    },
    {
      sid: '433122',
      name: '泸溪县',
      citySid: '4331',
      provinceSid: '43'
    },
    {
      sid: '433123',
      name: '凤凰县',
      citySid: '4331',
      provinceSid: '43'
    },
    {
      sid: '433124',
      name: '花垣县',
      citySid: '4331',
      provinceSid: '43'
    },
    {
      sid: '433125',
      name: '保靖县',
      citySid: '4331',
      provinceSid: '43'
    },
    {
      sid: '433126',
      name: '古丈县',
      citySid: '4331',
      provinceSid: '43'
    },
    {
      sid: '433127',
      name: '永顺县',
      citySid: '4331',
      provinceSid: '43'
    },
    {
      sid: '433130',
      name: '龙山县',
      citySid: '4331',
      provinceSid: '43'
    },
    {
      sid: '433172',
      name: '湖南吉首经济开发区',
      citySid: '4331',
      provinceSid: '43'
    },
    {
      sid: '433173',
      name: '湖南永顺经济开发区',
      citySid: '4331',
      provinceSid: '43'
    },
    {
      sid: '440103',
      name: '荔湾区',
      citySid: '4401',
      provinceSid: '44'
    },
    {
      sid: '440104',
      name: '越秀区',
      citySid: '4401',
      provinceSid: '44'
    },
    {
      sid: '440105',
      name: '海珠区',
      citySid: '4401',
      provinceSid: '44'
    },
    {
      sid: '440106',
      name: '天河区',
      citySid: '4401',
      provinceSid: '44'
    },
    {
      sid: '440111',
      name: '白云区',
      citySid: '4401',
      provinceSid: '44'
    },
    {
      sid: '440112',
      name: '黄埔区',
      citySid: '4401',
      provinceSid: '44'
    },
    {
      sid: '440113',
      name: '番禺区',
      citySid: '4401',
      provinceSid: '44'
    },
    {
      sid: '440114',
      name: '花都区',
      citySid: '4401',
      provinceSid: '44'
    },
    {
      sid: '440115',
      name: '南沙区',
      citySid: '4401',
      provinceSid: '44'
    },
    {
      sid: '440117',
      name: '从化区',
      citySid: '4401',
      provinceSid: '44'
    },
    {
      sid: '440118',
      name: '增城区',
      citySid: '4401',
      provinceSid: '44'
    },
    {
      sid: '440203',
      name: '武江区',
      citySid: '4402',
      provinceSid: '44'
    },
    {
      sid: '440204',
      name: '浈江区',
      citySid: '4402',
      provinceSid: '44'
    },
    {
      sid: '440205',
      name: '曲江区',
      citySid: '4402',
      provinceSid: '44'
    },
    {
      sid: '440222',
      name: '始兴县',
      citySid: '4402',
      provinceSid: '44'
    },
    {
      sid: '440224',
      name: '仁化县',
      citySid: '4402',
      provinceSid: '44'
    },
    {
      sid: '440229',
      name: '翁源县',
      citySid: '4402',
      provinceSid: '44'
    },
    {
      sid: '440232',
      name: '乳源瑶族自治县',
      citySid: '4402',
      provinceSid: '44'
    },
    {
      sid: '440233',
      name: '新丰县',
      citySid: '4402',
      provinceSid: '44'
    },
    {
      sid: '440281',
      name: '乐昌市',
      citySid: '4402',
      provinceSid: '44'
    },
    {
      sid: '440282',
      name: '南雄市',
      citySid: '4402',
      provinceSid: '44'
    },
    {
      sid: '440303',
      name: '罗湖区',
      citySid: '4403',
      provinceSid: '44'
    },
    {
      sid: '440304',
      name: '福田区',
      citySid: '4403',
      provinceSid: '44'
    },
    {
      sid: '440305',
      name: '南山区',
      citySid: '4403',
      provinceSid: '44'
    },
    {
      sid: '440306',
      name: '宝安区',
      citySid: '4403',
      provinceSid: '44'
    },
    {
      sid: '440307',
      name: '龙岗区',
      citySid: '4403',
      provinceSid: '44'
    },
    {
      sid: '440308',
      name: '盐田区',
      citySid: '4403',
      provinceSid: '44'
    },
    {
      sid: '440309',
      name: '龙华区',
      citySid: '4403',
      provinceSid: '44'
    },
    {
      sid: '440310',
      name: '坪山区',
      citySid: '4403',
      provinceSid: '44'
    },
    {
      sid: '440402',
      name: '香洲区',
      citySid: '4404',
      provinceSid: '44'
    },
    {
      sid: '440403',
      name: '斗门区',
      citySid: '4404',
      provinceSid: '44'
    },
    {
      sid: '440404',
      name: '金湾区',
      citySid: '4404',
      provinceSid: '44'
    },
    {
      sid: '440507',
      name: '龙湖区',
      citySid: '4405',
      provinceSid: '44'
    },
    {
      sid: '440511',
      name: '金平区',
      citySid: '4405',
      provinceSid: '44'
    },
    {
      sid: '440512',
      name: '濠江区',
      citySid: '4405',
      provinceSid: '44'
    },
    {
      sid: '440513',
      name: '潮阳区',
      citySid: '4405',
      provinceSid: '44'
    },
    {
      sid: '440514',
      name: '潮南区',
      citySid: '4405',
      provinceSid: '44'
    },
    {
      sid: '440515',
      name: '澄海区',
      citySid: '4405',
      provinceSid: '44'
    },
    {
      sid: '440523',
      name: '南澳县',
      citySid: '4405',
      provinceSid: '44'
    },
    {
      sid: '440604',
      name: '禅城区',
      citySid: '4406',
      provinceSid: '44'
    },
    {
      sid: '440605',
      name: '南海区',
      citySid: '4406',
      provinceSid: '44'
    },
    {
      sid: '440606',
      name: '顺德区',
      citySid: '4406',
      provinceSid: '44'
    },
    {
      sid: '440607',
      name: '三水区',
      citySid: '4406',
      provinceSid: '44'
    },
    {
      sid: '440608',
      name: '高明区',
      citySid: '4406',
      provinceSid: '44'
    },
    {
      sid: '440703',
      name: '蓬江区',
      citySid: '4407',
      provinceSid: '44'
    },
    {
      sid: '440704',
      name: '江海区',
      citySid: '4407',
      provinceSid: '44'
    },
    {
      sid: '440705',
      name: '新会区',
      citySid: '4407',
      provinceSid: '44'
    },
    {
      sid: '440781',
      name: '台山市',
      citySid: '4407',
      provinceSid: '44'
    },
    {
      sid: '440783',
      name: '开平市',
      citySid: '4407',
      provinceSid: '44'
    },
    {
      sid: '440784',
      name: '鹤山市',
      citySid: '4407',
      provinceSid: '44'
    },
    {
      sid: '440785',
      name: '恩平市',
      citySid: '4407',
      provinceSid: '44'
    },
    {
      sid: '440802',
      name: '赤坎区',
      citySid: '4408',
      provinceSid: '44'
    },
    {
      sid: '440803',
      name: '霞山区',
      citySid: '4408',
      provinceSid: '44'
    },
    {
      sid: '440804',
      name: '坡头区',
      citySid: '4408',
      provinceSid: '44'
    },
    {
      sid: '440811',
      name: '麻章区',
      citySid: '4408',
      provinceSid: '44'
    },
    {
      sid: '440823',
      name: '遂溪县',
      citySid: '4408',
      provinceSid: '44'
    },
    {
      sid: '440825',
      name: '徐闻县',
      citySid: '4408',
      provinceSid: '44'
    },
    {
      sid: '440881',
      name: '廉江市',
      citySid: '4408',
      provinceSid: '44'
    },
    {
      sid: '440882',
      name: '雷州市',
      citySid: '4408',
      provinceSid: '44'
    },
    {
      sid: '440883',
      name: '吴川市',
      citySid: '4408',
      provinceSid: '44'
    },
    {
      sid: '440902',
      name: '茂南区',
      citySid: '4409',
      provinceSid: '44'
    },
    {
      sid: '440904',
      name: '电白区',
      citySid: '4409',
      provinceSid: '44'
    },
    {
      sid: '440981',
      name: '高州市',
      citySid: '4409',
      provinceSid: '44'
    },
    {
      sid: '440982',
      name: '化州市',
      citySid: '4409',
      provinceSid: '44'
    },
    {
      sid: '440983',
      name: '信宜市',
      citySid: '4409',
      provinceSid: '44'
    },
    {
      sid: '441202',
      name: '端州区',
      citySid: '4412',
      provinceSid: '44'
    },
    {
      sid: '441203',
      name: '鼎湖区',
      citySid: '4412',
      provinceSid: '44'
    },
    {
      sid: '441204',
      name: '高要区',
      citySid: '4412',
      provinceSid: '44'
    },
    {
      sid: '441223',
      name: '广宁县',
      citySid: '4412',
      provinceSid: '44'
    },
    {
      sid: '441224',
      name: '怀集县',
      citySid: '4412',
      provinceSid: '44'
    },
    {
      sid: '441225',
      name: '封开县',
      citySid: '4412',
      provinceSid: '44'
    },
    {
      sid: '441226',
      name: '德庆县',
      citySid: '4412',
      provinceSid: '44'
    },
    {
      sid: '441284',
      name: '四会市',
      citySid: '4412',
      provinceSid: '44'
    },
    {
      sid: '441302',
      name: '惠城区',
      citySid: '4413',
      provinceSid: '44'
    },
    {
      sid: '441303',
      name: '惠阳区',
      citySid: '4413',
      provinceSid: '44'
    },
    {
      sid: '441322',
      name: '博罗县',
      citySid: '4413',
      provinceSid: '44'
    },
    {
      sid: '441323',
      name: '惠东县',
      citySid: '4413',
      provinceSid: '44'
    },
    {
      sid: '441324',
      name: '龙门县',
      citySid: '4413',
      provinceSid: '44'
    },
    {
      sid: '441402',
      name: '梅江区',
      citySid: '4414',
      provinceSid: '44'
    },
    {
      sid: '441403',
      name: '梅县区',
      citySid: '4414',
      provinceSid: '44'
    },
    {
      sid: '441422',
      name: '大埔县',
      citySid: '4414',
      provinceSid: '44'
    },
    {
      sid: '441423',
      name: '丰顺县',
      citySid: '4414',
      provinceSid: '44'
    },
    {
      sid: '441424',
      name: '五华县',
      citySid: '4414',
      provinceSid: '44'
    },
    {
      sid: '441426',
      name: '平远县',
      citySid: '4414',
      provinceSid: '44'
    },
    {
      sid: '441427',
      name: '蕉岭县',
      citySid: '4414',
      provinceSid: '44'
    },
    {
      sid: '441481',
      name: '兴宁市',
      citySid: '4414',
      provinceSid: '44'
    },
    {
      sid: '441502',
      name: '城区',
      citySid: '4415',
      provinceSid: '44'
    },
    {
      sid: '441521',
      name: '海丰县',
      citySid: '4415',
      provinceSid: '44'
    },
    {
      sid: '441523',
      name: '陆河县',
      citySid: '4415',
      provinceSid: '44'
    },
    {
      sid: '441581',
      name: '陆丰市',
      citySid: '4415',
      provinceSid: '44'
    },
    {
      sid: '441602',
      name: '源城区',
      citySid: '4416',
      provinceSid: '44'
    },
    {
      sid: '441621',
      name: '紫金县',
      citySid: '4416',
      provinceSid: '44'
    },
    {
      sid: '441622',
      name: '龙川县',
      citySid: '4416',
      provinceSid: '44'
    },
    {
      sid: '441623',
      name: '连平县',
      citySid: '4416',
      provinceSid: '44'
    },
    {
      sid: '441624',
      name: '和平县',
      citySid: '4416',
      provinceSid: '44'
    },
    {
      sid: '441625',
      name: '东源县',
      citySid: '4416',
      provinceSid: '44'
    },
    {
      sid: '441702',
      name: '江城区',
      citySid: '4417',
      provinceSid: '44'
    },
    {
      sid: '441704',
      name: '阳东区',
      citySid: '4417',
      provinceSid: '44'
    },
    {
      sid: '441721',
      name: '阳西县',
      citySid: '4417',
      provinceSid: '44'
    },
    {
      sid: '441781',
      name: '阳春市',
      citySid: '4417',
      provinceSid: '44'
    },
    {
      sid: '441802',
      name: '清城区',
      citySid: '4418',
      provinceSid: '44'
    },
    {
      sid: '441803',
      name: '清新区',
      citySid: '4418',
      provinceSid: '44'
    },
    {
      sid: '441821',
      name: '佛冈县',
      citySid: '4418',
      provinceSid: '44'
    },
    {
      sid: '441823',
      name: '阳山县',
      citySid: '4418',
      provinceSid: '44'
    },
    {
      sid: '441825',
      name: '连山壮族瑶族自治县',
      citySid: '4418',
      provinceSid: '44'
    },
    {
      sid: '441826',
      name: '连南瑶族自治县',
      citySid: '4418',
      provinceSid: '44'
    },
    {
      sid: '441881',
      name: '英德市',
      citySid: '4418',
      provinceSid: '44'
    },
    {
      sid: '441882',
      name: '连州市',
      citySid: '4418',
      provinceSid: '44'
    },
    {
      sid: '441900',
      name: '东莞市',
      citySid: '4419',
      provinceSid: '44'
    },
    {
      sid: '442000',
      name: '中山市',
      citySid: '4420',
      provinceSid: '44'
    },
    {
      sid: '445102',
      name: '湘桥区',
      citySid: '4451',
      provinceSid: '44'
    },
    {
      sid: '445103',
      name: '潮安区',
      citySid: '4451',
      provinceSid: '44'
    },
    {
      sid: '445122',
      name: '饶平县',
      citySid: '4451',
      provinceSid: '44'
    },
    {
      sid: '445202',
      name: '榕城区',
      citySid: '4452',
      provinceSid: '44'
    },
    {
      sid: '445203',
      name: '揭东区',
      citySid: '4452',
      provinceSid: '44'
    },
    {
      sid: '445222',
      name: '揭西县',
      citySid: '4452',
      provinceSid: '44'
    },
    {
      sid: '445224',
      name: '惠来县',
      citySid: '4452',
      provinceSid: '44'
    },
    {
      sid: '445281',
      name: '普宁市',
      citySid: '4452',
      provinceSid: '44'
    },
    {
      sid: '445302',
      name: '云城区',
      citySid: '4453',
      provinceSid: '44'
    },
    {
      sid: '445303',
      name: '云安区',
      citySid: '4453',
      provinceSid: '44'
    },
    {
      sid: '445321',
      name: '新兴县',
      citySid: '4453',
      provinceSid: '44'
    },
    {
      sid: '445322',
      name: '郁南县',
      citySid: '4453',
      provinceSid: '44'
    },
    {
      sid: '445381',
      name: '罗定市',
      citySid: '4453',
      provinceSid: '44'
    },
    {
      sid: '450102',
      name: '兴宁区',
      citySid: '4501',
      provinceSid: '45'
    },
    {
      sid: '450103',
      name: '青秀区',
      citySid: '4501',
      provinceSid: '45'
    },
    {
      sid: '450105',
      name: '江南区',
      citySid: '4501',
      provinceSid: '45'
    },
    {
      sid: '450107',
      name: '西乡塘区',
      citySid: '4501',
      provinceSid: '45'
    },
    {
      sid: '450108',
      name: '良庆区',
      citySid: '4501',
      provinceSid: '45'
    },
    {
      sid: '450109',
      name: '邕宁区',
      citySid: '4501',
      provinceSid: '45'
    },
    {
      sid: '450110',
      name: '武鸣区',
      citySid: '4501',
      provinceSid: '45'
    },
    {
      sid: '450123',
      name: '隆安县',
      citySid: '4501',
      provinceSid: '45'
    },
    {
      sid: '450124',
      name: '马山县',
      citySid: '4501',
      provinceSid: '45'
    },
    {
      sid: '450125',
      name: '上林县',
      citySid: '4501',
      provinceSid: '45'
    },
    {
      sid: '450126',
      name: '宾阳县',
      citySid: '4501',
      provinceSid: '45'
    },
    {
      sid: '450127',
      name: '横县',
      citySid: '4501',
      provinceSid: '45'
    },
    {
      sid: '450202',
      name: '城中区',
      citySid: '4502',
      provinceSid: '45'
    },
    {
      sid: '450203',
      name: '鱼峰区',
      citySid: '4502',
      provinceSid: '45'
    },
    {
      sid: '450204',
      name: '柳南区',
      citySid: '4502',
      provinceSid: '45'
    },
    {
      sid: '450205',
      name: '柳北区',
      citySid: '4502',
      provinceSid: '45'
    },
    {
      sid: '450206',
      name: '柳江区',
      citySid: '4502',
      provinceSid: '45'
    },
    {
      sid: '450222',
      name: '柳城县',
      citySid: '4502',
      provinceSid: '45'
    },
    {
      sid: '450223',
      name: '鹿寨县',
      citySid: '4502',
      provinceSid: '45'
    },
    {
      sid: '450224',
      name: '融安县',
      citySid: '4502',
      provinceSid: '45'
    },
    {
      sid: '450225',
      name: '融水苗族自治县',
      citySid: '4502',
      provinceSid: '45'
    },
    {
      sid: '450226',
      name: '三江侗族自治县',
      citySid: '4502',
      provinceSid: '45'
    },
    {
      sid: '450302',
      name: '秀峰区',
      citySid: '4503',
      provinceSid: '45'
    },
    {
      sid: '450303',
      name: '叠彩区',
      citySid: '4503',
      provinceSid: '45'
    },
    {
      sid: '450304',
      name: '象山区',
      citySid: '4503',
      provinceSid: '45'
    },
    {
      sid: '450305',
      name: '七星区',
      citySid: '4503',
      provinceSid: '45'
    },
    {
      sid: '450311',
      name: '雁山区',
      citySid: '4503',
      provinceSid: '45'
    },
    {
      sid: '450312',
      name: '临桂区',
      citySid: '4503',
      provinceSid: '45'
    },
    {
      sid: '450321',
      name: '阳朔县',
      citySid: '4503',
      provinceSid: '45'
    },
    {
      sid: '450323',
      name: '灵川县',
      citySid: '4503',
      provinceSid: '45'
    },
    {
      sid: '450324',
      name: '全州县',
      citySid: '4503',
      provinceSid: '45'
    },
    {
      sid: '450325',
      name: '兴安县',
      citySid: '4503',
      provinceSid: '45'
    },
    {
      sid: '450326',
      name: '永福县',
      citySid: '4503',
      provinceSid: '45'
    },
    {
      sid: '450327',
      name: '灌阳县',
      citySid: '4503',
      provinceSid: '45'
    },
    {
      sid: '450328',
      name: '龙胜各族自治县',
      citySid: '4503',
      provinceSid: '45'
    },
    {
      sid: '450329',
      name: '资源县',
      citySid: '4503',
      provinceSid: '45'
    },
    {
      sid: '450330',
      name: '平乐县',
      citySid: '4503',
      provinceSid: '45'
    },
    {
      sid: '450331',
      name: '荔浦县',
      citySid: '4503',
      provinceSid: '45'
    },
    {
      sid: '450332',
      name: '恭城瑶族自治县',
      citySid: '4503',
      provinceSid: '45'
    },
    {
      sid: '450403',
      name: '万秀区',
      citySid: '4504',
      provinceSid: '45'
    },
    {
      sid: '450405',
      name: '长洲区',
      citySid: '4504',
      provinceSid: '45'
    },
    {
      sid: '450406',
      name: '龙圩区',
      citySid: '4504',
      provinceSid: '45'
    },
    {
      sid: '450421',
      name: '苍梧县',
      citySid: '4504',
      provinceSid: '45'
    },
    {
      sid: '450422',
      name: '藤县',
      citySid: '4504',
      provinceSid: '45'
    },
    {
      sid: '450423',
      name: '蒙山县',
      citySid: '4504',
      provinceSid: '45'
    },
    {
      sid: '450481',
      name: '岑溪市',
      citySid: '4504',
      provinceSid: '45'
    },
    {
      sid: '450502',
      name: '海城区',
      citySid: '4505',
      provinceSid: '45'
    },
    {
      sid: '450503',
      name: '银海区',
      citySid: '4505',
      provinceSid: '45'
    },
    {
      sid: '450512',
      name: '铁山港区',
      citySid: '4505',
      provinceSid: '45'
    },
    {
      sid: '450521',
      name: '合浦县',
      citySid: '4505',
      provinceSid: '45'
    },
    {
      sid: '450602',
      name: '港口区',
      citySid: '4506',
      provinceSid: '45'
    },
    {
      sid: '450603',
      name: '防城区',
      citySid: '4506',
      provinceSid: '45'
    },
    {
      sid: '450621',
      name: '上思县',
      citySid: '4506',
      provinceSid: '45'
    },
    {
      sid: '450681',
      name: '东兴市',
      citySid: '4506',
      provinceSid: '45'
    },
    {
      sid: '450702',
      name: '钦南区',
      citySid: '4507',
      provinceSid: '45'
    },
    {
      sid: '450703',
      name: '钦北区',
      citySid: '4507',
      provinceSid: '45'
    },
    {
      sid: '450721',
      name: '灵山县',
      citySid: '4507',
      provinceSid: '45'
    },
    {
      sid: '450722',
      name: '浦北县',
      citySid: '4507',
      provinceSid: '45'
    },
    {
      sid: '450802',
      name: '港北区',
      citySid: '4508',
      provinceSid: '45'
    },
    {
      sid: '450803',
      name: '港南区',
      citySid: '4508',
      provinceSid: '45'
    },
    {
      sid: '450804',
      name: '覃塘区',
      citySid: '4508',
      provinceSid: '45'
    },
    {
      sid: '450821',
      name: '平南县',
      citySid: '4508',
      provinceSid: '45'
    },
    {
      sid: '450881',
      name: '桂平市',
      citySid: '4508',
      provinceSid: '45'
    },
    {
      sid: '450902',
      name: '玉州区',
      citySid: '4509',
      provinceSid: '45'
    },
    {
      sid: '450903',
      name: '福绵区',
      citySid: '4509',
      provinceSid: '45'
    },
    {
      sid: '450921',
      name: '容县',
      citySid: '4509',
      provinceSid: '45'
    },
    {
      sid: '450922',
      name: '陆川县',
      citySid: '4509',
      provinceSid: '45'
    },
    {
      sid: '450923',
      name: '博白县',
      citySid: '4509',
      provinceSid: '45'
    },
    {
      sid: '450924',
      name: '兴业县',
      citySid: '4509',
      provinceSid: '45'
    },
    {
      sid: '450981',
      name: '北流市',
      citySid: '4509',
      provinceSid: '45'
    },
    {
      sid: '451002',
      name: '右江区',
      citySid: '4510',
      provinceSid: '45'
    },
    {
      sid: '451021',
      name: '田阳县',
      citySid: '4510',
      provinceSid: '45'
    },
    {
      sid: '451022',
      name: '田东县',
      citySid: '4510',
      provinceSid: '45'
    },
    {
      sid: '451023',
      name: '平果县',
      citySid: '4510',
      provinceSid: '45'
    },
    {
      sid: '451024',
      name: '德保县',
      citySid: '4510',
      provinceSid: '45'
    },
    {
      sid: '451026',
      name: '那坡县',
      citySid: '4510',
      provinceSid: '45'
    },
    {
      sid: '451027',
      name: '凌云县',
      citySid: '4510',
      provinceSid: '45'
    },
    {
      sid: '451028',
      name: '乐业县',
      citySid: '4510',
      provinceSid: '45'
    },
    {
      sid: '451029',
      name: '田林县',
      citySid: '4510',
      provinceSid: '45'
    },
    {
      sid: '451030',
      name: '西林县',
      citySid: '4510',
      provinceSid: '45'
    },
    {
      sid: '451031',
      name: '隆林各族自治县',
      citySid: '4510',
      provinceSid: '45'
    },
    {
      sid: '451081',
      name: '靖西市',
      citySid: '4510',
      provinceSid: '45'
    },
    {
      sid: '451102',
      name: '八步区',
      citySid: '4511',
      provinceSid: '45'
    },
    {
      sid: '451103',
      name: '平桂区',
      citySid: '4511',
      provinceSid: '45'
    },
    {
      sid: '451121',
      name: '昭平县',
      citySid: '4511',
      provinceSid: '45'
    },
    {
      sid: '451122',
      name: '钟山县',
      citySid: '4511',
      provinceSid: '45'
    },
    {
      sid: '451123',
      name: '富川瑶族自治县',
      citySid: '4511',
      provinceSid: '45'
    },
    {
      sid: '451202',
      name: '金城江区',
      citySid: '4512',
      provinceSid: '45'
    },
    {
      sid: '451203',
      name: '宜州区',
      citySid: '4512',
      provinceSid: '45'
    },
    {
      sid: '451221',
      name: '南丹县',
      citySid: '4512',
      provinceSid: '45'
    },
    {
      sid: '451222',
      name: '天峨县',
      citySid: '4512',
      provinceSid: '45'
    },
    {
      sid: '451223',
      name: '凤山县',
      citySid: '4512',
      provinceSid: '45'
    },
    {
      sid: '451224',
      name: '东兰县',
      citySid: '4512',
      provinceSid: '45'
    },
    {
      sid: '451225',
      name: '罗城仫佬族自治县',
      citySid: '4512',
      provinceSid: '45'
    },
    {
      sid: '451226',
      name: '环江毛南族自治县',
      citySid: '4512',
      provinceSid: '45'
    },
    {
      sid: '451227',
      name: '巴马瑶族自治县',
      citySid: '4512',
      provinceSid: '45'
    },
    {
      sid: '451228',
      name: '都安瑶族自治县',
      citySid: '4512',
      provinceSid: '45'
    },
    {
      sid: '451229',
      name: '大化瑶族自治县',
      citySid: '4512',
      provinceSid: '45'
    },
    {
      sid: '451302',
      name: '兴宾区',
      citySid: '4513',
      provinceSid: '45'
    },
    {
      sid: '451321',
      name: '忻城县',
      citySid: '4513',
      provinceSid: '45'
    },
    {
      sid: '451322',
      name: '象州县',
      citySid: '4513',
      provinceSid: '45'
    },
    {
      sid: '451323',
      name: '武宣县',
      citySid: '4513',
      provinceSid: '45'
    },
    {
      sid: '451324',
      name: '金秀瑶族自治县',
      citySid: '4513',
      provinceSid: '45'
    },
    {
      sid: '451381',
      name: '合山市',
      citySid: '4513',
      provinceSid: '45'
    },
    {
      sid: '451402',
      name: '江州区',
      citySid: '4514',
      provinceSid: '45'
    },
    {
      sid: '451421',
      name: '扶绥县',
      citySid: '4514',
      provinceSid: '45'
    },
    {
      sid: '451422',
      name: '宁明县',
      citySid: '4514',
      provinceSid: '45'
    },
    {
      sid: '451423',
      name: '龙州县',
      citySid: '4514',
      provinceSid: '45'
    },
    {
      sid: '451424',
      name: '大新县',
      citySid: '4514',
      provinceSid: '45'
    },
    {
      sid: '451425',
      name: '天等县',
      citySid: '4514',
      provinceSid: '45'
    },
    {
      sid: '451481',
      name: '凭祥市',
      citySid: '4514',
      provinceSid: '45'
    },
    {
      sid: '460105',
      name: '秀英区',
      citySid: '4601',
      provinceSid: '46'
    },
    {
      sid: '460106',
      name: '龙华区',
      citySid: '4601',
      provinceSid: '46'
    },
    {
      sid: '460107',
      name: '琼山区',
      citySid: '4601',
      provinceSid: '46'
    },
    {
      sid: '460108',
      name: '美兰区',
      citySid: '4601',
      provinceSid: '46'
    },
    {
      sid: '460202',
      name: '海棠区',
      citySid: '4602',
      provinceSid: '46'
    },
    {
      sid: '460203',
      name: '吉阳区',
      citySid: '4602',
      provinceSid: '46'
    },
    {
      sid: '460204',
      name: '天涯区',
      citySid: '4602',
      provinceSid: '46'
    },
    {
      sid: '460205',
      name: '崖州区',
      citySid: '4602',
      provinceSid: '46'
    },
    {
      sid: '460321',
      name: '西沙群岛',
      citySid: '4603',
      provinceSid: '46'
    },
    {
      sid: '460322',
      name: '南沙群岛',
      citySid: '4603',
      provinceSid: '46'
    },
    {
      sid: '460323',
      name: '中沙群岛的岛礁及其海域',
      citySid: '4603',
      provinceSid: '46'
    },
    {
      sid: '460400',
      name: '儋州市',
      citySid: '4604',
      provinceSid: '46'
    },
    {
      sid: '469001',
      name: '五指山市',
      citySid: '4690',
      provinceSid: '46'
    },
    {
      sid: '469002',
      name: '琼海市',
      citySid: '4690',
      provinceSid: '46'
    },
    {
      sid: '469005',
      name: '文昌市',
      citySid: '4690',
      provinceSid: '46'
    },
    {
      sid: '469006',
      name: '万宁市',
      citySid: '4690',
      provinceSid: '46'
    },
    {
      sid: '469007',
      name: '东方市',
      citySid: '4690',
      provinceSid: '46'
    },
    {
      sid: '469021',
      name: '定安县',
      citySid: '4690',
      provinceSid: '46'
    },
    {
      sid: '469022',
      name: '屯昌县',
      citySid: '4690',
      provinceSid: '46'
    },
    {
      sid: '469023',
      name: '澄迈县',
      citySid: '4690',
      provinceSid: '46'
    },
    {
      sid: '469024',
      name: '临高县',
      citySid: '4690',
      provinceSid: '46'
    },
    {
      sid: '469025',
      name: '白沙黎族自治县',
      citySid: '4690',
      provinceSid: '46'
    },
    {
      sid: '469026',
      name: '昌江黎族自治县',
      citySid: '4690',
      provinceSid: '46'
    },
    {
      sid: '469027',
      name: '乐东黎族自治县',
      citySid: '4690',
      provinceSid: '46'
    },
    {
      sid: '469028',
      name: '陵水黎族自治县',
      citySid: '4690',
      provinceSid: '46'
    },
    {
      sid: '469029',
      name: '保亭黎族苗族自治县',
      citySid: '4690',
      provinceSid: '46'
    },
    {
      sid: '469030',
      name: '琼中黎族苗族自治县',
      citySid: '4690',
      provinceSid: '46'
    },
    {
      sid: '500101',
      name: '万州区',
      citySid: '5001',
      provinceSid: '50'
    },
    {
      sid: '500102',
      name: '涪陵区',
      citySid: '5001',
      provinceSid: '50'
    },
    {
      sid: '500103',
      name: '渝中区',
      citySid: '5001',
      provinceSid: '50'
    },
    {
      sid: '500104',
      name: '大渡口区',
      citySid: '5001',
      provinceSid: '50'
    },
    {
      sid: '500105',
      name: '江北区',
      citySid: '5001',
      provinceSid: '50'
    },
    {
      sid: '500106',
      name: '沙坪坝区',
      citySid: '5001',
      provinceSid: '50'
    },
    {
      sid: '500107',
      name: '九龙坡区',
      citySid: '5001',
      provinceSid: '50'
    },
    {
      sid: '500108',
      name: '南岸区',
      citySid: '5001',
      provinceSid: '50'
    },
    {
      sid: '500109',
      name: '北碚区',
      citySid: '5001',
      provinceSid: '50'
    },
    {
      sid: '500110',
      name: '綦江区',
      citySid: '5001',
      provinceSid: '50'
    },
    {
      sid: '500111',
      name: '大足区',
      citySid: '5001',
      provinceSid: '50'
    },
    {
      sid: '500112',
      name: '渝北区',
      citySid: '5001',
      provinceSid: '50'
    },
    {
      sid: '500113',
      name: '巴南区',
      citySid: '5001',
      provinceSid: '50'
    },
    {
      sid: '500114',
      name: '黔江区',
      citySid: '5001',
      provinceSid: '50'
    },
    {
      sid: '500115',
      name: '长寿区',
      citySid: '5001',
      provinceSid: '50'
    },
    {
      sid: '500116',
      name: '江津区',
      citySid: '5001',
      provinceSid: '50'
    },
    {
      sid: '500117',
      name: '合川区',
      citySid: '5001',
      provinceSid: '50'
    },
    {
      sid: '500118',
      name: '永川区',
      citySid: '5001',
      provinceSid: '50'
    },
    {
      sid: '500119',
      name: '南川区',
      citySid: '5001',
      provinceSid: '50'
    },
    {
      sid: '500120',
      name: '璧山区',
      citySid: '5001',
      provinceSid: '50'
    },
    {
      sid: '500151',
      name: '铜梁区',
      citySid: '5001',
      provinceSid: '50'
    },
    {
      sid: '500152',
      name: '潼南区',
      citySid: '5001',
      provinceSid: '50'
    },
    {
      sid: '500153',
      name: '荣昌区',
      citySid: '5001',
      provinceSid: '50'
    },
    {
      sid: '500154',
      name: '开州区',
      citySid: '5001',
      provinceSid: '50'
    },
    {
      sid: '500155',
      name: '梁平区',
      citySid: '5001',
      provinceSid: '50'
    },
    {
      sid: '500156',
      name: '武隆区',
      citySid: '5001',
      provinceSid: '50'
    },
    {
      sid: '500229',
      name: '城口县',
      citySid: '5002',
      provinceSid: '50'
    },
    {
      sid: '500230',
      name: '丰都县',
      citySid: '5002',
      provinceSid: '50'
    },
    {
      sid: '500231',
      name: '垫江县',
      citySid: '5002',
      provinceSid: '50'
    },
    {
      sid: '500233',
      name: '忠县',
      citySid: '5002',
      provinceSid: '50'
    },
    {
      sid: '500235',
      name: '云阳县',
      citySid: '5002',
      provinceSid: '50'
    },
    {
      sid: '500236',
      name: '奉节县',
      citySid: '5002',
      provinceSid: '50'
    },
    {
      sid: '500237',
      name: '巫山县',
      citySid: '5002',
      provinceSid: '50'
    },
    {
      sid: '500238',
      name: '巫溪县',
      citySid: '5002',
      provinceSid: '50'
    },
    {
      sid: '500240',
      name: '石柱土家族自治县',
      citySid: '5002',
      provinceSid: '50'
    },
    {
      sid: '500241',
      name: '秀山土家族苗族自治县',
      citySid: '5002',
      provinceSid: '50'
    },
    {
      sid: '500242',
      name: '酉阳土家族苗族自治县',
      citySid: '5002',
      provinceSid: '50'
    },
    {
      sid: '500243',
      name: '彭水苗族土家族自治县',
      citySid: '5002',
      provinceSid: '50'
    },
    {
      sid: '510104',
      name: '锦江区',
      citySid: '5101',
      provinceSid: '51'
    },
    {
      sid: '510105',
      name: '青羊区',
      citySid: '5101',
      provinceSid: '51'
    },
    {
      sid: '510106',
      name: '金牛区',
      citySid: '5101',
      provinceSid: '51'
    },
    {
      sid: '510107',
      name: '武侯区',
      citySid: '5101',
      provinceSid: '51'
    },
    {
      sid: '510108',
      name: '成华区',
      citySid: '5101',
      provinceSid: '51'
    },
    {
      sid: '510112',
      name: '龙泉驿区',
      citySid: '5101',
      provinceSid: '51'
    },
    {
      sid: '510113',
      name: '青白江区',
      citySid: '5101',
      provinceSid: '51'
    },
    {
      sid: '510114',
      name: '新都区',
      citySid: '5101',
      provinceSid: '51'
    },
    {
      sid: '510115',
      name: '温江区',
      citySid: '5101',
      provinceSid: '51'
    },
    {
      sid: '510116',
      name: '双流区',
      citySid: '5101',
      provinceSid: '51'
    },
    {
      sid: '510117',
      name: '郫都区',
      citySid: '5101',
      provinceSid: '51'
    },
    {
      sid: '510121',
      name: '金堂县',
      citySid: '5101',
      provinceSid: '51'
    },
    {
      sid: '510129',
      name: '大邑县',
      citySid: '5101',
      provinceSid: '51'
    },
    {
      sid: '510131',
      name: '蒲江县',
      citySid: '5101',
      provinceSid: '51'
    },
    {
      sid: '510132',
      name: '新津县',
      citySid: '5101',
      provinceSid: '51'
    },
    {
      sid: '510181',
      name: '都江堰市',
      citySid: '5101',
      provinceSid: '51'
    },
    {
      sid: '510182',
      name: '彭州市',
      citySid: '5101',
      provinceSid: '51'
    },
    {
      sid: '510183',
      name: '邛崃市',
      citySid: '5101',
      provinceSid: '51'
    },
    {
      sid: '510184',
      name: '崇州市',
      citySid: '5101',
      provinceSid: '51'
    },
    {
      sid: '510185',
      name: '简阳市',
      citySid: '5101',
      provinceSid: '51'
    },
    {
      sid: '510302',
      name: '自流井区',
      citySid: '5103',
      provinceSid: '51'
    },
    {
      sid: '510303',
      name: '贡井区',
      citySid: '5103',
      provinceSid: '51'
    },
    {
      sid: '510304',
      name: '大安区',
      citySid: '5103',
      provinceSid: '51'
    },
    {
      sid: '510311',
      name: '沿滩区',
      citySid: '5103',
      provinceSid: '51'
    },
    {
      sid: '510321',
      name: '荣县',
      citySid: '5103',
      provinceSid: '51'
    },
    {
      sid: '510322',
      name: '富顺县',
      citySid: '5103',
      provinceSid: '51'
    },
    {
      sid: '510402',
      name: '东区',
      citySid: '5104',
      provinceSid: '51'
    },
    {
      sid: '510403',
      name: '西区',
      citySid: '5104',
      provinceSid: '51'
    },
    {
      sid: '510411',
      name: '仁和区',
      citySid: '5104',
      provinceSid: '51'
    },
    {
      sid: '510421',
      name: '米易县',
      citySid: '5104',
      provinceSid: '51'
    },
    {
      sid: '510422',
      name: '盐边县',
      citySid: '5104',
      provinceSid: '51'
    },
    {
      sid: '510502',
      name: '江阳区',
      citySid: '5105',
      provinceSid: '51'
    },
    {
      sid: '510503',
      name: '纳溪区',
      citySid: '5105',
      provinceSid: '51'
    },
    {
      sid: '510504',
      name: '龙马潭区',
      citySid: '5105',
      provinceSid: '51'
    },
    {
      sid: '510521',
      name: '泸县',
      citySid: '5105',
      provinceSid: '51'
    },
    {
      sid: '510522',
      name: '合江县',
      citySid: '5105',
      provinceSid: '51'
    },
    {
      sid: '510524',
      name: '叙永县',
      citySid: '5105',
      provinceSid: '51'
    },
    {
      sid: '510525',
      name: '古蔺县',
      citySid: '5105',
      provinceSid: '51'
    },
    {
      sid: '510603',
      name: '旌阳区',
      citySid: '5106',
      provinceSid: '51'
    },
    {
      sid: '510604',
      name: '罗江区',
      citySid: '5106',
      provinceSid: '51'
    },
    {
      sid: '510623',
      name: '中江县',
      citySid: '5106',
      provinceSid: '51'
    },
    {
      sid: '510681',
      name: '广汉市',
      citySid: '5106',
      provinceSid: '51'
    },
    {
      sid: '510682',
      name: '什邡市',
      citySid: '5106',
      provinceSid: '51'
    },
    {
      sid: '510683',
      name: '绵竹市',
      citySid: '5106',
      provinceSid: '51'
    },
    {
      sid: '510703',
      name: '涪城区',
      citySid: '5107',
      provinceSid: '51'
    },
    {
      sid: '510704',
      name: '游仙区',
      citySid: '5107',
      provinceSid: '51'
    },
    {
      sid: '510705',
      name: '安州区',
      citySid: '5107',
      provinceSid: '51'
    },
    {
      sid: '510722',
      name: '三台县',
      citySid: '5107',
      provinceSid: '51'
    },
    {
      sid: '510723',
      name: '盐亭县',
      citySid: '5107',
      provinceSid: '51'
    },
    {
      sid: '510725',
      name: '梓潼县',
      citySid: '5107',
      provinceSid: '51'
    },
    {
      sid: '510726',
      name: '北川羌族自治县',
      citySid: '5107',
      provinceSid: '51'
    },
    {
      sid: '510727',
      name: '平武县',
      citySid: '5107',
      provinceSid: '51'
    },
    {
      sid: '510781',
      name: '江油市',
      citySid: '5107',
      provinceSid: '51'
    },
    {
      sid: '510802',
      name: '利州区',
      citySid: '5108',
      provinceSid: '51'
    },
    {
      sid: '510811',
      name: '昭化区',
      citySid: '5108',
      provinceSid: '51'
    },
    {
      sid: '510812',
      name: '朝天区',
      citySid: '5108',
      provinceSid: '51'
    },
    {
      sid: '510821',
      name: '旺苍县',
      citySid: '5108',
      provinceSid: '51'
    },
    {
      sid: '510822',
      name: '青川县',
      citySid: '5108',
      provinceSid: '51'
    },
    {
      sid: '510823',
      name: '剑阁县',
      citySid: '5108',
      provinceSid: '51'
    },
    {
      sid: '510824',
      name: '苍溪县',
      citySid: '5108',
      provinceSid: '51'
    },
    {
      sid: '510903',
      name: '船山区',
      citySid: '5109',
      provinceSid: '51'
    },
    {
      sid: '510904',
      name: '安居区',
      citySid: '5109',
      provinceSid: '51'
    },
    {
      sid: '510921',
      name: '蓬溪县',
      citySid: '5109',
      provinceSid: '51'
    },
    {
      sid: '510922',
      name: '射洪县',
      citySid: '5109',
      provinceSid: '51'
    },
    {
      sid: '510923',
      name: '大英县',
      citySid: '5109',
      provinceSid: '51'
    },
    {
      sid: '511002',
      name: '市中区',
      citySid: '5110',
      provinceSid: '51'
    },
    {
      sid: '511011',
      name: '东兴区',
      citySid: '5110',
      provinceSid: '51'
    },
    {
      sid: '511024',
      name: '威远县',
      citySid: '5110',
      provinceSid: '51'
    },
    {
      sid: '511025',
      name: '资中县',
      citySid: '5110',
      provinceSid: '51'
    },
    {
      sid: '511071',
      name: '内江经济开发区',
      citySid: '5110',
      provinceSid: '51'
    },
    {
      sid: '511083',
      name: '隆昌市',
      citySid: '5110',
      provinceSid: '51'
    },
    {
      sid: '511102',
      name: '市中区',
      citySid: '5111',
      provinceSid: '51'
    },
    {
      sid: '511111',
      name: '沙湾区',
      citySid: '5111',
      provinceSid: '51'
    },
    {
      sid: '511112',
      name: '五通桥区',
      citySid: '5111',
      provinceSid: '51'
    },
    {
      sid: '511113',
      name: '金口河区',
      citySid: '5111',
      provinceSid: '51'
    },
    {
      sid: '511123',
      name: '犍为县',
      citySid: '5111',
      provinceSid: '51'
    },
    {
      sid: '511124',
      name: '井研县',
      citySid: '5111',
      provinceSid: '51'
    },
    {
      sid: '511126',
      name: '夹江县',
      citySid: '5111',
      provinceSid: '51'
    },
    {
      sid: '511129',
      name: '沐川县',
      citySid: '5111',
      provinceSid: '51'
    },
    {
      sid: '511132',
      name: '峨边彝族自治县',
      citySid: '5111',
      provinceSid: '51'
    },
    {
      sid: '511133',
      name: '马边彝族自治县',
      citySid: '5111',
      provinceSid: '51'
    },
    {
      sid: '511181',
      name: '峨眉山市',
      citySid: '5111',
      provinceSid: '51'
    },
    {
      sid: '511302',
      name: '顺庆区',
      citySid: '5113',
      provinceSid: '51'
    },
    {
      sid: '511303',
      name: '高坪区',
      citySid: '5113',
      provinceSid: '51'
    },
    {
      sid: '511304',
      name: '嘉陵区',
      citySid: '5113',
      provinceSid: '51'
    },
    {
      sid: '511321',
      name: '南部县',
      citySid: '5113',
      provinceSid: '51'
    },
    {
      sid: '511322',
      name: '营山县',
      citySid: '5113',
      provinceSid: '51'
    },
    {
      sid: '511323',
      name: '蓬安县',
      citySid: '5113',
      provinceSid: '51'
    },
    {
      sid: '511324',
      name: '仪陇县',
      citySid: '5113',
      provinceSid: '51'
    },
    {
      sid: '511325',
      name: '西充县',
      citySid: '5113',
      provinceSid: '51'
    },
    {
      sid: '511381',
      name: '阆中市',
      citySid: '5113',
      provinceSid: '51'
    },
    {
      sid: '511402',
      name: '东坡区',
      citySid: '5114',
      provinceSid: '51'
    },
    {
      sid: '511403',
      name: '彭山区',
      citySid: '5114',
      provinceSid: '51'
    },
    {
      sid: '511421',
      name: '仁寿县',
      citySid: '5114',
      provinceSid: '51'
    },
    {
      sid: '511423',
      name: '洪雅县',
      citySid: '5114',
      provinceSid: '51'
    },
    {
      sid: '511424',
      name: '丹棱县',
      citySid: '5114',
      provinceSid: '51'
    },
    {
      sid: '511425',
      name: '青神县',
      citySid: '5114',
      provinceSid: '51'
    },
    {
      sid: '511502',
      name: '翠屏区',
      citySid: '5115',
      provinceSid: '51'
    },
    {
      sid: '511503',
      name: '南溪区',
      citySid: '5115',
      provinceSid: '51'
    },
    {
      sid: '511521',
      name: '宜宾县',
      citySid: '5115',
      provinceSid: '51'
    },
    {
      sid: '511523',
      name: '江安县',
      citySid: '5115',
      provinceSid: '51'
    },
    {
      sid: '511524',
      name: '长宁县',
      citySid: '5115',
      provinceSid: '51'
    },
    {
      sid: '511525',
      name: '高县',
      citySid: '5115',
      provinceSid: '51'
    },
    {
      sid: '511526',
      name: '珙县',
      citySid: '5115',
      provinceSid: '51'
    },
    {
      sid: '511527',
      name: '筠连县',
      citySid: '5115',
      provinceSid: '51'
    },
    {
      sid: '511528',
      name: '兴文县',
      citySid: '5115',
      provinceSid: '51'
    },
    {
      sid: '511529',
      name: '屏山县',
      citySid: '5115',
      provinceSid: '51'
    },
    {
      sid: '511602',
      name: '广安区',
      citySid: '5116',
      provinceSid: '51'
    },
    {
      sid: '511603',
      name: '前锋区',
      citySid: '5116',
      provinceSid: '51'
    },
    {
      sid: '511621',
      name: '岳池县',
      citySid: '5116',
      provinceSid: '51'
    },
    {
      sid: '511622',
      name: '武胜县',
      citySid: '5116',
      provinceSid: '51'
    },
    {
      sid: '511623',
      name: '邻水县',
      citySid: '5116',
      provinceSid: '51'
    },
    {
      sid: '511681',
      name: '华蓥市',
      citySid: '5116',
      provinceSid: '51'
    },
    {
      sid: '511702',
      name: '通川区',
      citySid: '5117',
      provinceSid: '51'
    },
    {
      sid: '511703',
      name: '达川区',
      citySid: '5117',
      provinceSid: '51'
    },
    {
      sid: '511722',
      name: '宣汉县',
      citySid: '5117',
      provinceSid: '51'
    },
    {
      sid: '511723',
      name: '开江县',
      citySid: '5117',
      provinceSid: '51'
    },
    {
      sid: '511724',
      name: '大竹县',
      citySid: '5117',
      provinceSid: '51'
    },
    {
      sid: '511725',
      name: '渠县',
      citySid: '5117',
      provinceSid: '51'
    },
    {
      sid: '511771',
      name: '达州经济开发区',
      citySid: '5117',
      provinceSid: '51'
    },
    {
      sid: '511781',
      name: '万源市',
      citySid: '5117',
      provinceSid: '51'
    },
    {
      sid: '511802',
      name: '雨城区',
      citySid: '5118',
      provinceSid: '51'
    },
    {
      sid: '511803',
      name: '名山区',
      citySid: '5118',
      provinceSid: '51'
    },
    {
      sid: '511822',
      name: '荥经县',
      citySid: '5118',
      provinceSid: '51'
    },
    {
      sid: '511823',
      name: '汉源县',
      citySid: '5118',
      provinceSid: '51'
    },
    {
      sid: '511824',
      name: '石棉县',
      citySid: '5118',
      provinceSid: '51'
    },
    {
      sid: '511825',
      name: '天全县',
      citySid: '5118',
      provinceSid: '51'
    },
    {
      sid: '511826',
      name: '芦山县',
      citySid: '5118',
      provinceSid: '51'
    },
    {
      sid: '511827',
      name: '宝兴县',
      citySid: '5118',
      provinceSid: '51'
    },
    {
      sid: '511902',
      name: '巴州区',
      citySid: '5119',
      provinceSid: '51'
    },
    {
      sid: '511903',
      name: '恩阳区',
      citySid: '5119',
      provinceSid: '51'
    },
    {
      sid: '511921',
      name: '通江县',
      citySid: '5119',
      provinceSid: '51'
    },
    {
      sid: '511922',
      name: '南江县',
      citySid: '5119',
      provinceSid: '51'
    },
    {
      sid: '511923',
      name: '平昌县',
      citySid: '5119',
      provinceSid: '51'
    },
    {
      sid: '511971',
      name: '巴中经济开发区',
      citySid: '5119',
      provinceSid: '51'
    },
    {
      sid: '512002',
      name: '雁江区',
      citySid: '5120',
      provinceSid: '51'
    },
    {
      sid: '512021',
      name: '安岳县',
      citySid: '5120',
      provinceSid: '51'
    },
    {
      sid: '512022',
      name: '乐至县',
      citySid: '5120',
      provinceSid: '51'
    },
    {
      sid: '513201',
      name: '马尔康市',
      citySid: '5132',
      provinceSid: '51'
    },
    {
      sid: '513221',
      name: '汶川县',
      citySid: '5132',
      provinceSid: '51'
    },
    {
      sid: '513222',
      name: '理县',
      citySid: '5132',
      provinceSid: '51'
    },
    {
      sid: '513223',
      name: '茂县',
      citySid: '5132',
      provinceSid: '51'
    },
    {
      sid: '513224',
      name: '松潘县',
      citySid: '5132',
      provinceSid: '51'
    },
    {
      sid: '513225',
      name: '九寨沟县',
      citySid: '5132',
      provinceSid: '51'
    },
    {
      sid: '513226',
      name: '金川县',
      citySid: '5132',
      provinceSid: '51'
    },
    {
      sid: '513227',
      name: '小金县',
      citySid: '5132',
      provinceSid: '51'
    },
    {
      sid: '513228',
      name: '黑水县',
      citySid: '5132',
      provinceSid: '51'
    },
    {
      sid: '513230',
      name: '壤塘县',
      citySid: '5132',
      provinceSid: '51'
    },
    {
      sid: '513231',
      name: '阿坝县',
      citySid: '5132',
      provinceSid: '51'
    },
    {
      sid: '513232',
      name: '若尔盖县',
      citySid: '5132',
      provinceSid: '51'
    },
    {
      sid: '513233',
      name: '红原县',
      citySid: '5132',
      provinceSid: '51'
    },
    {
      sid: '513301',
      name: '康定市',
      citySid: '5133',
      provinceSid: '51'
    },
    {
      sid: '513322',
      name: '泸定县',
      citySid: '5133',
      provinceSid: '51'
    },
    {
      sid: '513323',
      name: '丹巴县',
      citySid: '5133',
      provinceSid: '51'
    },
    {
      sid: '513324',
      name: '九龙县',
      citySid: '5133',
      provinceSid: '51'
    },
    {
      sid: '513325',
      name: '雅江县',
      citySid: '5133',
      provinceSid: '51'
    },
    {
      sid: '513326',
      name: '道孚县',
      citySid: '5133',
      provinceSid: '51'
    },
    {
      sid: '513327',
      name: '炉霍县',
      citySid: '5133',
      provinceSid: '51'
    },
    {
      sid: '513328',
      name: '甘孜县',
      citySid: '5133',
      provinceSid: '51'
    },
    {
      sid: '513329',
      name: '新龙县',
      citySid: '5133',
      provinceSid: '51'
    },
    {
      sid: '513330',
      name: '德格县',
      citySid: '5133',
      provinceSid: '51'
    },
    {
      sid: '513331',
      name: '白玉县',
      citySid: '5133',
      provinceSid: '51'
    },
    {
      sid: '513332',
      name: '石渠县',
      citySid: '5133',
      provinceSid: '51'
    },
    {
      sid: '513333',
      name: '色达县',
      citySid: '5133',
      provinceSid: '51'
    },
    {
      sid: '513334',
      name: '理塘县',
      citySid: '5133',
      provinceSid: '51'
    },
    {
      sid: '513335',
      name: '巴塘县',
      citySid: '5133',
      provinceSid: '51'
    },
    {
      sid: '513336',
      name: '乡城县',
      citySid: '5133',
      provinceSid: '51'
    },
    {
      sid: '513337',
      name: '稻城县',
      citySid: '5133',
      provinceSid: '51'
    },
    {
      sid: '513338',
      name: '得荣县',
      citySid: '5133',
      provinceSid: '51'
    },
    {
      sid: '513401',
      name: '西昌市',
      citySid: '5134',
      provinceSid: '51'
    },
    {
      sid: '513422',
      name: '木里藏族自治县',
      citySid: '5134',
      provinceSid: '51'
    },
    {
      sid: '513423',
      name: '盐源县',
      citySid: '5134',
      provinceSid: '51'
    },
    {
      sid: '513424',
      name: '德昌县',
      citySid: '5134',
      provinceSid: '51'
    },
    {
      sid: '513425',
      name: '会理县',
      citySid: '5134',
      provinceSid: '51'
    },
    {
      sid: '513426',
      name: '会东县',
      citySid: '5134',
      provinceSid: '51'
    },
    {
      sid: '513427',
      name: '宁南县',
      citySid: '5134',
      provinceSid: '51'
    },
    {
      sid: '513428',
      name: '普格县',
      citySid: '5134',
      provinceSid: '51'
    },
    {
      sid: '513429',
      name: '布拖县',
      citySid: '5134',
      provinceSid: '51'
    },
    {
      sid: '513430',
      name: '金阳县',
      citySid: '5134',
      provinceSid: '51'
    },
    {
      sid: '513431',
      name: '昭觉县',
      citySid: '5134',
      provinceSid: '51'
    },
    {
      sid: '513432',
      name: '喜德县',
      citySid: '5134',
      provinceSid: '51'
    },
    {
      sid: '513433',
      name: '冕宁县',
      citySid: '5134',
      provinceSid: '51'
    },
    {
      sid: '513434',
      name: '越西县',
      citySid: '5134',
      provinceSid: '51'
    },
    {
      sid: '513435',
      name: '甘洛县',
      citySid: '5134',
      provinceSid: '51'
    },
    {
      sid: '513436',
      name: '美姑县',
      citySid: '5134',
      provinceSid: '51'
    },
    {
      sid: '513437',
      name: '雷波县',
      citySid: '5134',
      provinceSid: '51'
    },
    {
      sid: '520102',
      name: '南明区',
      citySid: '5201',
      provinceSid: '52'
    },
    {
      sid: '520103',
      name: '云岩区',
      citySid: '5201',
      provinceSid: '52'
    },
    {
      sid: '520111',
      name: '花溪区',
      citySid: '5201',
      provinceSid: '52'
    },
    {
      sid: '520112',
      name: '乌当区',
      citySid: '5201',
      provinceSid: '52'
    },
    {
      sid: '520113',
      name: '白云区',
      citySid: '5201',
      provinceSid: '52'
    },
    {
      sid: '520115',
      name: '观山湖区',
      citySid: '5201',
      provinceSid: '52'
    },
    {
      sid: '520121',
      name: '开阳县',
      citySid: '5201',
      provinceSid: '52'
    },
    {
      sid: '520122',
      name: '息烽县',
      citySid: '5201',
      provinceSid: '52'
    },
    {
      sid: '520123',
      name: '修文县',
      citySid: '5201',
      provinceSid: '52'
    },
    {
      sid: '520181',
      name: '清镇市',
      citySid: '5201',
      provinceSid: '52'
    },
    {
      sid: '520201',
      name: '钟山区',
      citySid: '5202',
      provinceSid: '52'
    },
    {
      sid: '520203',
      name: '六枝特区',
      citySid: '5202',
      provinceSid: '52'
    },
    {
      sid: '520221',
      name: '水城县',
      citySid: '5202',
      provinceSid: '52'
    },
    {
      sid: '520281',
      name: '盘州市',
      citySid: '5202',
      provinceSid: '52'
    },
    {
      sid: '520302',
      name: '红花岗区',
      citySid: '5203',
      provinceSid: '52'
    },
    {
      sid: '520303',
      name: '汇川区',
      citySid: '5203',
      provinceSid: '52'
    },
    {
      sid: '520304',
      name: '播州区',
      citySid: '5203',
      provinceSid: '52'
    },
    {
      sid: '520322',
      name: '桐梓县',
      citySid: '5203',
      provinceSid: '52'
    },
    {
      sid: '520323',
      name: '绥阳县',
      citySid: '5203',
      provinceSid: '52'
    },
    {
      sid: '520324',
      name: '正安县',
      citySid: '5203',
      provinceSid: '52'
    },
    {
      sid: '520325',
      name: '道真仡佬族苗族自治县',
      citySid: '5203',
      provinceSid: '52'
    },
    {
      sid: '520326',
      name: '务川仡佬族苗族自治县',
      citySid: '5203',
      provinceSid: '52'
    },
    {
      sid: '520327',
      name: '凤冈县',
      citySid: '5203',
      provinceSid: '52'
    },
    {
      sid: '520328',
      name: '湄潭县',
      citySid: '5203',
      provinceSid: '52'
    },
    {
      sid: '520329',
      name: '余庆县',
      citySid: '5203',
      provinceSid: '52'
    },
    {
      sid: '520330',
      name: '习水县',
      citySid: '5203',
      provinceSid: '52'
    },
    {
      sid: '520381',
      name: '赤水市',
      citySid: '5203',
      provinceSid: '52'
    },
    {
      sid: '520382',
      name: '仁怀市',
      citySid: '5203',
      provinceSid: '52'
    },
    {
      sid: '520402',
      name: '西秀区',
      citySid: '5204',
      provinceSid: '52'
    },
    {
      sid: '520403',
      name: '平坝区',
      citySid: '5204',
      provinceSid: '52'
    },
    {
      sid: '520422',
      name: '普定县',
      citySid: '5204',
      provinceSid: '52'
    },
    {
      sid: '520423',
      name: '镇宁布依族苗族自治县',
      citySid: '5204',
      provinceSid: '52'
    },
    {
      sid: '520424',
      name: '关岭布依族苗族自治县',
      citySid: '5204',
      provinceSid: '52'
    },
    {
      sid: '520425',
      name: '紫云苗族布依族自治县',
      citySid: '5204',
      provinceSid: '52'
    },
    {
      sid: '520502',
      name: '七星关区',
      citySid: '5205',
      provinceSid: '52'
    },
    {
      sid: '520521',
      name: '大方县',
      citySid: '5205',
      provinceSid: '52'
    },
    {
      sid: '520522',
      name: '黔西县',
      citySid: '5205',
      provinceSid: '52'
    },
    {
      sid: '520523',
      name: '金沙县',
      citySid: '5205',
      provinceSid: '52'
    },
    {
      sid: '520524',
      name: '织金县',
      citySid: '5205',
      provinceSid: '52'
    },
    {
      sid: '520525',
      name: '纳雍县',
      citySid: '5205',
      provinceSid: '52'
    },
    {
      sid: '520526',
      name: '威宁彝族回族苗族自治县',
      citySid: '5205',
      provinceSid: '52'
    },
    {
      sid: '520527',
      name: '赫章县',
      citySid: '5205',
      provinceSid: '52'
    },
    {
      sid: '520602',
      name: '碧江区',
      citySid: '5206',
      provinceSid: '52'
    },
    {
      sid: '520603',
      name: '万山区',
      citySid: '5206',
      provinceSid: '52'
    },
    {
      sid: '520621',
      name: '江口县',
      citySid: '5206',
      provinceSid: '52'
    },
    {
      sid: '520622',
      name: '玉屏侗族自治县',
      citySid: '5206',
      provinceSid: '52'
    },
    {
      sid: '520623',
      name: '石阡县',
      citySid: '5206',
      provinceSid: '52'
    },
    {
      sid: '520624',
      name: '思南县',
      citySid: '5206',
      provinceSid: '52'
    },
    {
      sid: '520625',
      name: '印江土家族苗族自治县',
      citySid: '5206',
      provinceSid: '52'
    },
    {
      sid: '520626',
      name: '德江县',
      citySid: '5206',
      provinceSid: '52'
    },
    {
      sid: '520627',
      name: '沿河土家族自治县',
      citySid: '5206',
      provinceSid: '52'
    },
    {
      sid: '520628',
      name: '松桃苗族自治县',
      citySid: '5206',
      provinceSid: '52'
    },
    {
      sid: '522301',
      name: '兴义市',
      citySid: '5223',
      provinceSid: '52'
    },
    {
      sid: '522322',
      name: '兴仁县',
      citySid: '5223',
      provinceSid: '52'
    },
    {
      sid: '522323',
      name: '普安县',
      citySid: '5223',
      provinceSid: '52'
    },
    {
      sid: '522324',
      name: '晴隆县',
      citySid: '5223',
      provinceSid: '52'
    },
    {
      sid: '522325',
      name: '贞丰县',
      citySid: '5223',
      provinceSid: '52'
    },
    {
      sid: '522326',
      name: '望谟县',
      citySid: '5223',
      provinceSid: '52'
    },
    {
      sid: '522327',
      name: '册亨县',
      citySid: '5223',
      provinceSid: '52'
    },
    {
      sid: '522328',
      name: '安龙县',
      citySid: '5223',
      provinceSid: '52'
    },
    {
      sid: '522601',
      name: '凯里市',
      citySid: '5226',
      provinceSid: '52'
    },
    {
      sid: '522622',
      name: '黄平县',
      citySid: '5226',
      provinceSid: '52'
    },
    {
      sid: '522623',
      name: '施秉县',
      citySid: '5226',
      provinceSid: '52'
    },
    {
      sid: '522624',
      name: '三穗县',
      citySid: '5226',
      provinceSid: '52'
    },
    {
      sid: '522625',
      name: '镇远县',
      citySid: '5226',
      provinceSid: '52'
    },
    {
      sid: '522626',
      name: '岑巩县',
      citySid: '5226',
      provinceSid: '52'
    },
    {
      sid: '522627',
      name: '天柱县',
      citySid: '5226',
      provinceSid: '52'
    },
    {
      sid: '522628',
      name: '锦屏县',
      citySid: '5226',
      provinceSid: '52'
    },
    {
      sid: '522629',
      name: '剑河县',
      citySid: '5226',
      provinceSid: '52'
    },
    {
      sid: '522630',
      name: '台江县',
      citySid: '5226',
      provinceSid: '52'
    },
    {
      sid: '522631',
      name: '黎平县',
      citySid: '5226',
      provinceSid: '52'
    },
    {
      sid: '522632',
      name: '榕江县',
      citySid: '5226',
      provinceSid: '52'
    },
    {
      sid: '522633',
      name: '从江县',
      citySid: '5226',
      provinceSid: '52'
    },
    {
      sid: '522634',
      name: '雷山县',
      citySid: '5226',
      provinceSid: '52'
    },
    {
      sid: '522635',
      name: '麻江县',
      citySid: '5226',
      provinceSid: '52'
    },
    {
      sid: '522636',
      name: '丹寨县',
      citySid: '5226',
      provinceSid: '52'
    },
    {
      sid: '522701',
      name: '都匀市',
      citySid: '5227',
      provinceSid: '52'
    },
    {
      sid: '522702',
      name: '福泉市',
      citySid: '5227',
      provinceSid: '52'
    },
    {
      sid: '522722',
      name: '荔波县',
      citySid: '5227',
      provinceSid: '52'
    },
    {
      sid: '522723',
      name: '贵定县',
      citySid: '5227',
      provinceSid: '52'
    },
    {
      sid: '522725',
      name: '瓮安县',
      citySid: '5227',
      provinceSid: '52'
    },
    {
      sid: '522726',
      name: '独山县',
      citySid: '5227',
      provinceSid: '52'
    },
    {
      sid: '522727',
      name: '平塘县',
      citySid: '5227',
      provinceSid: '52'
    },
    {
      sid: '522728',
      name: '罗甸县',
      citySid: '5227',
      provinceSid: '52'
    },
    {
      sid: '522729',
      name: '长顺县',
      citySid: '5227',
      provinceSid: '52'
    },
    {
      sid: '522730',
      name: '龙里县',
      citySid: '5227',
      provinceSid: '52'
    },
    {
      sid: '522731',
      name: '惠水县',
      citySid: '5227',
      provinceSid: '52'
    },
    {
      sid: '522732',
      name: '三都水族自治县',
      citySid: '5227',
      provinceSid: '52'
    },
    {
      sid: '530102',
      name: '五华区',
      citySid: '5301',
      provinceSid: '53'
    },
    {
      sid: '530103',
      name: '盘龙区',
      citySid: '5301',
      provinceSid: '53'
    },
    {
      sid: '530111',
      name: '官渡区',
      citySid: '5301',
      provinceSid: '53'
    },
    {
      sid: '530112',
      name: '西山区',
      citySid: '5301',
      provinceSid: '53'
    },
    {
      sid: '530113',
      name: '东川区',
      citySid: '5301',
      provinceSid: '53'
    },
    {
      sid: '530114',
      name: '呈贡区',
      citySid: '5301',
      provinceSid: '53'
    },
    {
      sid: '530115',
      name: '晋宁区',
      citySid: '5301',
      provinceSid: '53'
    },
    {
      sid: '530124',
      name: '富民县',
      citySid: '5301',
      provinceSid: '53'
    },
    {
      sid: '530125',
      name: '宜良县',
      citySid: '5301',
      provinceSid: '53'
    },
    {
      sid: '530126',
      name: '石林彝族自治县',
      citySid: '5301',
      provinceSid: '53'
    },
    {
      sid: '530127',
      name: '嵩明县',
      citySid: '5301',
      provinceSid: '53'
    },
    {
      sid: '530128',
      name: '禄劝彝族苗族自治县',
      citySid: '5301',
      provinceSid: '53'
    },
    {
      sid: '530129',
      name: '寻甸回族彝族自治县',
      citySid: '5301',
      provinceSid: '53'
    },
    {
      sid: '530181',
      name: '安宁市',
      citySid: '5301',
      provinceSid: '53'
    },
    {
      sid: '530302',
      name: '麒麟区',
      citySid: '5303',
      provinceSid: '53'
    },
    {
      sid: '530303',
      name: '沾益区',
      citySid: '5303',
      provinceSid: '53'
    },
    {
      sid: '530321',
      name: '马龙县',
      citySid: '5303',
      provinceSid: '53'
    },
    {
      sid: '530322',
      name: '陆良县',
      citySid: '5303',
      provinceSid: '53'
    },
    {
      sid: '530323',
      name: '师宗县',
      citySid: '5303',
      provinceSid: '53'
    },
    {
      sid: '530324',
      name: '罗平县',
      citySid: '5303',
      provinceSid: '53'
    },
    {
      sid: '530325',
      name: '富源县',
      citySid: '5303',
      provinceSid: '53'
    },
    {
      sid: '530326',
      name: '会泽县',
      citySid: '5303',
      provinceSid: '53'
    },
    {
      sid: '530381',
      name: '宣威市',
      citySid: '5303',
      provinceSid: '53'
    },
    {
      sid: '530402',
      name: '红塔区',
      citySid: '5304',
      provinceSid: '53'
    },
    {
      sid: '530403',
      name: '江川区',
      citySid: '5304',
      provinceSid: '53'
    },
    {
      sid: '530422',
      name: '澄江县',
      citySid: '5304',
      provinceSid: '53'
    },
    {
      sid: '530423',
      name: '通海县',
      citySid: '5304',
      provinceSid: '53'
    },
    {
      sid: '530424',
      name: '华宁县',
      citySid: '5304',
      provinceSid: '53'
    },
    {
      sid: '530425',
      name: '易门县',
      citySid: '5304',
      provinceSid: '53'
    },
    {
      sid: '530426',
      name: '峨山彝族自治县',
      citySid: '5304',
      provinceSid: '53'
    },
    {
      sid: '530427',
      name: '新平彝族傣族自治县',
      citySid: '5304',
      provinceSid: '53'
    },
    {
      sid: '530428',
      name: '元江哈尼族彝族傣族自治县',
      citySid: '5304',
      provinceSid: '53'
    },
    {
      sid: '530502',
      name: '隆阳区',
      citySid: '5305',
      provinceSid: '53'
    },
    {
      sid: '530521',
      name: '施甸县',
      citySid: '5305',
      provinceSid: '53'
    },
    {
      sid: '530523',
      name: '龙陵县',
      citySid: '5305',
      provinceSid: '53'
    },
    {
      sid: '530524',
      name: '昌宁县',
      citySid: '5305',
      provinceSid: '53'
    },
    {
      sid: '530581',
      name: '腾冲市',
      citySid: '5305',
      provinceSid: '53'
    },
    {
      sid: '530602',
      name: '昭阳区',
      citySid: '5306',
      provinceSid: '53'
    },
    {
      sid: '530621',
      name: '鲁甸县',
      citySid: '5306',
      provinceSid: '53'
    },
    {
      sid: '530622',
      name: '巧家县',
      citySid: '5306',
      provinceSid: '53'
    },
    {
      sid: '530623',
      name: '盐津县',
      citySid: '5306',
      provinceSid: '53'
    },
    {
      sid: '530624',
      name: '大关县',
      citySid: '5306',
      provinceSid: '53'
    },
    {
      sid: '530625',
      name: '永善县',
      citySid: '5306',
      provinceSid: '53'
    },
    {
      sid: '530626',
      name: '绥江县',
      citySid: '5306',
      provinceSid: '53'
    },
    {
      sid: '530627',
      name: '镇雄县',
      citySid: '5306',
      provinceSid: '53'
    },
    {
      sid: '530628',
      name: '彝良县',
      citySid: '5306',
      provinceSid: '53'
    },
    {
      sid: '530629',
      name: '威信县',
      citySid: '5306',
      provinceSid: '53'
    },
    {
      sid: '530630',
      name: '水富县',
      citySid: '5306',
      provinceSid: '53'
    },
    {
      sid: '530702',
      name: '古城区',
      citySid: '5307',
      provinceSid: '53'
    },
    {
      sid: '530721',
      name: '玉龙纳西族自治县',
      citySid: '5307',
      provinceSid: '53'
    },
    {
      sid: '530722',
      name: '永胜县',
      citySid: '5307',
      provinceSid: '53'
    },
    {
      sid: '530723',
      name: '华坪县',
      citySid: '5307',
      provinceSid: '53'
    },
    {
      sid: '530724',
      name: '宁蒗彝族自治县',
      citySid: '5307',
      provinceSid: '53'
    },
    {
      sid: '530802',
      name: '思茅区',
      citySid: '5308',
      provinceSid: '53'
    },
    {
      sid: '530821',
      name: '宁洱哈尼族彝族自治县',
      citySid: '5308',
      provinceSid: '53'
    },
    {
      sid: '530822',
      name: '墨江哈尼族自治县',
      citySid: '5308',
      provinceSid: '53'
    },
    {
      sid: '530823',
      name: '景东彝族自治县',
      citySid: '5308',
      provinceSid: '53'
    },
    {
      sid: '530824',
      name: '景谷傣族彝族自治县',
      citySid: '5308',
      provinceSid: '53'
    },
    {
      sid: '530825',
      name: '镇沅彝族哈尼族拉祜族自治县',
      citySid: '5308',
      provinceSid: '53'
    },
    {
      sid: '530826',
      name: '江城哈尼族彝族自治县',
      citySid: '5308',
      provinceSid: '53'
    },
    {
      sid: '530827',
      name: '孟连傣族拉祜族佤族自治县',
      citySid: '5308',
      provinceSid: '53'
    },
    {
      sid: '530828',
      name: '澜沧拉祜族自治县',
      citySid: '5308',
      provinceSid: '53'
    },
    {
      sid: '530829',
      name: '西盟佤族自治县',
      citySid: '5308',
      provinceSid: '53'
    },
    {
      sid: '530902',
      name: '临翔区',
      citySid: '5309',
      provinceSid: '53'
    },
    {
      sid: '530921',
      name: '凤庆县',
      citySid: '5309',
      provinceSid: '53'
    },
    {
      sid: '530922',
      name: '云县',
      citySid: '5309',
      provinceSid: '53'
    },
    {
      sid: '530923',
      name: '永德县',
      citySid: '5309',
      provinceSid: '53'
    },
    {
      sid: '530924',
      name: '镇康县',
      citySid: '5309',
      provinceSid: '53'
    },
    {
      sid: '530925',
      name: '双江拉祜族佤族布朗族傣族自治县',
      citySid: '5309',
      provinceSid: '53'
    },
    {
      sid: '530926',
      name: '耿马傣族佤族自治县',
      citySid: '5309',
      provinceSid: '53'
    },
    {
      sid: '530927',
      name: '沧源佤族自治县',
      citySid: '5309',
      provinceSid: '53'
    },
    {
      sid: '532301',
      name: '楚雄市',
      citySid: '5323',
      provinceSid: '53'
    },
    {
      sid: '532322',
      name: '双柏县',
      citySid: '5323',
      provinceSid: '53'
    },
    {
      sid: '532323',
      name: '牟定县',
      citySid: '5323',
      provinceSid: '53'
    },
    {
      sid: '532324',
      name: '南华县',
      citySid: '5323',
      provinceSid: '53'
    },
    {
      sid: '532325',
      name: '姚安县',
      citySid: '5323',
      provinceSid: '53'
    },
    {
      sid: '532326',
      name: '大姚县',
      citySid: '5323',
      provinceSid: '53'
    },
    {
      sid: '532327',
      name: '永仁县',
      citySid: '5323',
      provinceSid: '53'
    },
    {
      sid: '532328',
      name: '元谋县',
      citySid: '5323',
      provinceSid: '53'
    },
    {
      sid: '532329',
      name: '武定县',
      citySid: '5323',
      provinceSid: '53'
    },
    {
      sid: '532331',
      name: '禄丰县',
      citySid: '5323',
      provinceSid: '53'
    },
    {
      sid: '532501',
      name: '个旧市',
      citySid: '5325',
      provinceSid: '53'
    },
    {
      sid: '532502',
      name: '开远市',
      citySid: '5325',
      provinceSid: '53'
    },
    {
      sid: '532503',
      name: '蒙自市',
      citySid: '5325',
      provinceSid: '53'
    },
    {
      sid: '532504',
      name: '弥勒市',
      citySid: '5325',
      provinceSid: '53'
    },
    {
      sid: '532523',
      name: '屏边苗族自治县',
      citySid: '5325',
      provinceSid: '53'
    },
    {
      sid: '532524',
      name: '建水县',
      citySid: '5325',
      provinceSid: '53'
    },
    {
      sid: '532525',
      name: '石屏县',
      citySid: '5325',
      provinceSid: '53'
    },
    {
      sid: '532527',
      name: '泸西县',
      citySid: '5325',
      provinceSid: '53'
    },
    {
      sid: '532528',
      name: '元阳县',
      citySid: '5325',
      provinceSid: '53'
    },
    {
      sid: '532529',
      name: '红河县',
      citySid: '5325',
      provinceSid: '53'
    },
    {
      sid: '532530',
      name: '金平苗族瑶族傣族自治县',
      citySid: '5325',
      provinceSid: '53'
    },
    {
      sid: '532531',
      name: '绿春县',
      citySid: '5325',
      provinceSid: '53'
    },
    {
      sid: '532532',
      name: '河口瑶族自治县',
      citySid: '5325',
      provinceSid: '53'
    },
    {
      sid: '532601',
      name: '文山市',
      citySid: '5326',
      provinceSid: '53'
    },
    {
      sid: '532622',
      name: '砚山县',
      citySid: '5326',
      provinceSid: '53'
    },
    {
      sid: '532623',
      name: '西畴县',
      citySid: '5326',
      provinceSid: '53'
    },
    {
      sid: '532624',
      name: '麻栗坡县',
      citySid: '5326',
      provinceSid: '53'
    },
    {
      sid: '532625',
      name: '马关县',
      citySid: '5326',
      provinceSid: '53'
    },
    {
      sid: '532626',
      name: '丘北县',
      citySid: '5326',
      provinceSid: '53'
    },
    {
      sid: '532627',
      name: '广南县',
      citySid: '5326',
      provinceSid: '53'
    },
    {
      sid: '532628',
      name: '富宁县',
      citySid: '5326',
      provinceSid: '53'
    },
    {
      sid: '532801',
      name: '景洪市',
      citySid: '5328',
      provinceSid: '53'
    },
    {
      sid: '532822',
      name: '勐海县',
      citySid: '5328',
      provinceSid: '53'
    },
    {
      sid: '532823',
      name: '勐腊县',
      citySid: '5328',
      provinceSid: '53'
    },
    {
      sid: '532901',
      name: '大理市',
      citySid: '5329',
      provinceSid: '53'
    },
    {
      sid: '532922',
      name: '漾濞彝族自治县',
      citySid: '5329',
      provinceSid: '53'
    },
    {
      sid: '532923',
      name: '祥云县',
      citySid: '5329',
      provinceSid: '53'
    },
    {
      sid: '532924',
      name: '宾川县',
      citySid: '5329',
      provinceSid: '53'
    },
    {
      sid: '532925',
      name: '弥渡县',
      citySid: '5329',
      provinceSid: '53'
    },
    {
      sid: '532926',
      name: '南涧彝族自治县',
      citySid: '5329',
      provinceSid: '53'
    },
    {
      sid: '532927',
      name: '巍山彝族回族自治县',
      citySid: '5329',
      provinceSid: '53'
    },
    {
      sid: '532928',
      name: '永平县',
      citySid: '5329',
      provinceSid: '53'
    },
    {
      sid: '532929',
      name: '云龙县',
      citySid: '5329',
      provinceSid: '53'
    },
    {
      sid: '532930',
      name: '洱源县',
      citySid: '5329',
      provinceSid: '53'
    },
    {
      sid: '532931',
      name: '剑川县',
      citySid: '5329',
      provinceSid: '53'
    },
    {
      sid: '532932',
      name: '鹤庆县',
      citySid: '5329',
      provinceSid: '53'
    },
    {
      sid: '533102',
      name: '瑞丽市',
      citySid: '5331',
      provinceSid: '53'
    },
    {
      sid: '533103',
      name: '芒市',
      citySid: '5331',
      provinceSid: '53'
    },
    {
      sid: '533122',
      name: '梁河县',
      citySid: '5331',
      provinceSid: '53'
    },
    {
      sid: '533123',
      name: '盈江县',
      citySid: '5331',
      provinceSid: '53'
    },
    {
      sid: '533124',
      name: '陇川县',
      citySid: '5331',
      provinceSid: '53'
    },
    {
      sid: '533301',
      name: '泸水市',
      citySid: '5333',
      provinceSid: '53'
    },
    {
      sid: '533323',
      name: '福贡县',
      citySid: '5333',
      provinceSid: '53'
    },
    {
      sid: '533324',
      name: '贡山独龙族怒族自治县',
      citySid: '5333',
      provinceSid: '53'
    },
    {
      sid: '533325',
      name: '兰坪白族普米族自治县',
      citySid: '5333',
      provinceSid: '53'
    },
    {
      sid: '533401',
      name: '香格里拉市',
      citySid: '5334',
      provinceSid: '53'
    },
    {
      sid: '533422',
      name: '德钦县',
      citySid: '5334',
      provinceSid: '53'
    },
    {
      sid: '533423',
      name: '维西傈僳族自治县',
      citySid: '5334',
      provinceSid: '53'
    },
    {
      sid: '540102',
      name: '城关区',
      citySid: '5401',
      provinceSid: '54'
    },
    {
      sid: '540103',
      name: '堆龙德庆区',
      citySid: '5401',
      provinceSid: '54'
    },
    {
      sid: '540121',
      name: '林周县',
      citySid: '5401',
      provinceSid: '54'
    },
    {
      sid: '540122',
      name: '当雄县',
      citySid: '5401',
      provinceSid: '54'
    },
    {
      sid: '540123',
      name: '尼木县',
      citySid: '5401',
      provinceSid: '54'
    },
    {
      sid: '540124',
      name: '曲水县',
      citySid: '5401',
      provinceSid: '54'
    },
    {
      sid: '540126',
      name: '达孜县',
      citySid: '5401',
      provinceSid: '54'
    },
    {
      sid: '540127',
      name: '墨竹工卡县',
      citySid: '5401',
      provinceSid: '54'
    },
    {
      sid: '540171',
      name: '格尔木藏青工业园区',
      citySid: '5401',
      provinceSid: '54'
    },
    {
      sid: '540172',
      name: '拉萨经济技术开发区',
      citySid: '5401',
      provinceSid: '54'
    },
    {
      sid: '540173',
      name: '西藏文化旅游创意园区',
      citySid: '5401',
      provinceSid: '54'
    },
    {
      sid: '540174',
      name: '达孜工业园区',
      citySid: '5401',
      provinceSid: '54'
    },
    {
      sid: '540202',
      name: '桑珠孜区',
      citySid: '5402',
      provinceSid: '54'
    },
    {
      sid: '540221',
      name: '南木林县',
      citySid: '5402',
      provinceSid: '54'
    },
    {
      sid: '540222',
      name: '江孜县',
      citySid: '5402',
      provinceSid: '54'
    },
    {
      sid: '540223',
      name: '定日县',
      citySid: '5402',
      provinceSid: '54'
    },
    {
      sid: '540224',
      name: '萨迦县',
      citySid: '5402',
      provinceSid: '54'
    },
    {
      sid: '540225',
      name: '拉孜县',
      citySid: '5402',
      provinceSid: '54'
    },
    {
      sid: '540226',
      name: '昂仁县',
      citySid: '5402',
      provinceSid: '54'
    },
    {
      sid: '540227',
      name: '谢通门县',
      citySid: '5402',
      provinceSid: '54'
    },
    {
      sid: '540228',
      name: '白朗县',
      citySid: '5402',
      provinceSid: '54'
    },
    {
      sid: '540229',
      name: '仁布县',
      citySid: '5402',
      provinceSid: '54'
    },
    {
      sid: '540230',
      name: '康马县',
      citySid: '5402',
      provinceSid: '54'
    },
    {
      sid: '540231',
      name: '定结县',
      citySid: '5402',
      provinceSid: '54'
    },
    {
      sid: '540232',
      name: '仲巴县',
      citySid: '5402',
      provinceSid: '54'
    },
    {
      sid: '540233',
      name: '亚东县',
      citySid: '5402',
      provinceSid: '54'
    },
    {
      sid: '540234',
      name: '吉隆县',
      citySid: '5402',
      provinceSid: '54'
    },
    {
      sid: '540235',
      name: '聂拉木县',
      citySid: '5402',
      provinceSid: '54'
    },
    {
      sid: '540236',
      name: '萨嘎县',
      citySid: '5402',
      provinceSid: '54'
    },
    {
      sid: '540237',
      name: '岗巴县',
      citySid: '5402',
      provinceSid: '54'
    },
    {
      sid: '540302',
      name: '卡若区',
      citySid: '5403',
      provinceSid: '54'
    },
    {
      sid: '540321',
      name: '江达县',
      citySid: '5403',
      provinceSid: '54'
    },
    {
      sid: '540322',
      name: '贡觉县',
      citySid: '5403',
      provinceSid: '54'
    },
    {
      sid: '540323',
      name: '类乌齐县',
      citySid: '5403',
      provinceSid: '54'
    },
    {
      sid: '540324',
      name: '丁青县',
      citySid: '5403',
      provinceSid: '54'
    },
    {
      sid: '540325',
      name: '察雅县',
      citySid: '5403',
      provinceSid: '54'
    },
    {
      sid: '540326',
      name: '八宿县',
      citySid: '5403',
      provinceSid: '54'
    },
    {
      sid: '540327',
      name: '左贡县',
      citySid: '5403',
      provinceSid: '54'
    },
    {
      sid: '540328',
      name: '芒康县',
      citySid: '5403',
      provinceSid: '54'
    },
    {
      sid: '540329',
      name: '洛隆县',
      citySid: '5403',
      provinceSid: '54'
    },
    {
      sid: '540330',
      name: '边坝县',
      citySid: '5403',
      provinceSid: '54'
    },
    {
      sid: '540402',
      name: '巴宜区',
      citySid: '5404',
      provinceSid: '54'
    },
    {
      sid: '540421',
      name: '工布江达县',
      citySid: '5404',
      provinceSid: '54'
    },
    {
      sid: '540422',
      name: '米林县',
      citySid: '5404',
      provinceSid: '54'
    },
    {
      sid: '540423',
      name: '墨脱县',
      citySid: '5404',
      provinceSid: '54'
    },
    {
      sid: '540424',
      name: '波密县',
      citySid: '5404',
      provinceSid: '54'
    },
    {
      sid: '540425',
      name: '察隅县',
      citySid: '5404',
      provinceSid: '54'
    },
    {
      sid: '540426',
      name: '朗县',
      citySid: '5404',
      provinceSid: '54'
    },
    {
      sid: '540502',
      name: '乃东区',
      citySid: '5405',
      provinceSid: '54'
    },
    {
      sid: '540521',
      name: '扎囊县',
      citySid: '5405',
      provinceSid: '54'
    },
    {
      sid: '540522',
      name: '贡嘎县',
      citySid: '5405',
      provinceSid: '54'
    },
    {
      sid: '540523',
      name: '桑日县',
      citySid: '5405',
      provinceSid: '54'
    },
    {
      sid: '540524',
      name: '琼结县',
      citySid: '5405',
      provinceSid: '54'
    },
    {
      sid: '540525',
      name: '曲松县',
      citySid: '5405',
      provinceSid: '54'
    },
    {
      sid: '540526',
      name: '措美县',
      citySid: '5405',
      provinceSid: '54'
    },
    {
      sid: '540527',
      name: '洛扎县',
      citySid: '5405',
      provinceSid: '54'
    },
    {
      sid: '540528',
      name: '加查县',
      citySid: '5405',
      provinceSid: '54'
    },
    {
      sid: '540529',
      name: '隆子县',
      citySid: '5405',
      provinceSid: '54'
    },
    {
      sid: '540530',
      name: '错那县',
      citySid: '5405',
      provinceSid: '54'
    },
    {
      sid: '540531',
      name: '浪卡子县',
      citySid: '5405',
      provinceSid: '54'
    },
    {
      sid: '542421',
      name: '那曲县',
      citySid: '5424',
      provinceSid: '54'
    },
    {
      sid: '542422',
      name: '嘉黎县',
      citySid: '5424',
      provinceSid: '54'
    },
    {
      sid: '542423',
      name: '比如县',
      citySid: '5424',
      provinceSid: '54'
    },
    {
      sid: '542424',
      name: '聂荣县',
      citySid: '5424',
      provinceSid: '54'
    },
    {
      sid: '542425',
      name: '安多县',
      citySid: '5424',
      provinceSid: '54'
    },
    {
      sid: '542426',
      name: '申扎县',
      citySid: '5424',
      provinceSid: '54'
    },
    {
      sid: '542427',
      name: '索县',
      citySid: '5424',
      provinceSid: '54'
    },
    {
      sid: '542428',
      name: '班戈县',
      citySid: '5424',
      provinceSid: '54'
    },
    {
      sid: '542429',
      name: '巴青县',
      citySid: '5424',
      provinceSid: '54'
    },
    {
      sid: '542430',
      name: '尼玛县',
      citySid: '5424',
      provinceSid: '54'
    },
    {
      sid: '542431',
      name: '双湖县',
      citySid: '5424',
      provinceSid: '54'
    },
    {
      sid: '542521',
      name: '普兰县',
      citySid: '5425',
      provinceSid: '54'
    },
    {
      sid: '542522',
      name: '札达县',
      citySid: '5425',
      provinceSid: '54'
    },
    {
      sid: '542523',
      name: '噶尔县',
      citySid: '5425',
      provinceSid: '54'
    },
    {
      sid: '542524',
      name: '日土县',
      citySid: '5425',
      provinceSid: '54'
    },
    {
      sid: '542525',
      name: '革吉县',
      citySid: '5425',
      provinceSid: '54'
    },
    {
      sid: '542526',
      name: '改则县',
      citySid: '5425',
      provinceSid: '54'
    },
    {
      sid: '542527',
      name: '措勤县',
      citySid: '5425',
      provinceSid: '54'
    },
    {
      sid: '610102',
      name: '新城区',
      citySid: '6101',
      provinceSid: '61'
    },
    {
      sid: '610103',
      name: '碑林区',
      citySid: '6101',
      provinceSid: '61'
    },
    {
      sid: '610104',
      name: '莲湖区',
      citySid: '6101',
      provinceSid: '61'
    },
    {
      sid: '610111',
      name: '灞桥区',
      citySid: '6101',
      provinceSid: '61'
    },
    {
      sid: '610112',
      name: '未央区',
      citySid: '6101',
      provinceSid: '61'
    },
    {
      sid: '610113',
      name: '雁塔区',
      citySid: '6101',
      provinceSid: '61'
    },
    {
      sid: '610114',
      name: '阎良区',
      citySid: '6101',
      provinceSid: '61'
    },
    {
      sid: '610115',
      name: '临潼区',
      citySid: '6101',
      provinceSid: '61'
    },
    {
      sid: '610116',
      name: '长安区',
      citySid: '6101',
      provinceSid: '61'
    },
    {
      sid: '610117',
      name: '高陵区',
      citySid: '6101',
      provinceSid: '61'
    },
    {
      sid: '610118',
      name: '鄠邑区',
      citySid: '6101',
      provinceSid: '61'
    },
    {
      sid: '610122',
      name: '蓝田县',
      citySid: '6101',
      provinceSid: '61'
    },
    {
      sid: '610124',
      name: '周至县',
      citySid: '6101',
      provinceSid: '61'
    },
    {
      sid: '610202',
      name: '王益区',
      citySid: '6102',
      provinceSid: '61'
    },
    {
      sid: '610203',
      name: '印台区',
      citySid: '6102',
      provinceSid: '61'
    },
    {
      sid: '610204',
      name: '耀州区',
      citySid: '6102',
      provinceSid: '61'
    },
    {
      sid: '610222',
      name: '宜君县',
      citySid: '6102',
      provinceSid: '61'
    },
    {
      sid: '610302',
      name: '渭滨区',
      citySid: '6103',
      provinceSid: '61'
    },
    {
      sid: '610303',
      name: '金台区',
      citySid: '6103',
      provinceSid: '61'
    },
    {
      sid: '610304',
      name: '陈仓区',
      citySid: '6103',
      provinceSid: '61'
    },
    {
      sid: '610322',
      name: '凤翔县',
      citySid: '6103',
      provinceSid: '61'
    },
    {
      sid: '610323',
      name: '岐山县',
      citySid: '6103',
      provinceSid: '61'
    },
    {
      sid: '610324',
      name: '扶风县',
      citySid: '6103',
      provinceSid: '61'
    },
    {
      sid: '610326',
      name: '眉县',
      citySid: '6103',
      provinceSid: '61'
    },
    {
      sid: '610327',
      name: '陇县',
      citySid: '6103',
      provinceSid: '61'
    },
    {
      sid: '610328',
      name: '千阳县',
      citySid: '6103',
      provinceSid: '61'
    },
    {
      sid: '610329',
      name: '麟游县',
      citySid: '6103',
      provinceSid: '61'
    },
    {
      sid: '610330',
      name: '凤县',
      citySid: '6103',
      provinceSid: '61'
    },
    {
      sid: '610331',
      name: '太白县',
      citySid: '6103',
      provinceSid: '61'
    },
    {
      sid: '610402',
      name: '秦都区',
      citySid: '6104',
      provinceSid: '61'
    },
    {
      sid: '610403',
      name: '杨陵区',
      citySid: '6104',
      provinceSid: '61'
    },
    {
      sid: '610404',
      name: '渭城区',
      citySid: '6104',
      provinceSid: '61'
    },
    {
      sid: '610422',
      name: '三原县',
      citySid: '6104',
      provinceSid: '61'
    },
    {
      sid: '610423',
      name: '泾阳县',
      citySid: '6104',
      provinceSid: '61'
    },
    {
      sid: '610424',
      name: '乾县',
      citySid: '6104',
      provinceSid: '61'
    },
    {
      sid: '610425',
      name: '礼泉县',
      citySid: '6104',
      provinceSid: '61'
    },
    {
      sid: '610426',
      name: '永寿县',
      citySid: '6104',
      provinceSid: '61'
    },
    {
      sid: '610427',
      name: '彬县',
      citySid: '6104',
      provinceSid: '61'
    },
    {
      sid: '610428',
      name: '长武县',
      citySid: '6104',
      provinceSid: '61'
    },
    {
      sid: '610429',
      name: '旬邑县',
      citySid: '6104',
      provinceSid: '61'
    },
    {
      sid: '610430',
      name: '淳化县',
      citySid: '6104',
      provinceSid: '61'
    },
    {
      sid: '610431',
      name: '武功县',
      citySid: '6104',
      provinceSid: '61'
    },
    {
      sid: '610481',
      name: '兴平市',
      citySid: '6104',
      provinceSid: '61'
    },
    {
      sid: '610502',
      name: '临渭区',
      citySid: '6105',
      provinceSid: '61'
    },
    {
      sid: '610503',
      name: '华州区',
      citySid: '6105',
      provinceSid: '61'
    },
    {
      sid: '610522',
      name: '潼关县',
      citySid: '6105',
      provinceSid: '61'
    },
    {
      sid: '610523',
      name: '大荔县',
      citySid: '6105',
      provinceSid: '61'
    },
    {
      sid: '610524',
      name: '合阳县',
      citySid: '6105',
      provinceSid: '61'
    },
    {
      sid: '610525',
      name: '澄城县',
      citySid: '6105',
      provinceSid: '61'
    },
    {
      sid: '610526',
      name: '蒲城县',
      citySid: '6105',
      provinceSid: '61'
    },
    {
      sid: '610527',
      name: '白水县',
      citySid: '6105',
      provinceSid: '61'
    },
    {
      sid: '610528',
      name: '富平县',
      citySid: '6105',
      provinceSid: '61'
    },
    {
      sid: '610581',
      name: '韩城市',
      citySid: '6105',
      provinceSid: '61'
    },
    {
      sid: '610582',
      name: '华阴市',
      citySid: '6105',
      provinceSid: '61'
    },
    {
      sid: '610602',
      name: '宝塔区',
      citySid: '6106',
      provinceSid: '61'
    },
    {
      sid: '610603',
      name: '安塞区',
      citySid: '6106',
      provinceSid: '61'
    },
    {
      sid: '610621',
      name: '延长县',
      citySid: '6106',
      provinceSid: '61'
    },
    {
      sid: '610622',
      name: '延川县',
      citySid: '6106',
      provinceSid: '61'
    },
    {
      sid: '610623',
      name: '子长县',
      citySid: '6106',
      provinceSid: '61'
    },
    {
      sid: '610625',
      name: '志丹县',
      citySid: '6106',
      provinceSid: '61'
    },
    {
      sid: '610626',
      name: '吴起县',
      citySid: '6106',
      provinceSid: '61'
    },
    {
      sid: '610627',
      name: '甘泉县',
      citySid: '6106',
      provinceSid: '61'
    },
    {
      sid: '610628',
      name: '富县',
      citySid: '6106',
      provinceSid: '61'
    },
    {
      sid: '610629',
      name: '洛川县',
      citySid: '6106',
      provinceSid: '61'
    },
    {
      sid: '610630',
      name: '宜川县',
      citySid: '6106',
      provinceSid: '61'
    },
    {
      sid: '610631',
      name: '黄龙县',
      citySid: '6106',
      provinceSid: '61'
    },
    {
      sid: '610632',
      name: '黄陵县',
      citySid: '6106',
      provinceSid: '61'
    },
    {
      sid: '610702',
      name: '汉台区',
      citySid: '6107',
      provinceSid: '61'
    },
    {
      sid: '610703',
      name: '南郑区',
      citySid: '6107',
      provinceSid: '61'
    },
    {
      sid: '610722',
      name: '城固县',
      citySid: '6107',
      provinceSid: '61'
    },
    {
      sid: '610723',
      name: '洋县',
      citySid: '6107',
      provinceSid: '61'
    },
    {
      sid: '610724',
      name: '西乡县',
      citySid: '6107',
      provinceSid: '61'
    },
    {
      sid: '610725',
      name: '勉县',
      citySid: '6107',
      provinceSid: '61'
    },
    {
      sid: '610726',
      name: '宁强县',
      citySid: '6107',
      provinceSid: '61'
    },
    {
      sid: '610727',
      name: '略阳县',
      citySid: '6107',
      provinceSid: '61'
    },
    {
      sid: '610728',
      name: '镇巴县',
      citySid: '6107',
      provinceSid: '61'
    },
    {
      sid: '610729',
      name: '留坝县',
      citySid: '6107',
      provinceSid: '61'
    },
    {
      sid: '610730',
      name: '佛坪县',
      citySid: '6107',
      provinceSid: '61'
    },
    {
      sid: '610802',
      name: '榆阳区',
      citySid: '6108',
      provinceSid: '61'
    },
    {
      sid: '610803',
      name: '横山区',
      citySid: '6108',
      provinceSid: '61'
    },
    {
      sid: '610822',
      name: '府谷县',
      citySid: '6108',
      provinceSid: '61'
    },
    {
      sid: '610824',
      name: '靖边县',
      citySid: '6108',
      provinceSid: '61'
    },
    {
      sid: '610825',
      name: '定边县',
      citySid: '6108',
      provinceSid: '61'
    },
    {
      sid: '610826',
      name: '绥德县',
      citySid: '6108',
      provinceSid: '61'
    },
    {
      sid: '610827',
      name: '米脂县',
      citySid: '6108',
      provinceSid: '61'
    },
    {
      sid: '610828',
      name: '佳县',
      citySid: '6108',
      provinceSid: '61'
    },
    {
      sid: '610829',
      name: '吴堡县',
      citySid: '6108',
      provinceSid: '61'
    },
    {
      sid: '610830',
      name: '清涧县',
      citySid: '6108',
      provinceSid: '61'
    },
    {
      sid: '610831',
      name: '子洲县',
      citySid: '6108',
      provinceSid: '61'
    },
    {
      sid: '610881',
      name: '神木市',
      citySid: '6108',
      provinceSid: '61'
    },
    {
      sid: '610902',
      name: '汉滨区',
      citySid: '6109',
      provinceSid: '61'
    },
    {
      sid: '610921',
      name: '汉阴县',
      citySid: '6109',
      provinceSid: '61'
    },
    {
      sid: '610922',
      name: '石泉县',
      citySid: '6109',
      provinceSid: '61'
    },
    {
      sid: '610923',
      name: '宁陕县',
      citySid: '6109',
      provinceSid: '61'
    },
    {
      sid: '610924',
      name: '紫阳县',
      citySid: '6109',
      provinceSid: '61'
    },
    {
      sid: '610925',
      name: '岚皋县',
      citySid: '6109',
      provinceSid: '61'
    },
    {
      sid: '610926',
      name: '平利县',
      citySid: '6109',
      provinceSid: '61'
    },
    {
      sid: '610927',
      name: '镇坪县',
      citySid: '6109',
      provinceSid: '61'
    },
    {
      sid: '610928',
      name: '旬阳县',
      citySid: '6109',
      provinceSid: '61'
    },
    {
      sid: '610929',
      name: '白河县',
      citySid: '6109',
      provinceSid: '61'
    },
    {
      sid: '611002',
      name: '商州区',
      citySid: '6110',
      provinceSid: '61'
    },
    {
      sid: '611021',
      name: '洛南县',
      citySid: '6110',
      provinceSid: '61'
    },
    {
      sid: '611022',
      name: '丹凤县',
      citySid: '6110',
      provinceSid: '61'
    },
    {
      sid: '611023',
      name: '商南县',
      citySid: '6110',
      provinceSid: '61'
    },
    {
      sid: '611024',
      name: '山阳县',
      citySid: '6110',
      provinceSid: '61'
    },
    {
      sid: '611025',
      name: '镇安县',
      citySid: '6110',
      provinceSid: '61'
    },
    {
      sid: '611026',
      name: '柞水县',
      citySid: '6110',
      provinceSid: '61'
    },
    {
      sid: '620102',
      name: '城关区',
      citySid: '6201',
      provinceSid: '62'
    },
    {
      sid: '620103',
      name: '七里河区',
      citySid: '6201',
      provinceSid: '62'
    },
    {
      sid: '620104',
      name: '西固区',
      citySid: '6201',
      provinceSid: '62'
    },
    {
      sid: '620105',
      name: '安宁区',
      citySid: '6201',
      provinceSid: '62'
    },
    {
      sid: '620111',
      name: '红古区',
      citySid: '6201',
      provinceSid: '62'
    },
    {
      sid: '620121',
      name: '永登县',
      citySid: '6201',
      provinceSid: '62'
    },
    {
      sid: '620122',
      name: '皋兰县',
      citySid: '6201',
      provinceSid: '62'
    },
    {
      sid: '620123',
      name: '榆中县',
      citySid: '6201',
      provinceSid: '62'
    },
    {
      sid: '620171',
      name: '兰州新区',
      citySid: '6201',
      provinceSid: '62'
    },
    {
      sid: '620201',
      name: '嘉峪关市',
      citySid: '6202',
      provinceSid: '62'
    },
    {
      sid: '620302',
      name: '金川区',
      citySid: '6203',
      provinceSid: '62'
    },
    {
      sid: '620321',
      name: '永昌县',
      citySid: '6203',
      provinceSid: '62'
    },
    {
      sid: '620402',
      name: '白银区',
      citySid: '6204',
      provinceSid: '62'
    },
    {
      sid: '620403',
      name: '平川区',
      citySid: '6204',
      provinceSid: '62'
    },
    {
      sid: '620421',
      name: '靖远县',
      citySid: '6204',
      provinceSid: '62'
    },
    {
      sid: '620422',
      name: '会宁县',
      citySid: '6204',
      provinceSid: '62'
    },
    {
      sid: '620423',
      name: '景泰县',
      citySid: '6204',
      provinceSid: '62'
    },
    {
      sid: '620502',
      name: '秦州区',
      citySid: '6205',
      provinceSid: '62'
    },
    {
      sid: '620503',
      name: '麦积区',
      citySid: '6205',
      provinceSid: '62'
    },
    {
      sid: '620521',
      name: '清水县',
      citySid: '6205',
      provinceSid: '62'
    },
    {
      sid: '620522',
      name: '秦安县',
      citySid: '6205',
      provinceSid: '62'
    },
    {
      sid: '620523',
      name: '甘谷县',
      citySid: '6205',
      provinceSid: '62'
    },
    {
      sid: '620524',
      name: '武山县',
      citySid: '6205',
      provinceSid: '62'
    },
    {
      sid: '620525',
      name: '张家川回族自治县',
      citySid: '6205',
      provinceSid: '62'
    },
    {
      sid: '620602',
      name: '凉州区',
      citySid: '6206',
      provinceSid: '62'
    },
    {
      sid: '620621',
      name: '民勤县',
      citySid: '6206',
      provinceSid: '62'
    },
    {
      sid: '620622',
      name: '古浪县',
      citySid: '6206',
      provinceSid: '62'
    },
    {
      sid: '620623',
      name: '天祝藏族自治县',
      citySid: '6206',
      provinceSid: '62'
    },
    {
      sid: '620702',
      name: '甘州区',
      citySid: '6207',
      provinceSid: '62'
    },
    {
      sid: '620721',
      name: '肃南裕固族自治县',
      citySid: '6207',
      provinceSid: '62'
    },
    {
      sid: '620722',
      name: '民乐县',
      citySid: '6207',
      provinceSid: '62'
    },
    {
      sid: '620723',
      name: '临泽县',
      citySid: '6207',
      provinceSid: '62'
    },
    {
      sid: '620724',
      name: '高台县',
      citySid: '6207',
      provinceSid: '62'
    },
    {
      sid: '620725',
      name: '山丹县',
      citySid: '6207',
      provinceSid: '62'
    },
    {
      sid: '620802',
      name: '崆峒区',
      citySid: '6208',
      provinceSid: '62'
    },
    {
      sid: '620821',
      name: '泾川县',
      citySid: '6208',
      provinceSid: '62'
    },
    {
      sid: '620822',
      name: '灵台县',
      citySid: '6208',
      provinceSid: '62'
    },
    {
      sid: '620823',
      name: '崇信县',
      citySid: '6208',
      provinceSid: '62'
    },
    {
      sid: '620824',
      name: '华亭县',
      citySid: '6208',
      provinceSid: '62'
    },
    {
      sid: '620825',
      name: '庄浪县',
      citySid: '6208',
      provinceSid: '62'
    },
    {
      sid: '620826',
      name: '静宁县',
      citySid: '6208',
      provinceSid: '62'
    },
    {
      sid: '620871',
      name: '平凉工业园区',
      citySid: '6208',
      provinceSid: '62'
    },
    {
      sid: '620902',
      name: '肃州区',
      citySid: '6209',
      provinceSid: '62'
    },
    {
      sid: '620921',
      name: '金塔县',
      citySid: '6209',
      provinceSid: '62'
    },
    {
      sid: '620922',
      name: '瓜州县',
      citySid: '6209',
      provinceSid: '62'
    },
    {
      sid: '620923',
      name: '肃北蒙古族自治县',
      citySid: '6209',
      provinceSid: '62'
    },
    {
      sid: '620924',
      name: '阿克塞哈萨克族自治县',
      citySid: '6209',
      provinceSid: '62'
    },
    {
      sid: '620981',
      name: '玉门市',
      citySid: '6209',
      provinceSid: '62'
    },
    {
      sid: '620982',
      name: '敦煌市',
      citySid: '6209',
      provinceSid: '62'
    },
    {
      sid: '621002',
      name: '西峰区',
      citySid: '6210',
      provinceSid: '62'
    },
    {
      sid: '621021',
      name: '庆城县',
      citySid: '6210',
      provinceSid: '62'
    },
    {
      sid: '621022',
      name: '环县',
      citySid: '6210',
      provinceSid: '62'
    },
    {
      sid: '621023',
      name: '华池县',
      citySid: '6210',
      provinceSid: '62'
    },
    {
      sid: '621024',
      name: '合水县',
      citySid: '6210',
      provinceSid: '62'
    },
    {
      sid: '621025',
      name: '正宁县',
      citySid: '6210',
      provinceSid: '62'
    },
    {
      sid: '621026',
      name: '宁县',
      citySid: '6210',
      provinceSid: '62'
    },
    {
      sid: '621027',
      name: '镇原县',
      citySid: '6210',
      provinceSid: '62'
    },
    {
      sid: '621102',
      name: '安定区',
      citySid: '6211',
      provinceSid: '62'
    },
    {
      sid: '621121',
      name: '通渭县',
      citySid: '6211',
      provinceSid: '62'
    },
    {
      sid: '621122',
      name: '陇西县',
      citySid: '6211',
      provinceSid: '62'
    },
    {
      sid: '621123',
      name: '渭源县',
      citySid: '6211',
      provinceSid: '62'
    },
    {
      sid: '621124',
      name: '临洮县',
      citySid: '6211',
      provinceSid: '62'
    },
    {
      sid: '621125',
      name: '漳县',
      citySid: '6211',
      provinceSid: '62'
    },
    {
      sid: '621126',
      name: '岷县',
      citySid: '6211',
      provinceSid: '62'
    },
    {
      sid: '621202',
      name: '武都区',
      citySid: '6212',
      provinceSid: '62'
    },
    {
      sid: '621221',
      name: '成县',
      citySid: '6212',
      provinceSid: '62'
    },
    {
      sid: '621222',
      name: '文县',
      citySid: '6212',
      provinceSid: '62'
    },
    {
      sid: '621223',
      name: '宕昌县',
      citySid: '6212',
      provinceSid: '62'
    },
    {
      sid: '621224',
      name: '康县',
      citySid: '6212',
      provinceSid: '62'
    },
    {
      sid: '621225',
      name: '西和县',
      citySid: '6212',
      provinceSid: '62'
    },
    {
      sid: '621226',
      name: '礼县',
      citySid: '6212',
      provinceSid: '62'
    },
    {
      sid: '621227',
      name: '徽县',
      citySid: '6212',
      provinceSid: '62'
    },
    {
      sid: '621228',
      name: '两当县',
      citySid: '6212',
      provinceSid: '62'
    },
    {
      sid: '622901',
      name: '临夏市',
      citySid: '6229',
      provinceSid: '62'
    },
    {
      sid: '622921',
      name: '临夏县',
      citySid: '6229',
      provinceSid: '62'
    },
    {
      sid: '622922',
      name: '康乐县',
      citySid: '6229',
      provinceSid: '62'
    },
    {
      sid: '622923',
      name: '永靖县',
      citySid: '6229',
      provinceSid: '62'
    },
    {
      sid: '622924',
      name: '广河县',
      citySid: '6229',
      provinceSid: '62'
    },
    {
      sid: '622925',
      name: '和政县',
      citySid: '6229',
      provinceSid: '62'
    },
    {
      sid: '622926',
      name: '东乡族自治县',
      citySid: '6229',
      provinceSid: '62'
    },
    {
      sid: '622927',
      name: '积石山保安族东乡族撒拉族自治县',
      citySid: '6229',
      provinceSid: '62'
    },
    {
      sid: '623001',
      name: '合作市',
      citySid: '6230',
      provinceSid: '62'
    },
    {
      sid: '623021',
      name: '临潭县',
      citySid: '6230',
      provinceSid: '62'
    },
    {
      sid: '623022',
      name: '卓尼县',
      citySid: '6230',
      provinceSid: '62'
    },
    {
      sid: '623023',
      name: '舟曲县',
      citySid: '6230',
      provinceSid: '62'
    },
    {
      sid: '623024',
      name: '迭部县',
      citySid: '6230',
      provinceSid: '62'
    },
    {
      sid: '623025',
      name: '玛曲县',
      citySid: '6230',
      provinceSid: '62'
    },
    {
      sid: '623026',
      name: '碌曲县',
      citySid: '6230',
      provinceSid: '62'
    },
    {
      sid: '623027',
      name: '夏河县',
      citySid: '6230',
      provinceSid: '62'
    },
    {
      sid: '630102',
      name: '城东区',
      citySid: '6301',
      provinceSid: '63'
    },
    {
      sid: '630103',
      name: '城中区',
      citySid: '6301',
      provinceSid: '63'
    },
    {
      sid: '630104',
      name: '城西区',
      citySid: '6301',
      provinceSid: '63'
    },
    {
      sid: '630105',
      name: '城北区',
      citySid: '6301',
      provinceSid: '63'
    },
    {
      sid: '630121',
      name: '大通回族土族自治县',
      citySid: '6301',
      provinceSid: '63'
    },
    {
      sid: '630122',
      name: '湟中县',
      citySid: '6301',
      provinceSid: '63'
    },
    {
      sid: '630123',
      name: '湟源县',
      citySid: '6301',
      provinceSid: '63'
    },
    {
      sid: '630202',
      name: '乐都区',
      citySid: '6302',
      provinceSid: '63'
    },
    {
      sid: '630203',
      name: '平安区',
      citySid: '6302',
      provinceSid: '63'
    },
    {
      sid: '630222',
      name: '民和回族土族自治县',
      citySid: '6302',
      provinceSid: '63'
    },
    {
      sid: '630223',
      name: '互助土族自治县',
      citySid: '6302',
      provinceSid: '63'
    },
    {
      sid: '630224',
      name: '化隆回族自治县',
      citySid: '6302',
      provinceSid: '63'
    },
    {
      sid: '630225',
      name: '循化撒拉族自治县',
      citySid: '6302',
      provinceSid: '63'
    },
    {
      sid: '632221',
      name: '门源回族自治县',
      citySid: '6322',
      provinceSid: '63'
    },
    {
      sid: '632222',
      name: '祁连县',
      citySid: '6322',
      provinceSid: '63'
    },
    {
      sid: '632223',
      name: '海晏县',
      citySid: '6322',
      provinceSid: '63'
    },
    {
      sid: '632224',
      name: '刚察县',
      citySid: '6322',
      provinceSid: '63'
    },
    {
      sid: '632321',
      name: '同仁县',
      citySid: '6323',
      provinceSid: '63'
    },
    {
      sid: '632322',
      name: '尖扎县',
      citySid: '6323',
      provinceSid: '63'
    },
    {
      sid: '632323',
      name: '泽库县',
      citySid: '6323',
      provinceSid: '63'
    },
    {
      sid: '632324',
      name: '河南蒙古族自治县',
      citySid: '6323',
      provinceSid: '63'
    },
    {
      sid: '632521',
      name: '共和县',
      citySid: '6325',
      provinceSid: '63'
    },
    {
      sid: '632522',
      name: '同德县',
      citySid: '6325',
      provinceSid: '63'
    },
    {
      sid: '632523',
      name: '贵德县',
      citySid: '6325',
      provinceSid: '63'
    },
    {
      sid: '632524',
      name: '兴海县',
      citySid: '6325',
      provinceSid: '63'
    },
    {
      sid: '632525',
      name: '贵南县',
      citySid: '6325',
      provinceSid: '63'
    },
    {
      sid: '632621',
      name: '玛沁县',
      citySid: '6326',
      provinceSid: '63'
    },
    {
      sid: '632622',
      name: '班玛县',
      citySid: '6326',
      provinceSid: '63'
    },
    {
      sid: '632623',
      name: '甘德县',
      citySid: '6326',
      provinceSid: '63'
    },
    {
      sid: '632624',
      name: '达日县',
      citySid: '6326',
      provinceSid: '63'
    },
    {
      sid: '632625',
      name: '久治县',
      citySid: '6326',
      provinceSid: '63'
    },
    {
      sid: '632626',
      name: '玛多县',
      citySid: '6326',
      provinceSid: '63'
    },
    {
      sid: '632701',
      name: '玉树市',
      citySid: '6327',
      provinceSid: '63'
    },
    {
      sid: '632722',
      name: '杂多县',
      citySid: '6327',
      provinceSid: '63'
    },
    {
      sid: '632723',
      name: '称多县',
      citySid: '6327',
      provinceSid: '63'
    },
    {
      sid: '632724',
      name: '治多县',
      citySid: '6327',
      provinceSid: '63'
    },
    {
      sid: '632725',
      name: '囊谦县',
      citySid: '6327',
      provinceSid: '63'
    },
    {
      sid: '632726',
      name: '曲麻莱县',
      citySid: '6327',
      provinceSid: '63'
    },
    {
      sid: '632801',
      name: '格尔木市',
      citySid: '6328',
      provinceSid: '63'
    },
    {
      sid: '632802',
      name: '德令哈市',
      citySid: '6328',
      provinceSid: '63'
    },
    {
      sid: '632821',
      name: '乌兰县',
      citySid: '6328',
      provinceSid: '63'
    },
    {
      sid: '632822',
      name: '都兰县',
      citySid: '6328',
      provinceSid: '63'
    },
    {
      sid: '632823',
      name: '天峻县',
      citySid: '6328',
      provinceSid: '63'
    },
    {
      sid: '632857',
      name: '大柴旦行政委员会',
      citySid: '6328',
      provinceSid: '63'
    },
    {
      sid: '632858',
      name: '冷湖行政委员会',
      citySid: '6328',
      provinceSid: '63'
    },
    {
      sid: '632859',
      name: '茫崖行政委员会',
      citySid: '6328',
      provinceSid: '63'
    },
    {
      sid: '640104',
      name: '兴庆区',
      citySid: '6401',
      provinceSid: '64'
    },
    {
      sid: '640105',
      name: '西夏区',
      citySid: '6401',
      provinceSid: '64'
    },
    {
      sid: '640106',
      name: '金凤区',
      citySid: '6401',
      provinceSid: '64'
    },
    {
      sid: '640121',
      name: '永宁县',
      citySid: '6401',
      provinceSid: '64'
    },
    {
      sid: '640122',
      name: '贺兰县',
      citySid: '6401',
      provinceSid: '64'
    },
    {
      sid: '640181',
      name: '灵武市',
      citySid: '6401',
      provinceSid: '64'
    },
    {
      sid: '640202',
      name: '大武口区',
      citySid: '6402',
      provinceSid: '64'
    },
    {
      sid: '640205',
      name: '惠农区',
      citySid: '6402',
      provinceSid: '64'
    },
    {
      sid: '640221',
      name: '平罗县',
      citySid: '6402',
      provinceSid: '64'
    },
    {
      sid: '640302',
      name: '利通区',
      citySid: '6403',
      provinceSid: '64'
    },
    {
      sid: '640303',
      name: '红寺堡区',
      citySid: '6403',
      provinceSid: '64'
    },
    {
      sid: '640323',
      name: '盐池县',
      citySid: '6403',
      provinceSid: '64'
    },
    {
      sid: '640324',
      name: '同心县',
      citySid: '6403',
      provinceSid: '64'
    },
    {
      sid: '640381',
      name: '青铜峡市',
      citySid: '6403',
      provinceSid: '64'
    },
    {
      sid: '640402',
      name: '原州区',
      citySid: '6404',
      provinceSid: '64'
    },
    {
      sid: '640422',
      name: '西吉县',
      citySid: '6404',
      provinceSid: '64'
    },
    {
      sid: '640423',
      name: '隆德县',
      citySid: '6404',
      provinceSid: '64'
    },
    {
      sid: '640424',
      name: '泾源县',
      citySid: '6404',
      provinceSid: '64'
    },
    {
      sid: '640425',
      name: '彭阳县',
      citySid: '6404',
      provinceSid: '64'
    },
    {
      sid: '640502',
      name: '沙坡头区',
      citySid: '6405',
      provinceSid: '64'
    },
    {
      sid: '640521',
      name: '中宁县',
      citySid: '6405',
      provinceSid: '64'
    },
    {
      sid: '640522',
      name: '海原县',
      citySid: '6405',
      provinceSid: '64'
    },
    {
      sid: '650102',
      name: '天山区',
      citySid: '6501',
      provinceSid: '65'
    },
    {
      sid: '650103',
      name: '沙依巴克区',
      citySid: '6501',
      provinceSid: '65'
    },
    {
      sid: '650104',
      name: '新市区',
      citySid: '6501',
      provinceSid: '65'
    },
    {
      sid: '650105',
      name: '水磨沟区',
      citySid: '6501',
      provinceSid: '65'
    },
    {
      sid: '650106',
      name: '头屯河区',
      citySid: '6501',
      provinceSid: '65'
    },
    {
      sid: '650107',
      name: '达坂城区',
      citySid: '6501',
      provinceSid: '65'
    },
    {
      sid: '650109',
      name: '米东区',
      citySid: '6501',
      provinceSid: '65'
    },
    {
      sid: '650121',
      name: '乌鲁木齐县',
      citySid: '6501',
      provinceSid: '65'
    },
    {
      sid: '650171',
      name: '乌鲁木齐经济技术开发区',
      citySid: '6501',
      provinceSid: '65'
    },
    {
      sid: '650172',
      name: '乌鲁木齐高新技术产业开发区',
      citySid: '6501',
      provinceSid: '65'
    },
    {
      sid: '650202',
      name: '独山子区',
      citySid: '6502',
      provinceSid: '65'
    },
    {
      sid: '650203',
      name: '克拉玛依区',
      citySid: '6502',
      provinceSid: '65'
    },
    {
      sid: '650204',
      name: '白碱滩区',
      citySid: '6502',
      provinceSid: '65'
    },
    {
      sid: '650205',
      name: '乌尔禾区',
      citySid: '6502',
      provinceSid: '65'
    },
    {
      sid: '650402',
      name: '高昌区',
      citySid: '6504',
      provinceSid: '65'
    },
    {
      sid: '650421',
      name: '鄯善县',
      citySid: '6504',
      provinceSid: '65'
    },
    {
      sid: '650422',
      name: '托克逊县',
      citySid: '6504',
      provinceSid: '65'
    },
    {
      sid: '650502',
      name: '伊州区',
      citySid: '6505',
      provinceSid: '65'
    },
    {
      sid: '650521',
      name: '巴里坤哈萨克自治县',
      citySid: '6505',
      provinceSid: '65'
    },
    {
      sid: '650522',
      name: '伊吾县',
      citySid: '6505',
      provinceSid: '65'
    },
    {
      sid: '652301',
      name: '昌吉市',
      citySid: '6523',
      provinceSid: '65'
    },
    {
      sid: '652302',
      name: '阜康市',
      citySid: '6523',
      provinceSid: '65'
    },
    {
      sid: '652323',
      name: '呼图壁县',
      citySid: '6523',
      provinceSid: '65'
    },
    {
      sid: '652324',
      name: '玛纳斯县',
      citySid: '6523',
      provinceSid: '65'
    },
    {
      sid: '652325',
      name: '奇台县',
      citySid: '6523',
      provinceSid: '65'
    },
    {
      sid: '652327',
      name: '吉木萨尔县',
      citySid: '6523',
      provinceSid: '65'
    },
    {
      sid: '652328',
      name: '木垒哈萨克自治县',
      citySid: '6523',
      provinceSid: '65'
    },
    {
      sid: '652701',
      name: '博乐市',
      citySid: '6527',
      provinceSid: '65'
    },
    {
      sid: '652702',
      name: '阿拉山口市',
      citySid: '6527',
      provinceSid: '65'
    },
    {
      sid: '652722',
      name: '精河县',
      citySid: '6527',
      provinceSid: '65'
    },
    {
      sid: '652723',
      name: '温泉县',
      citySid: '6527',
      provinceSid: '65'
    },
    {
      sid: '652801',
      name: '库尔勒市',
      citySid: '6528',
      provinceSid: '65'
    },
    {
      sid: '652822',
      name: '轮台县',
      citySid: '6528',
      provinceSid: '65'
    },
    {
      sid: '652823',
      name: '尉犁县',
      citySid: '6528',
      provinceSid: '65'
    },
    {
      sid: '652824',
      name: '若羌县',
      citySid: '6528',
      provinceSid: '65'
    },
    {
      sid: '652825',
      name: '且末县',
      citySid: '6528',
      provinceSid: '65'
    },
    {
      sid: '652826',
      name: '焉耆回族自治县',
      citySid: '6528',
      provinceSid: '65'
    },
    {
      sid: '652827',
      name: '和静县',
      citySid: '6528',
      provinceSid: '65'
    },
    {
      sid: '652828',
      name: '和硕县',
      citySid: '6528',
      provinceSid: '65'
    },
    {
      sid: '652829',
      name: '博湖县',
      citySid: '6528',
      provinceSid: '65'
    },
    {
      sid: '652871',
      name: '库尔勒经济技术开发区',
      citySid: '6528',
      provinceSid: '65'
    },
    {
      sid: '652901',
      name: '阿克苏市',
      citySid: '6529',
      provinceSid: '65'
    },
    {
      sid: '652922',
      name: '温宿县',
      citySid: '6529',
      provinceSid: '65'
    },
    {
      sid: '652923',
      name: '库车县',
      citySid: '6529',
      provinceSid: '65'
    },
    {
      sid: '652924',
      name: '沙雅县',
      citySid: '6529',
      provinceSid: '65'
    },
    {
      sid: '652925',
      name: '新和县',
      citySid: '6529',
      provinceSid: '65'
    },
    {
      sid: '652926',
      name: '拜城县',
      citySid: '6529',
      provinceSid: '65'
    },
    {
      sid: '652927',
      name: '乌什县',
      citySid: '6529',
      provinceSid: '65'
    },
    {
      sid: '652928',
      name: '阿瓦提县',
      citySid: '6529',
      provinceSid: '65'
    },
    {
      sid: '652929',
      name: '柯坪县',
      citySid: '6529',
      provinceSid: '65'
    },
    {
      sid: '653001',
      name: '阿图什市',
      citySid: '6530',
      provinceSid: '65'
    },
    {
      sid: '653022',
      name: '阿克陶县',
      citySid: '6530',
      provinceSid: '65'
    },
    {
      sid: '653023',
      name: '阿合奇县',
      citySid: '6530',
      provinceSid: '65'
    },
    {
      sid: '653024',
      name: '乌恰县',
      citySid: '6530',
      provinceSid: '65'
    },
    {
      sid: '653101',
      name: '喀什市',
      citySid: '6531',
      provinceSid: '65'
    },
    {
      sid: '653121',
      name: '疏附县',
      citySid: '6531',
      provinceSid: '65'
    },
    {
      sid: '653122',
      name: '疏勒县',
      citySid: '6531',
      provinceSid: '65'
    },
    {
      sid: '653123',
      name: '英吉沙县',
      citySid: '6531',
      provinceSid: '65'
    },
    {
      sid: '653124',
      name: '泽普县',
      citySid: '6531',
      provinceSid: '65'
    },
    {
      sid: '653125',
      name: '莎车县',
      citySid: '6531',
      provinceSid: '65'
    },
    {
      sid: '653126',
      name: '叶城县',
      citySid: '6531',
      provinceSid: '65'
    },
    {
      sid: '653127',
      name: '麦盖提县',
      citySid: '6531',
      provinceSid: '65'
    },
    {
      sid: '653128',
      name: '岳普湖县',
      citySid: '6531',
      provinceSid: '65'
    },
    {
      sid: '653129',
      name: '伽师县',
      citySid: '6531',
      provinceSid: '65'
    },
    {
      sid: '653130',
      name: '巴楚县',
      citySid: '6531',
      provinceSid: '65'
    },
    {
      sid: '653131',
      name: '塔什库尔干塔吉克自治县',
      citySid: '6531',
      provinceSid: '65'
    },
    {
      sid: '653201',
      name: '和田市',
      citySid: '6532',
      provinceSid: '65'
    },
    {
      sid: '653221',
      name: '和田县',
      citySid: '6532',
      provinceSid: '65'
    },
    {
      sid: '653222',
      name: '墨玉县',
      citySid: '6532',
      provinceSid: '65'
    },
    {
      sid: '653223',
      name: '皮山县',
      citySid: '6532',
      provinceSid: '65'
    },
    {
      sid: '653224',
      name: '洛浦县',
      citySid: '6532',
      provinceSid: '65'
    },
    {
      sid: '653225',
      name: '策勒县',
      citySid: '6532',
      provinceSid: '65'
    },
    {
      sid: '653226',
      name: '于田县',
      citySid: '6532',
      provinceSid: '65'
    },
    {
      sid: '653227',
      name: '民丰县',
      citySid: '6532',
      provinceSid: '65'
    },
    {
      sid: '654002',
      name: '伊宁市',
      citySid: '6540',
      provinceSid: '65'
    },
    {
      sid: '654003',
      name: '奎屯市',
      citySid: '6540',
      provinceSid: '65'
    },
    {
      sid: '654004',
      name: '霍尔果斯市',
      citySid: '6540',
      provinceSid: '65'
    },
    {
      sid: '654021',
      name: '伊宁县',
      citySid: '6540',
      provinceSid: '65'
    },
    {
      sid: '654022',
      name: '察布查尔锡伯自治县',
      citySid: '6540',
      provinceSid: '65'
    },
    {
      sid: '654023',
      name: '霍城县',
      citySid: '6540',
      provinceSid: '65'
    },
    {
      sid: '654024',
      name: '巩留县',
      citySid: '6540',
      provinceSid: '65'
    },
    {
      sid: '654025',
      name: '新源县',
      citySid: '6540',
      provinceSid: '65'
    },
    {
      sid: '654026',
      name: '昭苏县',
      citySid: '6540',
      provinceSid: '65'
    },
    {
      sid: '654027',
      name: '特克斯县',
      citySid: '6540',
      provinceSid: '65'
    },
    {
      sid: '654028',
      name: '尼勒克县',
      citySid: '6540',
      provinceSid: '65'
    },
    {
      sid: '654201',
      name: '塔城市',
      citySid: '6542',
      provinceSid: '65'
    },
    {
      sid: '654202',
      name: '乌苏市',
      citySid: '6542',
      provinceSid: '65'
    },
    {
      sid: '654221',
      name: '额敏县',
      citySid: '6542',
      provinceSid: '65'
    },
    {
      sid: '654223',
      name: '沙湾县',
      citySid: '6542',
      provinceSid: '65'
    },
    {
      sid: '654224',
      name: '托里县',
      citySid: '6542',
      provinceSid: '65'
    },
    {
      sid: '654225',
      name: '裕民县',
      citySid: '6542',
      provinceSid: '65'
    },
    {
      sid: '654226',
      name: '和布克赛尔蒙古自治县',
      citySid: '6542',
      provinceSid: '65'
    },
    {
      sid: '654301',
      name: '阿勒泰市',
      citySid: '6543',
      provinceSid: '65'
    },
    {
      sid: '654321',
      name: '布尔津县',
      citySid: '6543',
      provinceSid: '65'
    },
    {
      sid: '654322',
      name: '富蕴县',
      citySid: '6543',
      provinceSid: '65'
    },
    {
      sid: '654323',
      name: '福海县',
      citySid: '6543',
      provinceSid: '65'
    },
    {
      sid: '654324',
      name: '哈巴河县',
      citySid: '6543',
      provinceSid: '65'
    },
    {
      sid: '654325',
      name: '青河县',
      citySid: '6543',
      provinceSid: '65'
    },
    {
      sid: '654326',
      name: '吉木乃县',
      citySid: '6543',
      provinceSid: '65'
    },
    {
      sid: '659001',
      name: '石河子市',
      citySid: '6590',
      provinceSid: '65'
    },
    {
      sid: '659002',
      name: '阿拉尔市',
      citySid: '6590',
      provinceSid: '65'
    },
    {
      sid: '659003',
      name: '图木舒克市',
      citySid: '6590',
      provinceSid: '65'
    },
    {
      sid: '659004',
      name: '五家渠市',
      citySid: '6590',
      provinceSid: '65'
    },
    {
      sid: '659006',
      name: '铁门关市',
      citySid: '6590',
      provinceSid: '65'
    }
  ],
  city: [
    {
      sid: '1101',
      name: '市辖区',
      provinceSid: '11'
    },
    {
      sid: '1201',
      name: '市辖区',
      provinceSid: '12'
    },
    {
      sid: '1301',
      name: '石家庄市',
      provinceSid: '13'
    },
    {
      sid: '1302',
      name: '唐山市',
      provinceSid: '13'
    },
    {
      sid: '1303',
      name: '秦皇岛市',
      provinceSid: '13'
    },
    {
      sid: '1304',
      name: '邯郸市',
      provinceSid: '13'
    },
    {
      sid: '1305',
      name: '邢台市',
      provinceSid: '13'
    },
    {
      sid: '1306',
      name: '保定市',
      provinceSid: '13'
    },
    {
      sid: '1307',
      name: '张家口市',
      provinceSid: '13'
    },
    {
      sid: '1308',
      name: '承德市',
      provinceSid: '13'
    },
    {
      sid: '1309',
      name: '沧州市',
      provinceSid: '13'
    },
    {
      sid: '1310',
      name: '廊坊市',
      provinceSid: '13'
    },
    {
      sid: '1311',
      name: '衡水市',
      provinceSid: '13'
    },
    {
      sid: '1401',
      name: '太原市',
      provinceSid: '14'
    },
    {
      sid: '1402',
      name: '大同市',
      provinceSid: '14'
    },
    {
      sid: '1403',
      name: '阳泉市',
      provinceSid: '14'
    },
    {
      sid: '1404',
      name: '长治市',
      provinceSid: '14'
    },
    {
      sid: '1405',
      name: '晋城市',
      provinceSid: '14'
    },
    {
      sid: '1406',
      name: '朔州市',
      provinceSid: '14'
    },
    {
      sid: '1407',
      name: '晋中市',
      provinceSid: '14'
    },
    {
      sid: '1408',
      name: '运城市',
      provinceSid: '14'
    },
    {
      sid: '1409',
      name: '忻州市',
      provinceSid: '14'
    },
    {
      sid: '1410',
      name: '临汾市',
      provinceSid: '14'
    },
    {
      sid: '1411',
      name: '吕梁市',
      provinceSid: '14'
    },
    {
      sid: '1501',
      name: '呼和浩特市',
      provinceSid: '15'
    },
    {
      sid: '1502',
      name: '包头市',
      provinceSid: '15'
    },
    {
      sid: '1503',
      name: '乌海市',
      provinceSid: '15'
    },
    {
      sid: '1504',
      name: '赤峰市',
      provinceSid: '15'
    },
    {
      sid: '1505',
      name: '通辽市',
      provinceSid: '15'
    },
    {
      sid: '1506',
      name: '鄂尔多斯市',
      provinceSid: '15'
    },
    {
      sid: '1507',
      name: '呼伦贝尔市',
      provinceSid: '15'
    },
    {
      sid: '1508',
      name: '巴彦淖尔市',
      provinceSid: '15'
    },
    {
      sid: '1509',
      name: '乌兰察布市',
      provinceSid: '15'
    },
    {
      sid: '1522',
      name: '兴安盟',
      provinceSid: '15'
    },
    {
      sid: '1525',
      name: '锡林郭勒盟',
      provinceSid: '15'
    },
    {
      sid: '1529',
      name: '阿拉善盟',
      provinceSid: '15'
    },
    {
      sid: '2101',
      name: '沈阳市',
      provinceSid: '21'
    },
    {
      sid: '2102',
      name: '大连市',
      provinceSid: '21'
    },
    {
      sid: '2103',
      name: '鞍山市',
      provinceSid: '21'
    },
    {
      sid: '2104',
      name: '抚顺市',
      provinceSid: '21'
    },
    {
      sid: '2105',
      name: '本溪市',
      provinceSid: '21'
    },
    {
      sid: '2106',
      name: '丹东市',
      provinceSid: '21'
    },
    {
      sid: '2107',
      name: '锦州市',
      provinceSid: '21'
    },
    {
      sid: '2108',
      name: '营口市',
      provinceSid: '21'
    },
    {
      sid: '2109',
      name: '阜新市',
      provinceSid: '21'
    },
    {
      sid: '2110',
      name: '辽阳市',
      provinceSid: '21'
    },
    {
      sid: '2111',
      name: '盘锦市',
      provinceSid: '21'
    },
    {
      sid: '2112',
      name: '铁岭市',
      provinceSid: '21'
    },
    {
      sid: '2113',
      name: '朝阳市',
      provinceSid: '21'
    },
    {
      sid: '2114',
      name: '葫芦岛市',
      provinceSid: '21'
    },
    {
      sid: '2201',
      name: '长春市',
      provinceSid: '22'
    },
    {
      sid: '2202',
      name: '吉林市',
      provinceSid: '22'
    },
    {
      sid: '2203',
      name: '四平市',
      provinceSid: '22'
    },
    {
      sid: '2204',
      name: '辽源市',
      provinceSid: '22'
    },
    {
      sid: '2205',
      name: '通化市',
      provinceSid: '22'
    },
    {
      sid: '2206',
      name: '白山市',
      provinceSid: '22'
    },
    {
      sid: '2207',
      name: '松原市',
      provinceSid: '22'
    },
    {
      sid: '2208',
      name: '白城市',
      provinceSid: '22'
    },
    {
      sid: '2224',
      name: '延边朝鲜族自治州',
      provinceSid: '22'
    },
    {
      sid: '2301',
      name: '哈尔滨市',
      provinceSid: '23'
    },
    {
      sid: '2302',
      name: '齐齐哈尔市',
      provinceSid: '23'
    },
    {
      sid: '2303',
      name: '鸡西市',
      provinceSid: '23'
    },
    {
      sid: '2304',
      name: '鹤岗市',
      provinceSid: '23'
    },
    {
      sid: '2305',
      name: '双鸭山市',
      provinceSid: '23'
    },
    {
      sid: '2306',
      name: '大庆市',
      provinceSid: '23'
    },
    {
      sid: '2307',
      name: '伊春市',
      provinceSid: '23'
    },
    {
      sid: '2308',
      name: '佳木斯市',
      provinceSid: '23'
    },
    {
      sid: '2309',
      name: '七台河市',
      provinceSid: '23'
    },
    {
      sid: '2310',
      name: '牡丹江市',
      provinceSid: '23'
    },
    {
      sid: '2311',
      name: '黑河市',
      provinceSid: '23'
    },
    {
      sid: '2312',
      name: '绥化市',
      provinceSid: '23'
    },
    {
      sid: '2327',
      name: '大兴安岭地区',
      provinceSid: '23'
    },
    {
      sid: '3101',
      name: '市辖区',
      provinceSid: '31'
    },
    {
      sid: '3201',
      name: '南京市',
      provinceSid: '32'
    },
    {
      sid: '3202',
      name: '无锡市',
      provinceSid: '32'
    },
    {
      sid: '3203',
      name: '徐州市',
      provinceSid: '32'
    },
    {
      sid: '3204',
      name: '常州市',
      provinceSid: '32'
    },
    {
      sid: '3205',
      name: '苏州市',
      provinceSid: '32'
    },
    {
      sid: '3206',
      name: '南通市',
      provinceSid: '32'
    },
    {
      sid: '3207',
      name: '连云港市',
      provinceSid: '32'
    },
    {
      sid: '3208',
      name: '淮安市',
      provinceSid: '32'
    },
    {
      sid: '3209',
      name: '盐城市',
      provinceSid: '32'
    },
    {
      sid: '3210',
      name: '扬州市',
      provinceSid: '32'
    },
    {
      sid: '3211',
      name: '镇江市',
      provinceSid: '32'
    },
    {
      sid: '3212',
      name: '泰州市',
      provinceSid: '32'
    },
    {
      sid: '3213',
      name: '宿迁市',
      provinceSid: '32'
    },
    {
      sid: '3301',
      name: '杭州市',
      provinceSid: '33'
    },
    {
      sid: '3302',
      name: '宁波市',
      provinceSid: '33'
    },
    {
      sid: '3303',
      name: '温州市',
      provinceSid: '33'
    },
    {
      sid: '3304',
      name: '嘉兴市',
      provinceSid: '33'
    },
    {
      sid: '3305',
      name: '湖州市',
      provinceSid: '33'
    },
    {
      sid: '3306',
      name: '绍兴市',
      provinceSid: '33'
    },
    {
      sid: '3307',
      name: '金华市',
      provinceSid: '33'
    },
    {
      sid: '3308',
      name: '衢州市',
      provinceSid: '33'
    },
    {
      sid: '3309',
      name: '舟山市',
      provinceSid: '33'
    },
    {
      sid: '3310',
      name: '台州市',
      provinceSid: '33'
    },
    {
      sid: '3311',
      name: '丽水市',
      provinceSid: '33'
    },
    {
      sid: '3401',
      name: '合肥市',
      provinceSid: '34'
    },
    {
      sid: '3402',
      name: '芜湖市',
      provinceSid: '34'
    },
    {
      sid: '3403',
      name: '蚌埠市',
      provinceSid: '34'
    },
    {
      sid: '3404',
      name: '淮南市',
      provinceSid: '34'
    },
    {
      sid: '3405',
      name: '马鞍山市',
      provinceSid: '34'
    },
    {
      sid: '3406',
      name: '淮北市',
      provinceSid: '34'
    },
    {
      sid: '3407',
      name: '铜陵市',
      provinceSid: '34'
    },
    {
      sid: '3408',
      name: '安庆市',
      provinceSid: '34'
    },
    {
      sid: '3410',
      name: '黄山市',
      provinceSid: '34'
    },
    {
      sid: '3411',
      name: '滁州市',
      provinceSid: '34'
    },
    {
      sid: '3412',
      name: '阜阳市',
      provinceSid: '34'
    },
    {
      sid: '3413',
      name: '宿州市',
      provinceSid: '34'
    },
    {
      sid: '3415',
      name: '六安市',
      provinceSid: '34'
    },
    {
      sid: '3416',
      name: '亳州市',
      provinceSid: '34'
    },
    {
      sid: '3417',
      name: '池州市',
      provinceSid: '34'
    },
    {
      sid: '3418',
      name: '宣城市',
      provinceSid: '34'
    },
    {
      sid: '3501',
      name: '福州市',
      provinceSid: '35'
    },
    {
      sid: '3502',
      name: '厦门市',
      provinceSid: '35'
    },
    {
      sid: '3503',
      name: '莆田市',
      provinceSid: '35'
    },
    {
      sid: '3504',
      name: '三明市',
      provinceSid: '35'
    },
    {
      sid: '3505',
      name: '泉州市',
      provinceSid: '35'
    },
    {
      sid: '3506',
      name: '漳州市',
      provinceSid: '35'
    },
    {
      sid: '3507',
      name: '南平市',
      provinceSid: '35'
    },
    {
      sid: '3508',
      name: '龙岩市',
      provinceSid: '35'
    },
    {
      sid: '3509',
      name: '宁德市',
      provinceSid: '35'
    },
    {
      sid: '3601',
      name: '南昌市',
      provinceSid: '36'
    },
    {
      sid: '3602',
      name: '景德镇市',
      provinceSid: '36'
    },
    {
      sid: '3603',
      name: '萍乡市',
      provinceSid: '36'
    },
    {
      sid: '3604',
      name: '九江市',
      provinceSid: '36'
    },
    {
      sid: '3605',
      name: '新余市',
      provinceSid: '36'
    },
    {
      sid: '3606',
      name: '鹰潭市',
      provinceSid: '36'
    },
    {
      sid: '3607',
      name: '赣州市',
      provinceSid: '36'
    },
    {
      sid: '3608',
      name: '吉安市',
      provinceSid: '36'
    },
    {
      sid: '3609',
      name: '宜春市',
      provinceSid: '36'
    },
    {
      sid: '3610',
      name: '抚州市',
      provinceSid: '36'
    },
    {
      sid: '3611',
      name: '上饶市',
      provinceSid: '36'
    },
    {
      sid: '3701',
      name: '济南市',
      provinceSid: '37'
    },
    {
      sid: '3702',
      name: '青岛市',
      provinceSid: '37'
    },
    {
      sid: '3703',
      name: '淄博市',
      provinceSid: '37'
    },
    {
      sid: '3704',
      name: '枣庄市',
      provinceSid: '37'
    },
    {
      sid: '3705',
      name: '东营市',
      provinceSid: '37'
    },
    {
      sid: '3706',
      name: '烟台市',
      provinceSid: '37'
    },
    {
      sid: '3707',
      name: '潍坊市',
      provinceSid: '37'
    },
    {
      sid: '3708',
      name: '济宁市',
      provinceSid: '37'
    },
    {
      sid: '3709',
      name: '泰安市',
      provinceSid: '37'
    },
    {
      sid: '3710',
      name: '威海市',
      provinceSid: '37'
    },
    {
      sid: '3711',
      name: '日照市',
      provinceSid: '37'
    },
    {
      sid: '3712',
      name: '莱芜市',
      provinceSid: '37'
    },
    {
      sid: '3713',
      name: '临沂市',
      provinceSid: '37'
    },
    {
      sid: '3714',
      name: '德州市',
      provinceSid: '37'
    },
    {
      sid: '3715',
      name: '聊城市',
      provinceSid: '37'
    },
    {
      sid: '3716',
      name: '滨州市',
      provinceSid: '37'
    },
    {
      sid: '3717',
      name: '菏泽市',
      provinceSid: '37'
    },
    {
      sid: '4101',
      name: '郑州市',
      provinceSid: '41'
    },
    {
      sid: '4102',
      name: '开封市',
      provinceSid: '41'
    },
    {
      sid: '4103',
      name: '洛阳市',
      provinceSid: '41'
    },
    {
      sid: '4104',
      name: '平顶山市',
      provinceSid: '41'
    },
    {
      sid: '4105',
      name: '安阳市',
      provinceSid: '41'
    },
    {
      sid: '4106',
      name: '鹤壁市',
      provinceSid: '41'
    },
    {
      sid: '4107',
      name: '新乡市',
      provinceSid: '41'
    },
    {
      sid: '4108',
      name: '焦作市',
      provinceSid: '41'
    },
    {
      sid: '4109',
      name: '濮阳市',
      provinceSid: '41'
    },
    {
      sid: '4110',
      name: '许昌市',
      provinceSid: '41'
    },
    {
      sid: '4111',
      name: '漯河市',
      provinceSid: '41'
    },
    {
      sid: '4112',
      name: '三门峡市',
      provinceSid: '41'
    },
    {
      sid: '4113',
      name: '南阳市',
      provinceSid: '41'
    },
    {
      sid: '4114',
      name: '商丘市',
      provinceSid: '41'
    },
    {
      sid: '4115',
      name: '信阳市',
      provinceSid: '41'
    },
    {
      sid: '4116',
      name: '周口市',
      provinceSid: '41'
    },
    {
      sid: '4117',
      name: '驻马店市',
      provinceSid: '41'
    },
    {
      sid: '4190',
      name: '省直辖县级行政区划',
      provinceSid: '41'
    },
    {
      sid: '4201',
      name: '武汉市',
      provinceSid: '42'
    },
    {
      sid: '4202',
      name: '黄石市',
      provinceSid: '42'
    },
    {
      sid: '4203',
      name: '十堰市',
      provinceSid: '42'
    },
    {
      sid: '4205',
      name: '宜昌市',
      provinceSid: '42'
    },
    {
      sid: '4206',
      name: '襄阳市',
      provinceSid: '42'
    },
    {
      sid: '4207',
      name: '鄂州市',
      provinceSid: '42'
    },
    {
      sid: '4208',
      name: '荆门市',
      provinceSid: '42'
    },
    {
      sid: '4209',
      name: '孝感市',
      provinceSid: '42'
    },
    {
      sid: '4210',
      name: '荆州市',
      provinceSid: '42'
    },
    {
      sid: '4211',
      name: '黄冈市',
      provinceSid: '42'
    },
    {
      sid: '4212',
      name: '咸宁市',
      provinceSid: '42'
    },
    {
      sid: '4213',
      name: '随州市',
      provinceSid: '42'
    },
    {
      sid: '4228',
      name: '恩施土家族苗族自治州',
      provinceSid: '42'
    },
    {
      sid: '4290',
      name: '省直辖县级行政区划',
      provinceSid: '42'
    },
    {
      sid: '4301',
      name: '长沙市',
      provinceSid: '43'
    },
    {
      sid: '4302',
      name: '株洲市',
      provinceSid: '43'
    },
    {
      sid: '4303',
      name: '湘潭市',
      provinceSid: '43'
    },
    {
      sid: '4304',
      name: '衡阳市',
      provinceSid: '43'
    },
    {
      sid: '4305',
      name: '邵阳市',
      provinceSid: '43'
    },
    {
      sid: '4306',
      name: '岳阳市',
      provinceSid: '43'
    },
    {
      sid: '4307',
      name: '常德市',
      provinceSid: '43'
    },
    {
      sid: '4308',
      name: '张家界市',
      provinceSid: '43'
    },
    {
      sid: '4309',
      name: '益阳市',
      provinceSid: '43'
    },
    {
      sid: '4310',
      name: '郴州市',
      provinceSid: '43'
    },
    {
      sid: '4311',
      name: '永州市',
      provinceSid: '43'
    },
    {
      sid: '4312',
      name: '怀化市',
      provinceSid: '43'
    },
    {
      sid: '4313',
      name: '娄底市',
      provinceSid: '43'
    },
    {
      sid: '4331',
      name: '湘西土家族苗族自治州',
      provinceSid: '43'
    },
    {
      sid: '4401',
      name: '广州市',
      provinceSid: '44'
    },
    {
      sid: '4402',
      name: '韶关市',
      provinceSid: '44'
    },
    {
      sid: '4403',
      name: '深圳市',
      provinceSid: '44'
    },
    {
      sid: '4404',
      name: '珠海市',
      provinceSid: '44'
    },
    {
      sid: '4405',
      name: '汕头市',
      provinceSid: '44'
    },
    {
      sid: '4406',
      name: '佛山市',
      provinceSid: '44'
    },
    {
      sid: '4407',
      name: '江门市',
      provinceSid: '44'
    },
    {
      sid: '4408',
      name: '湛江市',
      provinceSid: '44'
    },
    {
      sid: '4409',
      name: '茂名市',
      provinceSid: '44'
    },
    {
      sid: '4412',
      name: '肇庆市',
      provinceSid: '44'
    },
    {
      sid: '4413',
      name: '惠州市',
      provinceSid: '44'
    },
    {
      sid: '4414',
      name: '梅州市',
      provinceSid: '44'
    },
    {
      sid: '4415',
      name: '汕尾市',
      provinceSid: '44'
    },
    {
      sid: '4416',
      name: '河源市',
      provinceSid: '44'
    },
    {
      sid: '4417',
      name: '阳江市',
      provinceSid: '44'
    },
    {
      sid: '4418',
      name: '清远市',
      provinceSid: '44'
    },
    {
      sid: '4419',
      name: '东莞市',
      provinceSid: '44'
    },
    {
      sid: '4420',
      name: '中山市',
      provinceSid: '44'
    },
    {
      sid: '4451',
      name: '潮州市',
      provinceSid: '44'
    },
    {
      sid: '4452',
      name: '揭阳市',
      provinceSid: '44'
    },
    {
      sid: '4453',
      name: '云浮市',
      provinceSid: '44'
    },
    {
      sid: '4501',
      name: '南宁市',
      provinceSid: '45'
    },
    {
      sid: '4502',
      name: '柳州市',
      provinceSid: '45'
    },
    {
      sid: '4503',
      name: '桂林市',
      provinceSid: '45'
    },
    {
      sid: '4504',
      name: '梧州市',
      provinceSid: '45'
    },
    {
      sid: '4505',
      name: '北海市',
      provinceSid: '45'
    },
    {
      sid: '4506',
      name: '防城港市',
      provinceSid: '45'
    },
    {
      sid: '4507',
      name: '钦州市',
      provinceSid: '45'
    },
    {
      sid: '4508',
      name: '贵港市',
      provinceSid: '45'
    },
    {
      sid: '4509',
      name: '玉林市',
      provinceSid: '45'
    },
    {
      sid: '4510',
      name: '百色市',
      provinceSid: '45'
    },
    {
      sid: '4511',
      name: '贺州市',
      provinceSid: '45'
    },
    {
      sid: '4512',
      name: '河池市',
      provinceSid: '45'
    },
    {
      sid: '4513',
      name: '来宾市',
      provinceSid: '45'
    },
    {
      sid: '4514',
      name: '崇左市',
      provinceSid: '45'
    },
    {
      sid: '4601',
      name: '海口市',
      provinceSid: '46'
    },
    {
      sid: '4602',
      name: '三亚市',
      provinceSid: '46'
    },
    {
      sid: '4603',
      name: '三沙市',
      provinceSid: '46'
    },
    {
      sid: '4604',
      name: '儋州市',
      provinceSid: '46'
    },
    {
      sid: '4690',
      name: '省直辖县级行政区划',
      provinceSid: '46'
    },
    {
      sid: '5001',
      name: '市辖区',
      provinceSid: '50'
    },
    {
      sid: '5002',
      name: '县',
      provinceSid: '50'
    },
    {
      sid: '5101',
      name: '成都市',
      provinceSid: '51'
    },
    {
      sid: '5103',
      name: '自贡市',
      provinceSid: '51'
    },
    {
      sid: '5104',
      name: '攀枝花市',
      provinceSid: '51'
    },
    {
      sid: '5105',
      name: '泸州市',
      provinceSid: '51'
    },
    {
      sid: '5106',
      name: '德阳市',
      provinceSid: '51'
    },
    {
      sid: '5107',
      name: '绵阳市',
      provinceSid: '51'
    },
    {
      sid: '5108',
      name: '广元市',
      provinceSid: '51'
    },
    {
      sid: '5109',
      name: '遂宁市',
      provinceSid: '51'
    },
    {
      sid: '5110',
      name: '内江市',
      provinceSid: '51'
    },
    {
      sid: '5111',
      name: '乐山市',
      provinceSid: '51'
    },
    {
      sid: '5113',
      name: '南充市',
      provinceSid: '51'
    },
    {
      sid: '5114',
      name: '眉山市',
      provinceSid: '51'
    },
    {
      sid: '5115',
      name: '宜宾市',
      provinceSid: '51'
    },
    {
      sid: '5116',
      name: '广安市',
      provinceSid: '51'
    },
    {
      sid: '5117',
      name: '达州市',
      provinceSid: '51'
    },
    {
      sid: '5118',
      name: '雅安市',
      provinceSid: '51'
    },
    {
      sid: '5119',
      name: '巴中市',
      provinceSid: '51'
    },
    {
      sid: '5120',
      name: '资阳市',
      provinceSid: '51'
    },
    {
      sid: '5132',
      name: '阿坝藏族羌族自治州',
      provinceSid: '51'
    },
    {
      sid: '5133',
      name: '甘孜藏族自治州',
      provinceSid: '51'
    },
    {
      sid: '5134',
      name: '凉山彝族自治州',
      provinceSid: '51'
    },
    {
      sid: '5201',
      name: '贵阳市',
      provinceSid: '52'
    },
    {
      sid: '5202',
      name: '六盘水市',
      provinceSid: '52'
    },
    {
      sid: '5203',
      name: '遵义市',
      provinceSid: '52'
    },
    {
      sid: '5204',
      name: '安顺市',
      provinceSid: '52'
    },
    {
      sid: '5205',
      name: '毕节市',
      provinceSid: '52'
    },
    {
      sid: '5206',
      name: '铜仁市',
      provinceSid: '52'
    },
    {
      sid: '5223',
      name: '黔西南布依族苗族自治州',
      provinceSid: '52'
    },
    {
      sid: '5226',
      name: '黔东南苗族侗族自治州',
      provinceSid: '52'
    },
    {
      sid: '5227',
      name: '黔南布依族苗族自治州',
      provinceSid: '52'
    },
    {
      sid: '5301',
      name: '昆明市',
      provinceSid: '53'
    },
    {
      sid: '5303',
      name: '曲靖市',
      provinceSid: '53'
    },
    {
      sid: '5304',
      name: '玉溪市',
      provinceSid: '53'
    },
    {
      sid: '5305',
      name: '保山市',
      provinceSid: '53'
    },
    {
      sid: '5306',
      name: '昭通市',
      provinceSid: '53'
    },
    {
      sid: '5307',
      name: '丽江市',
      provinceSid: '53'
    },
    {
      sid: '5308',
      name: '普洱市',
      provinceSid: '53'
    },
    {
      sid: '5309',
      name: '临沧市',
      provinceSid: '53'
    },
    {
      sid: '5323',
      name: '楚雄彝族自治州',
      provinceSid: '53'
    },
    {
      sid: '5325',
      name: '红河哈尼族彝族自治州',
      provinceSid: '53'
    },
    {
      sid: '5326',
      name: '文山壮族苗族自治州',
      provinceSid: '53'
    },
    {
      sid: '5328',
      name: '西双版纳傣族自治州',
      provinceSid: '53'
    },
    {
      sid: '5329',
      name: '大理白族自治州',
      provinceSid: '53'
    },
    {
      sid: '5331',
      name: '德宏傣族景颇族自治州',
      provinceSid: '53'
    },
    {
      sid: '5333',
      name: '怒江傈僳族自治州',
      provinceSid: '53'
    },
    {
      sid: '5334',
      name: '迪庆藏族自治州',
      provinceSid: '53'
    },
    {
      sid: '5401',
      name: '拉萨市',
      provinceSid: '54'
    },
    {
      sid: '5402',
      name: '日喀则市',
      provinceSid: '54'
    },
    {
      sid: '5403',
      name: '昌都市',
      provinceSid: '54'
    },
    {
      sid: '5404',
      name: '林芝市',
      provinceSid: '54'
    },
    {
      sid: '5405',
      name: '山南市',
      provinceSid: '54'
    },
    {
      sid: '5424',
      name: '那曲地区',
      provinceSid: '54'
    },
    {
      sid: '5425',
      name: '阿里地区',
      provinceSid: '54'
    },
    {
      sid: '6101',
      name: '西安市',
      provinceSid: '61'
    },
    {
      sid: '6102',
      name: '铜川市',
      provinceSid: '61'
    },
    {
      sid: '6103',
      name: '宝鸡市',
      provinceSid: '61'
    },
    {
      sid: '6104',
      name: '咸阳市',
      provinceSid: '61'
    },
    {
      sid: '6105',
      name: '渭南市',
      provinceSid: '61'
    },
    {
      sid: '6106',
      name: '延安市',
      provinceSid: '61'
    },
    {
      sid: '6107',
      name: '汉中市',
      provinceSid: '61'
    },
    {
      sid: '6108',
      name: '榆林市',
      provinceSid: '61'
    },
    {
      sid: '6109',
      name: '安康市',
      provinceSid: '61'
    },
    {
      sid: '6110',
      name: '商洛市',
      provinceSid: '61'
    },
    {
      sid: '6201',
      name: '兰州市',
      provinceSid: '62'
    },
    {
      sid: '6202',
      name: '嘉峪关市',
      provinceSid: '62'
    },
    {
      sid: '6203',
      name: '金昌市',
      provinceSid: '62'
    },
    {
      sid: '6204',
      name: '白银市',
      provinceSid: '62'
    },
    {
      sid: '6205',
      name: '天水市',
      provinceSid: '62'
    },
    {
      sid: '6206',
      name: '武威市',
      provinceSid: '62'
    },
    {
      sid: '6207',
      name: '张掖市',
      provinceSid: '62'
    },
    {
      sid: '6208',
      name: '平凉市',
      provinceSid: '62'
    },
    {
      sid: '6209',
      name: '酒泉市',
      provinceSid: '62'
    },
    {
      sid: '6210',
      name: '庆阳市',
      provinceSid: '62'
    },
    {
      sid: '6211',
      name: '定西市',
      provinceSid: '62'
    },
    {
      sid: '6212',
      name: '陇南市',
      provinceSid: '62'
    },
    {
      sid: '6229',
      name: '临夏回族自治州',
      provinceSid: '62'
    },
    {
      sid: '6230',
      name: '甘南藏族自治州',
      provinceSid: '62'
    },
    {
      sid: '6301',
      name: '西宁市',
      provinceSid: '63'
    },
    {
      sid: '6302',
      name: '海东市',
      provinceSid: '63'
    },
    {
      sid: '6322',
      name: '海北藏族自治州',
      provinceSid: '63'
    },
    {
      sid: '6323',
      name: '黄南藏族自治州',
      provinceSid: '63'
    },
    {
      sid: '6325',
      name: '海南藏族自治州',
      provinceSid: '63'
    },
    {
      sid: '6326',
      name: '果洛藏族自治州',
      provinceSid: '63'
    },
    {
      sid: '6327',
      name: '玉树藏族自治州',
      provinceSid: '63'
    },
    {
      sid: '6328',
      name: '海西蒙古族藏族自治州',
      provinceSid: '63'
    },
    {
      sid: '6401',
      name: '银川市',
      provinceSid: '64'
    },
    {
      sid: '6402',
      name: '石嘴山市',
      provinceSid: '64'
    },
    {
      sid: '6403',
      name: '吴忠市',
      provinceSid: '64'
    },
    {
      sid: '6404',
      name: '固原市',
      provinceSid: '64'
    },
    {
      sid: '6405',
      name: '中卫市',
      provinceSid: '64'
    },
    {
      sid: '6501',
      name: '乌鲁木齐市',
      provinceSid: '65'
    },
    {
      sid: '6502',
      name: '克拉玛依市',
      provinceSid: '65'
    },
    {
      sid: '6504',
      name: '吐鲁番市',
      provinceSid: '65'
    },
    {
      sid: '6505',
      name: '哈密市',
      provinceSid: '65'
    },
    {
      sid: '6523',
      name: '昌吉回族自治州',
      provinceSid: '65'
    },
    {
      sid: '6527',
      name: '博尔塔拉蒙古自治州',
      provinceSid: '65'
    },
    {
      sid: '6528',
      name: '巴音郭楞蒙古自治州',
      provinceSid: '65'
    },
    {
      sid: '6529',
      name: '阿克苏地区',
      provinceSid: '65'
    },
    {
      sid: '6530',
      name: '克孜勒苏柯尔克孜自治州',
      provinceSid: '65'
    },
    {
      sid: '6531',
      name: '喀什地区',
      provinceSid: '65'
    },
    {
      sid: '6532',
      name: '和田地区',
      provinceSid: '65'
    },
    {
      sid: '6540',
      name: '伊犁哈萨克自治州',
      provinceSid: '65'
    },
    {
      sid: '6542',
      name: '塔城地区',
      provinceSid: '65'
    },
    {
      sid: '6543',
      name: '阿勒泰地区',
      provinceSid: '65'
    },
    {
      sid: '6590',
      name: '自治区直辖县级行政区划',
      provinceSid: '65'
    }
  ],
  province: [
    {
      sid: '11',
      name: '北京市'
    },
    {
      sid: '12',
      name: '天津市'
    },
    {
      sid: '13',
      name: '河北省'
    },
    {
      sid: '14',
      name: '山西省'
    },
    {
      sid: '15',
      name: '内蒙古自治区'
    },
    {
      sid: '21',
      name: '辽宁省'
    },
    {
      sid: '22',
      name: '吉林省'
    },
    {
      sid: '23',
      name: '黑龙江省'
    },
    {
      sid: '31',
      name: '上海市'
    },
    {
      sid: '32',
      name: '江苏省'
    },
    {
      sid: '33',
      name: '浙江省'
    },
    {
      sid: '34',
      name: '安徽省'
    },
    {
      sid: '35',
      name: '福建省'
    },
    {
      sid: '36',
      name: '江西省'
    },
    {
      sid: '37',
      name: '山东省'
    },
    {
      sid: '41',
      name: '河南省'
    },
    {
      sid: '42',
      name: '湖北省'
    },
    {
      sid: '43',
      name: '湖南省'
    },
    {
      sid: '44',
      name: '广东省'
    },
    {
      sid: '45',
      name: '广西壮族自治区'
    },
    {
      sid: '46',
      name: '海南省'
    },
    {
      sid: '50',
      name: '重庆市'
    },
    {
      sid: '51',
      name: '四川省'
    },
    {
      sid: '52',
      name: '贵州省'
    },
    {
      sid: '53',
      name: '云南省'
    },
    {
      sid: '54',
      name: '西藏自治区'
    },
    {
      sid: '61',
      name: '陕西省'
    },
    {
      sid: '62',
      name: '甘肃省'
    },
    {
      sid: '63',
      name: '青海省'
    },
    {
      sid: '64',
      name: '宁夏回族自治区'
    },
    {
      sid: '65',
      name: '新疆维吾尔自治区'
    }
  ]
}
