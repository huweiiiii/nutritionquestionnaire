// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App.vue'
import router from './router'
import axios from 'axios'
import Cookies from 'js-cookie'
import { Swipe, SwipeItem, Toast, Picker } from 'mint-ui'
import 'mint-ui/lib/style.css'
import '@/assets/index.css'
import VueAwesomeSwiper from 'vue-awesome-swiper'
// require styles
import 'swiper/dist/css/swiper.css'
Vue.use(VueAwesomeSwiper)
Vue.component(Swipe.name, Swipe)
Vue.component(SwipeItem.name, SwipeItem)
Vue.component(Picker.name, Picker)
console.log(process.env.NODE_ENV)
Vue.prototype.$http = axios
Vue.prototype.$Cookies = Cookies
Vue.prototype.$toast = Toast
Vue.config.productionTip = false
router.beforeEach((to, from, next) => {
/*
console.log(Cookies.get('isLogin'))
if (to.path === '/' && Cookies.get('isLogin') === '1') {
  next('/first')
} else if (to.path !== '/' && Cookies.get('isLogin') !== '1') {
  next('/')
} else {
  next()
}
*/
  next()
})
Vue.prototype.$getQueryString = function (name) {
  let reg = new RegExp('(^|&)' + name + '=([^&]*)(&|$)', 'i')
  let result = window.location.search.substr(1).match(reg)
  if (result != null) {
    return decodeURIComponent(result[2])
  } else {
    return null
  }
}

Vue.prototype.$setDocumentTitle = function (t) {
  document.title = t
  let i = document.createElement('iframe')
  i.style.display = 'none'
  i.onload = function () {
    setTimeout(function () {
      i.remove()
    }, 9)
  }
  document.body.appendChild(i)
}
window.questionArr = []
window.questionArrFirstHalf = []
window.questionArrSecondHalf = []
window.chooseTarget = []
window.q = -1
/* eslint-disable no-new */
let vm = new Vue({
  el: '#app',
  router,
  components: { App },
  template: '<App/>'
})
export default vm
