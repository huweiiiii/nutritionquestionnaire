import Vue from 'vue'
import Router from 'vue-router'
import Index from '@/components/Index'
import StartQuestion from '@/components/StartQuestion'
import InputName from '@/components/InputName'
import InputNameOver from '@/components/InputNameOver'
import MenuQuestion from '@/components/MenuQuestion'
import Question from '@/components/Question'
import NutritionTarget from '@/components/NutritionTarget'
import Order from '@/components/Order'
import Payment from '@/components/Payment'
import PayResult from '@/components/PayResult'
import Share from '@/components/Share'
import CustomerService from '@/components/CustomerService'
Vue.use(Router)
export default new Router({
  routes: [
    {
      path: '/',
      name: 'Index',
      component: Index
    },
    {
      path: '/start-question',
      name: 'StartQuestion',
      component: StartQuestion
    },
    {
      path: '/input-name',
      name: 'InputName',
      component: InputName
    },
    {
      path: '/input-name-over',
      name: 'InputNameOver',
      component: InputNameOver
    },
    {
      path: '/menu-question/:type',
      name: 'MenuQuestion',
      component: MenuQuestion
    },
    {
      path: '/question/:type/:q',
      name: 'Question',
      component: Question
    },
    {
      path: '/nutrition-target',
      name: 'NutritionTarget',
      component: NutritionTarget
    },
    {
      path: '/order',
      name: 'Order',
      component: Order
    },
    {
      path: '/payment',
      name: 'Payment',
      component: Payment
    },
    {
      path: '/pay-result',
      name: 'PayResult',
      component: PayResult
    },
    {
      path: '/share',
      name: 'Share',
      component: Share
    },
    {
      path: '/customer-service',
      name: 'CustomerService',
      component: CustomerService
    }
  ]
})
