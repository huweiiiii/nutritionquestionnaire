'use strict'
import axios from 'axios'
import qs from 'qs'
// import vm from '../main'
// import Cookies from 'js-cookie'
// import web from '../assets/url/web'
let Base64 = require('js-base64').Base64
axios.interceptors.request.use(config => {
  return config
}, error => {
  return Promise.reject(error)
})
axios.interceptors.response.use(response => {
  return response
}, error => {
  return Promise.resolve(error.response)
})
function checkStatus (response) {
  // loading
  // 如果http状态码正常，则直接返回数据
  if (response && (response.status === 200 || response.status === 304 || response.status === 400)) {
    return response
    // 如果不需要除了data之外的数据，可以直接 return response.data
  }
  // 异常状态下，把错误信息返回去
  return {
    status: -404,
    msg: '网络异常'
  }
}
function checkCode (res) {
  // 如果code异常(这里已经包括网络错误，服务器错误，后端抛出的错误)，可以弹出一个错误提示，告诉用户
  if (res.status === -404) {
  }
  if (res.data && (!res.data.success)) {
  }
  return res
}
// let baseURL = web.url
export default {
  post (url, data) {
    /*
    if (Cookies.get('isLogin') !== '1' && vm.$router.currentRoute.name !== 'Index') {
      console.log(vm.$router)
      vm.$router.push('/')
    }
    */
    for (let i in data) {
      data[i] = data[i].toString()
    }
    // data = qs.stringify(data)
    /*
    data = qs.stringify({
      key: Base64.encode(JSON.stringify(data))
    })
    */
    return axios({
      method: 'post',
      // baseURL,
      url,
      data,
      timeout: 10000,
      dataType: 'json',
      headers: {
        'content-type': 'application/json;charset=utf-8'
      }
    }).then(
      (response) => {
        return checkStatus(response)
      }
    ).then(
      (res) => {
        return checkCode(res)
      }
    )
  },
  get (url, params) {
    for (let i in params) {
      params[i] = params[i].toString()
    }
    params = qs.stringify({
      key: Base64.encode(JSON.stringify(params))
    })
    console.log(params)
    return axios({
      method: 'get',
      // baseURL,
      url,
      params, // get 请求时带的参数
      timeout: 10000,
      headers: {}
    }).then(
      (response) => {
        return checkStatus(response)
      }
    ).then(
      (res) => {
        return checkCode(res)
      }
    )
  }
}
