import obj from '../assets/url/web'
export default {
  login: obj.url + '/login/loginVerify',
  areaChain: obj.yanwei + '/area/chain/',
  areaProvinces: obj.yanwei + '/area/provinces',
  areaParent: obj.yanwei + '/area/parent/',
  initQuestion: obj.url + '/wxApi/initQuestion',
  getQ19Question: obj.url + '/wxApi/getQ19Question',
  order: obj.url + '/wxApi/order',
  useCode: obj.url + '/wxApi/useCode',
  createImg: obj.url + '/wxApi/createImg'
}
